import os
import jinja2
import paramiko
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG

from application                import app, db, bcrypt, mail
from application.models import *


flask_general = Blueprint('flask_general', __name__, template_folder="templates/general", url_prefix="/general")


@flask_general.route('/operators')
def general_operators():
    return render_template("general_operators.html")

@flask_general.route('/operators/data')
def general_operators_data():
    query = GeneralOperators.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    #print([data.to_dict() for data in query])
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': GeneralOperators.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_general.route('/station')
def general_station():
    return render_template("general_station.html")

@flask_general.route('/station/data')
def general_station_data():
    query = GeneralStation.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    #print([data.to_dict() for data in query])
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': GeneralStation.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_general.route('/grade')
def general_grade():
    return render_template("general_grade.html")

@flask_general.route('/grade/data')
def general_grade_data():
    query = GeneralGrade.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    #print([data.to_dict() for data in query])
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': GeneralGrade.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_general.route('/assemblysteps')
def general_assemblysteps():
    return render_template("general_assemblysteps.html")

@flask_general.route('/assemblysteps/data')
def assemblysteps_data():
    query = GeneralAssemblystep.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    #print([data.to_dict() for data in query])
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': GeneralAssemblystep.query.count(),
        'draw': request.args.get('draw', type=int),
    }
