import os
import jinja2
import pandas as pd
import paramiko
from datetime import datetime, timedelta
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG

from application                import app, db, bcrypt, mail
from application.models         import *
from application                    import engine


flask_upload = Blueprint('flask_upload', __name__, template_folder="templates/upload", url_prefix="/upload")


@flask_upload.route('/', methods=["GET", "POST"])
def upload():
    return "Hello World"

@flask_upload.route('/pulltest', methods=['POST'])
def pulltest():
    if request.method == 'POST':
        flash('Submission successful XML file is getting created!', 'success')
        id = request.form['id']

        templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
        jinjaEnv = jinja2.Environment(loader=templateFilePath)
        jTemplate = jinjaEnv.get_template("template_sensor_bond_pull_test.xml")
        data = PulltestResults.query.filter_by(id=id).first()
        xml_data = {'runType': 'Test',
                    'location': data.Station,
                    'operator': data.Operator,
                    'date': data.Datetime,
                    'comment': data.comment
                    }

        output = jTemplate.render(xml_data)
        outFileH = open('application/static/data/xml/pulltest/output_' + id + '.xml', 'w')
        outFileH.write(output)
        outFileH.close()
    return render_template("pulltest_overview.html")

@flask_upload.route('/iv_sensor_<measuremend_id>', methods=['GET', 'POST'])
def iv_sensor_upload(measuremend_id):
    data = IVMeasurement.query.filter_by(id=measuremend_id).first()

    df = pd.read_sql_query('''SELECT timestamp, voltage, current, humidity, temperature, ramp_direction  FROM "IV_Measurement_Results" WHERE "IV_measurement_id" = ''' + str(measuremend_id) + ";", con=engine)
    dt_base = datetime(1904, 1, 1, 0, 0, 0)
    data_dict = []
    for i in range(len(df)):
        data_dict.append({
            "time": (dt_base + timedelta(seconds=float(df.iloc[i]['timestamp']))).replace(microsecond=0),
            "voltage": df.iloc[i]['voltage'],
            "current": df.iloc[i]['current'],
            "temperature": df.iloc[i]['temperature'],
            "humidity": df.iloc[i]['humidity'],
        })


    class IVSensorUploadForm(FlaskForm):
        submit          = SubmitField("I confirm to upload")

    form = IVSensorUploadForm()

    id = measuremend_id

    templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_sensor_iv.xml")
    data = IVMeasurement.query.filter_by(id=id).first()
    xml_data = {'runType': data.assembly_step,
                'location': "Aachen",
                'operator': "Aachen Database Account",
                'structure': "2S Sensor",
                'id': data.Sensor_Overview.sensor_name,
                'date': data.timestamp,
                'comment': data.comment,
                'data': data_dict,
                'summary': {
                    'STATION': 'Aachen 1B',
                    'AV_TEMP_DEGC': data.air_temperature,
                    'AV_RH_PRCNT': data.air_humidity
                }
                }

    output = jTemplate.render(xml_data)
    xml_file = output

    if form.validate_on_submit():
        file_path = 'application/static/data/xml/iv/output_' + "2S Sensor" + " - " + str(data.timestamp) + " - " + str(data.Sensor_Overview.sensor_name) + " - " + str(id) + '.xml'
        outFileH = open(file_path, 'w')
        outFileH.write(output)
        outFileH.close()


        print("try uploading data from cern db")
        import sys
        from io import StringIO
        sys.path.append('application/scripts')
        from cmsdbldr_client import LoaderClient
        url = "https://cmsdca.cern.ch/trk_loader/trker/cmsr"

        cli = LoaderClient()
        cmd = ['--url', url, '--test', '--login', file_path]

        responseCode = cli.run(cmd)
        if responseCode == 1:
            responseCode = "Upload successful"
            status_color = 'success'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=True, cmsdb_upload_timestamp=datetime.now()))
            db.session.commit()
        elif responseCode == 4:
            responseCode = "Upload failed"
            status_color = 'danger'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()
        else:
            status_color = 'warning'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()


        print(responseCode)
        flash('Submission done - answer code of the server is ' + str(responseCode),  status_color)

        return redirect(url_for("flask_iv.iv_detail", measurement_number=measuremend_id))



    return render_template("iv_sensor_upload.html", data=data, form=form, xml_file=xml_file)


@flask_upload.route('/metrology_sensor_<measurement_id>', methods=['GET', 'POST'])
def metrology_sensor_upload(measurement_id):
    data = MetrologySensorSensorAlignment.query.filter_by(id=measurement_id).first()

    class MetrologySensorSensorAlignmentUploadForm(FlaskForm):
        # run_type        = StringField(u'run_type', validators=[DataRequired()])
        # location        = StringField(u'Location', validators=[DataRequired()])
        # operator        = StringField(u'operator', validators=[DataRequired()])
        # StringField(u'run_type', validators=[DataRequired()])
        submit          = SubmitField("I confirm to upload")

    form = MetrologySensorSensorAlignmentUploadForm()
    id = measurement_id

    templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_module_metrology.xml")
    data = MetrologySensorSensorAlignment.query.filter_by(id=id).first()
    xml_data = {'runType': 'mod_bareModule',
                'location': "Aachen",
                'operator': "Aachen Database Account",
                'date': data.timestamp,
                'comment':  data.comment if data.comment != None else '',
                'structure': "2S Module",
                'id': data.Module_Overview.cern_module_id,
                'data': [{
                    'kindOfMetrology': "DSM",
                    'rotation': data.rotation_urad,
                    'xShift': data.x_shift_um,
                    'yShift': data.y_shift_um,
                }],
                'summary': {
                    'STATION': 'Aachen 1B Metrology',
                    'AV_TEMP_DEGC': data.air_temperature,
                    'AV_RH_PRCNT': data.air_humidity
                    }
                }

    output = jTemplate.render(xml_data)
    xml_file = output

    if form.validate_on_submit():
        file_path = 'application/static/data/xml/metrology_sensor/output_' + "2S Module" + " - " + str(data.timestamp) + " - " + str(data.Module_Overview.cern_module_id) + " - " + str(id) + '.xml'
        outFileH = open(file_path, 'w')
        outFileH.write(output)
        outFileH.close()


        print("try uploading data from cern db")
        import sys
        from io import StringIO
        sys.path.append('application/scripts')
        from cmsdbldr_client import LoaderClient
        url = "https://cmsdca.cern.ch/trk_loader/trker/cmsr"

        cli = LoaderClient()
        cmd = ['--url', url, '--test', '--login', file_path]

        responseCode = cli.run(cmd)
        if responseCode == 1:
            responseCode = "Upload successful"
            status_color = 'success'
            MetrologySensorSensorAlignment.query.filter_by(id=id).update(dict(cmsdb_upload_status=True, cmsdb_upload_timestamp=datetime.now()))
            db.session.commit()
        elif responseCode == 4:
            responseCode = "Upload failed"
            status_color = 'danger'
            MetrologySensorSensorAlignment.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()
        else:
            status_color = 'warning'
            MetrologySensorSensorAlignment.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()


        print(responseCode)
        flash('Submission done - answer code of the server is ' + str(responseCode),  status_color)

        return redirect(url_for("flask_metrology.metrology_sensor_detail", sensor_number=measurement_id))

    return render_template("metrology_sensor_upload.html", data=data, form=form, xml_file=xml_file)

@flask_upload.route('/pulltest_module_<measurement_id>', methods=['GET', 'POST'])
def pulltest_module_upload(measurement_id):
    data = PulltestMeasurement.query.filter_by(id=measurement_id).first()
    pulltest_product = data.pulltest_product

    df_data = pd.read_sql_query('''SELECT tearoffforce, "correction_factor_tearoffforce", grade  FROM "Pulltest_Results" WHERE pulltest_measurement_id = ''' + str(measurement_id) + ";", con=engine)
    # check if id is already inside DCA-DB
    df_grades = pd.read_sql_query('''SELECT code, description, resultclass, product  FROM "Pulltest_Gradecode"''' ";", con=engine)
    df_grades = df_grades[df_grades['product'] == pulltest_product]

    print(df_grades)

    # print(df_data)
    mean_data = {}
    mean_data['mean_tearoffforce']              = round(df_data['tearoffforce'].mean(), 3)
    mean_data['std_tearoffforce']               = round(df_data['tearoffforce'].std(), 3)
    mean_data['mean_corrected_tearoffforce']    = round((df_data['tearoffforce']*df_data['correction_factor_tearoffforce']).mean(), 3)
    mean_data['std_corrected_tearoffforce']     = round((df_data['tearoffforce']*df_data['correction_factor_tearoffforce']).std(), 3)
    # total_number                                = len(df_data)

    data_dict = []
    for i in range(len(df_data)):
        grade_code = df_data.iloc[i]['grade']
        data_dict.append({
            "position": "Position " + str(i),
            "typeOfBreakage": str(df_grades['description'][df_grades['code'] == int(grade_code)].to_string(header=False, index=False)),
            "forceMeasured": df_data.iloc[i]['tearoffforce'],
            "forceCorrected": df_data.iloc[i]['tearoffforce']*df_data.iloc[i]['correction_factor_tearoffforce'],
        })

    class UploadForm(FlaskForm):
        # run_type        = StringField(u'run_type', validators=[DataRequired()])
        # location        = StringField(u'Location', validators=[DataRequired()])
        # operator        = StringField(u'operator', validators=[DataRequired()])
        # StringField(u'run_type', validators=[DataRequired()])
        submit          = SubmitField("I confirm to upload")

    form = UploadForm()
    id = measurement_id

    templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_module_bond_pull_test.xml")
    data = PulltestMeasurement.query.filter_by(id=id).first()
    datapoints = PulltestResult.query.filter_by(id=id).all()
    xml_data = {'runType': 'mod_bareModule',
                'location': "Aachen",
                'operator': "Aachen Database Account",
                'date': data.timestamp,
                'comment':  data.comment if data.comment != None else '',
                'structure': "2S Module",
                'id': data.Module_Overview.cern_module_id,
                'data': data_dict,
                'summary': {
                    'STATION': 'Aachen 3B Bondtester',
                    'AV_TEMP_DEGC': data.air_temperature,
                    'AV_RH_PRCNT': data.air_humidity
                    }
                }

    output = jTemplate.render(xml_data)
    xml_file = output

    if form.validate_on_submit():
        file_path = 'application/static/data/xml/pulltest/output_' + "2S Module" + " - " + str(data.timestamp) + " - " + str(data.Module_Overview.cern_module_id) + " - " + str(id) + '.xml'
        outFileH = open(file_path, 'w')
        outFileH.write(output)
        outFileH.close()


        print("try uploading data from cern db")
        import sys
        from io import StringIO
        sys.path.append('application/scripts')
        from cmsdbldr_client import LoaderClient
        url = "https://cmsdca.cern.ch/trk_loader/trker/cmsr"

        cli = LoaderClient()
        cmd = ['--url', url, '--test', '--login', file_path]

        responseCode = cli.run(cmd)
        if responseCode == 1:
            responseCode = "Upload successful"
            status_color = 'success'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=True, cmsdb_upload_timestamp=datetime.now()))
            db.session.commit()
        elif responseCode == 4:
            responseCode = "Upload failed"
            status_color = 'danger'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()
        else:
            status_color = 'warning'
            PulltestMeasurement.query.filter_by(id=id).update(dict(cmsdb_upload_status=False))
            db.session.commit()


        print(responseCode)
        flash('Submission done - answer code of the server is ' + str(responseCode),  status_color)

        return redirect(url_for("flask_pulltest.pulltest_detail", measurement_number=measurement_id))

    return render_template("pulltest_upload.html", data=data, form=form, xml_file=xml_file)

