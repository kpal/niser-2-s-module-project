import os
import jinja2
import json
import numpy as np
import paramiko
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG
import pandas as pd
import json
import plotly
import plotly.express as px
import plotly.graph_objects as go
##########################
##########################
from application        import app, db, bcrypt, mail
from application.models import *

flask_metrology = Blueprint('flask_metrology', __name__, template_folder="templates/metrology", url_prefix="/metrology")

green_color = "#A5D6A7"   # if value is good
red_color   = "#F48FB1"     # if value is bad
grey_color  = "grey"    # if unclear or not checked

##########################
##########################
##########################
##########################
##########################
##########################
##########################
# FUNCTIONS
##########################
def create_sensor_xml(id):
    templateFilePath = jinja2.FileSystemLoader('application/xml/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_module_metrology.xml")
    data = MetrologySensorSensorAlignment.query.filter_by(id=id).first()
    xml_data = {'runType': 'Test',
                'location': data.Station,
                'operator': data.Operator,
                'date': data.Datetime,
                'comment': data.comment
                }

    output = jTemplate.render(xml_data)
    outFileH = open('application/static/data/xml/metrology_sensor_sensor_output_' + id + '.xml', 'w')
    outFileH.write(output)
    outFileH.close()

def create_bridge_xml(id):
    templateFilePath = jinja2.FileSystemLoader('application/xml/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_module_metrology.xml")
    data = MetrologySensorBridgeAlignment.query.filter_by(id=id).first()
    xml_data = {'runType': 'Test',
                'location': data.Station,
                'operator': data.Operator,
                'date': data.Datetime
                }

    output = jTemplate.render(xml_data)
    outFileH = open('application/static/data/xml/metrology_sensor_bridge_output_' + id + '.xml', 'w')
    outFileH.write(output)
    outFileH.close()

def datatables_json(db_table_class):
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

def datatables_json_all(db_table_class):
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_all() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

##########################
##########################
##########################
##########################
##########################
##########################
# ROUTES
##########################

@flask_metrology.route('/get_data', methods=['POST'])
# trigger external script to update and pull new data from the Metrology PC
def metrology_get_data():
    if request.method == 'POST':
        os.system("python application/scripts/metrology_get_data.py")
    flash('Submission successful GET DATA script is performed!', 'success')
    return redirect(url_for("flask_metrology.metrology_overview"))


@flask_metrology.route('/create_xml', methods=['POST'])
# function to create XML file out of template for given id
def metrology_create_xml():
    if request.method == 'POST':
        id = request.form['id']
        type = request.form['type']
        if type == "sensor":
            print("sensor", id)
            create_sensor_xml(id)
        if type == "bridge":
            print("sensor", id)
            create_bridge_xml(id)
    return "create xml form"


##########################
##########################
# Main Views
##########################
##########################

@flask_metrology.route('/overview')
# Metrology overview page - data is added by table ajax request
def metrology_overview():
    return render_template("metrology_overview.html")

@flask_metrology.route('/overview/data_sensor')
def metrology_overview_data_sensor():
    db_table_class = MetrologySensorSensorAlignment
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(measurement_status=None)

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_metrology.route('/overview/data_bridge')
def metrology_overview_data_bridge():
    db_table_class = MetrologySensorBridgeAlignment
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(measurement_status=None)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_metrology.route('/overview/data_height')
def metrology_overview_data_height():
    db_table_class = MetrologyHeightProfile
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(measurement_status=None)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

##########################
##########################
# Per Setup Views
##########################
##########################
@flask_metrology.route('/sensor')
def metrology_sensor():
    return render_template("metrology_sensor.html")



@flask_metrology.route('/sensor/data')
def metrology_sensor_data():
    json_data = datatables_json_all(MetrologySensorSensorAlignment)
    return json_data

@flask_metrology.route('/bridge')
def metrology_bridge():
    return render_template("metrology_bridge.html")

@flask_metrology.route('/bridge/data')
def metrology_bridge_data():
    json_data = datatables_json_all(MetrologySensorBridgeAlignment)
    return json_data

@flask_metrology.route('/height')
def metrology_height():
    return render_template("metrology_height.html")

@flask_metrology.route('/height/data')
def metrology_height_data():
    json_data = datatables_json_all(MetrologyHeightProfile)
    return json_data

##########################
##########################
# Per Measurement views
##########################
##########################
@flask_metrology.route('/sensor_<int:sensor_number>', methods=["GET", "POST"])
def metrology_sensor_detail(sensor_number):
    MetrologySensorSensorAlignment_data = MetrologySensorSensorAlignment.query.filter_by(id=sensor_number).first()
    image_path_db = MetrologySensorSensorAlignment_data.image_folder
    image_path = "application/static/data/metrology/" + str(image_path_db)
    # print("metrology sensor")
    try:
        image_files = os.listdir(image_path)
        # print(image_files)

        image_list = []
        for image in image_files:
            image_file = image_path + '/' + image
            if os.path.isdir(image_file) == False and str(image_file[-3:]) == 'png':
                image_list.append(image_path_db + image)
    except Exception as e:
        print(e)
        image_list = []

    print(image_list)
    # query data from general tables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.identifier)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.filter_by(setup_metrology=True).all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    from application import engine
    # show histogram:
    df = pd.read_sql_query(
        sql=db.select([MetrologySensorSensorAlignment.rotation_urad,
                       MetrologySensorSensorAlignment.x_shift_um,
                       MetrologySensorSensorAlignment.y_shift_um,
                       MetrologySensorSensorAlignment.automatic_grade]),
        con=engine
    )

    # print("Type:", type(df))
    # print("Type:", df)
    # print(MetrologySensorSensorAlignment_data.rotation_urad)
    fig = px.histogram(df, x="rotation_urad", nbins=50)
    fig.add_vline(x=MetrologySensorSensorAlignment_data.rotation_urad, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_rotation = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="x_shift_um", nbins=50)
    fig.add_vline(x=MetrologySensorSensorAlignment_data.x_shift_um, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_x_shift = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="y_shift_um", nbins=50)
    fig.add_vline(x=MetrologySensorSensorAlignment_data.y_shift_um, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_y_shift = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)


    limit_data = MetrologyLimit.query.order_by("timestamp").limit(1).all()
    for limit in limit_data:
        limit_rotation  = limit.rotation_urad
        limit_x_shift   = limit.x_shift_um
        limit_y_shift   = limit.y_shift_um

    limit_colors = {}
    if MetrologySensorSensorAlignment_data.rotation_urad >= limit_rotation:
        limit_colors["rotation"] = red_color
    else:
        limit_colors["rotation"] = green_color
    if MetrologySensorSensorAlignment_data.x_shift_um >= limit_x_shift:
        limit_colors["x_shift"] = red_color
    else:
        limit_colors["x_shift"] = green_color
    if MetrologySensorSensorAlignment_data.y_shift_um >= limit_y_shift:
        limit_colors["y_shift"] = red_color
    else:
        limit_colors["y_shift"] = green_color
    if 19.0 <= MetrologySensorSensorAlignment_data.air_temperature < 24.0:
        limit_colors["temp"]    = green_color
    else:
        limit_colors["temp"] = red_color
    if 42.0 <= MetrologySensorSensorAlignment_data.air_humidity < 65.0:
        limit_colors["hum"]    = green_color
    else:
        limit_colors["hum"] = red_color

    # print(limit_colors)
    class metrology_sensor_detailForm(FlaskForm):
        operator        = SelectField(u'Operator', default=MetrologySensorSensorAlignment_data.operator, choices=operator_list, validators=[DataRequired()])
        station         = SelectField(u'Station', default=MetrologySensorSensorAlignment_data.station, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'Grade', default=MetrologySensorSensorAlignment_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        status          = SelectField(u'Station', default=MetrologySensorSensorAlignment_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=MetrologySensorSensorAlignment_data.comment)
        submit          = SubmitField("Update")

    form = metrology_sensor_detailForm()
    if form.validate_on_submit():
        flash('Submission successfull for!', 'success')
        # print(form.operator.data, form.station.data, form.comment.data)
        MetrologySensorSensorAlignment_data.operator    = form.operator.data
        MetrologySensorSensorAlignment_data.station     = form.station.data
        MetrologySensorSensorAlignment_data.manual_grade       = form.grade.data
        MetrologySensorSensorAlignment_data.measurement_status = form.status.data
        MetrologySensorSensorAlignment_data.comment     = form.comment.data
        db.session.add(MetrologySensorSensorAlignment_data)
        db.session.commit()

    return render_template("metrology_sensor_detail.html", limit_colors = limit_colors, graphJSON_list=[fig_rotation,fig_x_shift, fig_y_shift], data=MetrologySensorSensorAlignment_data, images=sorted(image_list), form=form)

@flask_metrology.route('/sensor_all_plots_<int:sensor_number>', methods=["GET", "POST"])
def metrology_sensor_detail_all_plots(sensor_number):
    MetrologySensorSensorAlignment_data = MetrologySensorSensorAlignment.query.filter_by(id=sensor_number).first()
    image_path_db = MetrologySensorSensorAlignment_data.image_folder
    image_path = "application/static/data/metrology/" + image_path_db + ""
    print(image_path)
    image_files = os.listdir(image_path)

    image_list_1 = []
    image_list_2 = []
    subfolder_list = []
    for image in image_files:
        image_file = image_path + '/' + image
        if os.path.isdir(image_file) == False and str(image_file[-3:]) == 'png':
            image_list_1.append(image_path_db + image)
        if os.path.isdir(image_file) == True:
            image_path_new = image_path + "/" + image + "/"
            image_files_new = os.listdir(image_path_new)
            subfolder_list.append(image)
            for subimages in image_files_new:
                subimages_file = image_path_new + "/" + subimages
                if os.path.isdir(subimages_file) == False and str(subimages_file[-3:]) == 'png':
                    image_list_2.append(image_path_db + image + "/" + subimages)


    return render_template("metrology_sensor_all_plots.html", data=MetrologySensorSensorAlignment_data, images_1=sorted(image_list_1), images_2=sorted(image_list_2), subfolder_list=np.unique(subfolder_list))

@flask_metrology.route('/bridge_<int:sensor_number>', methods=["GET", "POST"])
def metrology_bridge_detail(sensor_number):
    MetrologySensorBridgeAlignment_data = MetrologySensorBridgeAlignment.query.filter_by(id=sensor_number).first()

    # query data from general tables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.identifier)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.filter_by(setup_metrology=True).all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    class metrology_sensor_detailForm(FlaskForm):
        operator        = SelectField(u'Operator', default=MetrologySensorBridgeAlignment_data.operator, choices=operator_list, validators=[DataRequired()])
        station         = SelectField(u'Station', default=MetrologySensorBridgeAlignment_data.station, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'Grade', default=MetrologySensorBridgeAlignment_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        status          = SelectField(u'Station', default=MetrologySensorBridgeAlignment_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=MetrologySensorBridgeAlignment_data.comment)
        submit          = SubmitField("Update")

    form = metrology_sensor_detailForm()
    if form.validate_on_submit():
        flash('Submission successfull !', 'success')
        MetrologySensorBridgeAlignment_data.operator                = form.operator.data
        MetrologySensorBridgeAlignment_data.station                 = form.station.data
        MetrologySensorBridgeAlignment_data.manual_grade            = form.grade.data
        MetrologySensorBridgeAlignment_data.measurement_status      = form.status.data
        MetrologySensorBridgeAlignment_data.comment                 = form.comment.data

        db.session.add(MetrologySensorBridgeAlignment_data)
        db.session.commit()

    return render_template("metrology_bridge_detail.html", data=MetrologySensorBridgeAlignment_data, form=form)

@flask_metrology.route('/height_<int:sensor_number>', methods=["GET", "POST"])
def metrology_height_detail(sensor_number):
    MetrologyHeightProfile_data = MetrologyHeightProfile.query.filter_by(id=sensor_number).first()

    image_path_db = MetrologyHeightProfile_data.image_folder
    image_path = "application/static/data/metrology/" + str(image_path_db)

    try:
        image_files = os.listdir(image_path)
        print(image_files)

        image_list = []
        for image in image_files:
            image_file = image_path + '/' + image
            if os.path.isdir(image_file) == False and str(image_file[-3:]) == 'png':
                image_list.append(image_path_db + image)
    except Exception as e:
        print(e)
        image_list = []

    print(image_list)
    # query data from general dables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.identifier)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))


    class metrology_sensor_detailForm(FlaskForm):
        operator    = SelectField(u'Operator', default=MetrologyHeightProfile_data.operator, choices=operator_list, validators=[DataRequired()])
        station     = SelectField(u'Station', default=MetrologyHeightProfile_data.station, choices=station_list, validators=[DataRequired()])
        grade       = SelectField(u'Grade', default=MetrologyHeightProfile_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        status      = SelectField(u'Station', default=MetrologyHeightProfile_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment     = TextAreaField(u'Comment')
        submit      = SubmitField("Update")

    form = metrology_sensor_detailForm()
    if form.validate_on_submit():
        flash('Submission successfull for!', 'success')
        print(form.operator.data, form.station.data, form.comment.data)
        MetrologyHeightProfile_data.operator                = form.operator.data
        MetrologyHeightProfile_data.station                 = form.station.data
        MetrologyHeightProfile_data.measurement_status      = form.status.data
        MetrologyHeightProfile_data.manual_grade                   = form.grade.data
        MetrologyHeightProfile_data.comment                 = form.comment.data
        db.session.add(MetrologyHeightProfile_data)
        db.session.commit()

    return render_template("metrology_height_detail.html", data=MetrologyHeightProfile_data, form=form, images=image_list)