import os
import logging
from flask                      import Flask
from flask_sqlalchemy           import SQLAlchemy
from flask_bcrypt               import Bcrypt
from flask_mail                 import Mail
from flask_login                import LoginManager
from flask_migrate              import Migrate

app                                             = Flask(__name__, static_folder="./static", template_folder="./templates")
app.secret_key                                  = os.urandom(32)     # generate session key, by random 32 character key

# Set Database
app.config['SQLALCHEMY_DATABASE_URI']           = os.environ['DB_TYPE']+'://'+os.environ['DB_USER']+':'+os.environ['DB_PASSWORD']+'@'+os.environ['DB_HOSTNAME'] + ':'+os.environ['DB_PORT']+'/'+os.environ['DB_Database']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']    = False
app.config["UPLOAD_FOLDER"]                     = "data"
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
engine = db.create_engine(os.environ['DB_TYPE']+'://'+os.environ['DB_USER']+':'+os.environ['DB_PASSWORD']+'@'+os.environ['DB_HOSTNAME'] + ':'+os.environ['DB_PORT']+'/'+os.environ['DB_Database'])

# Set Logging
logging.basicConfig(filename = 'Flask.log', level=logging.DEBUG, format =f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
app.logger.info('--------------------------')
app.logger.info('APP Started')
app.logger.info('LOGGING activated')


# Set EMAIL
app.config['MAIL_SERVER']                       ='email_server'
app.config['MAIL_PORT']                         = 465
app.config['MAIL_USERNAME']                     = 'noreply@example.com'
app.config['MAIL_PASSWORD']                     = 'email_password'
app.config['MAIL_USE_TLS']                      = False
app.config['MAIL_USE_SSL']                      = True
mail = Mail(app)


# Set Login Manager
login_manager                                   = LoginManager(app)
login_manager.login_view                        = 'login'
login_manager.login_message_category            = 'info'


import git
#repo = git.Repo(search_parent_directories=True)
#sha = repo.head.object.hexsha
#sha = str(000000000000000000000000)
import subprocess
process = subprocess.Popen(['git', 'rev-parse', 'HEAD'], shell=False, stdout=subprocess.PIPE)
git_head_hash = process.communicate()[0].strip()
sha = git_head_hash.decode()
app.jinja_env.globals['GIT_COMMIT_HASH8'] = sha[:8]
app.jinja_env.globals['GIT_COMMIT_HASH'] = sha
# set all applications and routes
from application import routes
