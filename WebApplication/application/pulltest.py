import os
import re
import math
import json
import simplejson
import jinja2
import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import paramiko
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField
from wtforms import DateField, DateTimeField, TimeField, SelectField, DecimalField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp, Optional
from stat import S_ISDIR, S_ISREG
from flask import Blueprint
from flask                          import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask                          import send_file
from application.models             import *
from application                    import app, db, bcrypt, mail
from application                    import engine

flask_pulltest = Blueprint('flask_pulltest', __name__, template_folder="templates/pulltest", url_prefix="/pulltest")

green_color = "#A5D6A7"   # if value is good
red_color   = "#F48FB1"     # if value is bad
grey_color  = "grey"    # if unclear or not checked


def create_pulltest_xml(id):
    templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_sensor_bond_pull_test.xml")
    data = PulltestResults.query.filter_by(id=id).first()
    xml_data = {'runType': 'Test',
                'location': data.Station,
                'operator': data.Operator,
                'date': data.Datetime,
                'comment': data.comment
                }

    output = jTemplate.render(xml_data)
    outFileH = open('application/static/data/xml/pulltest/output_' + id + '.xml', 'w')
    outFileH.write(output)
    outFileH.close()

@flask_pulltest.route('/get_data', methods=['POST'])
def pulltest_getdata():
    flash('Submission successful SYNC DB script is now performed! - do not reload this page until confirmation', 'warning')
    os.system("python application/scripts/pulltest_get_data.py")
    flash('Submission successful SYNC DB script is performed!', 'success')
    return redirect(url_for("flask_pulltest.pulltest_overview"))

@flask_pulltest.route('/create_xml', methods=['POST'])
def pulltest_create_xml():
    if request.method == 'POST':
        flash('Submission successful XML file is getting created!', 'success')
        id = request.form['id']
        create_pulltest_xml(id)
    return render_template("pulltest_overview.html")

@flask_pulltest.route('/overview')
def pulltest_overview():
    return render_template("pulltest_overview.html")

@flask_pulltest.route('/overview/data_module')
def pulltest_module_data_pending():
    db_table_class = PulltestMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

# @flask_pulltest.route('/overview/data_HV')
# def pulltest_HV_data_pending():
#     db_table_class = PulltestMeasurement
#     column_list = []
#     for column_name in db_table_class.__dict__:
#         column_list.append(column_name)
#     # global filter
#     query = db_table_class.query.filter_by(is_HV=True)
#     total_filtered = query.count()
#     # sorting
#     order = []
#     i = 0
#     while True:
#         col_index = request.args.get(f'order[{i}][column]')
#         if col_index is None:
#             break
#         col_name = request.args.get(f'columns[{col_index}][data]')
#         if col_name not in column_list:
#             col_name = 'id'
#         descending = request.args.get(f'order[{i}][dir]') == 'desc'
#         col = getattr(db_table_class, col_name)
#         if descending:
#             col = col.desc()
#         order.append(col)
#         i += 1
#     if order:
#         query = query.order_by(*order)
#
#     # pagination
#     start = request.args.get('start', type=int)
#     length = request.args.get('length', type=int)
#     query = query.offset(start).limit(length)
#     # json data
#     json_data = {'data': [data.to_dict() for data in query],
#         'recordsFiltered': total_filtered,
#         'recordsTotal': db_table_class.query.count(),
#         'draw': request.args.get('draw', type=int),
#          }
#     json_string = json.dumps(json_data, default=str)
#     json_string = json_string.replace("NaN", "null")
#     json_data = json.loads(json_string)
#     return json_data

@flask_pulltest.route('/module')
def pulltest_module():
    return render_template("pulltest_module.html")

@flask_pulltest.route('/module/data')
def pulltest_module_data_all():
    db_table_class = PulltestMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(is_module=True)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    # query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
@flask_pulltest.route('/hv')
def pulltest_HV():
    return render_template("pulltest_HV.html")

@flask_pulltest.route('/HV/data')
def pulltest_HV_data_all():
    db_table_class = PulltestMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(is_HV=True)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_pulltest.route('/id_<int:measurement_number>', methods=["GET", "POST"])
def pulltest_detail(measurement_number):
    PulltestResults_data = PulltestMeasurement.query.filter_by(id=measurement_number).first()

    # query data from general dables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
        if station.name == "Pulltester":
            station_default = (station.id, station.name)
        else:
            station_default = PulltestResults_data.station

    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    assemblystep_data = GeneralAssemblystep.query.all()
    value = []
    name = []
    for step in assemblystep_data:
        value.append(step.run_type)
        name.append((str(step.number) + ". " + str(step.name) + " (" + str(step.run_type) + ")"))
    assemblystep_list = list(zip(value, name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.cern_module_id)
    module_list = list(zip(module_value, module_name))


    df_data = pd.read_sql_query('''SELECT tearoffforce, "correction_factor_tearoffforce", grade  FROM "Pulltest_Results" WHERE pulltest_measurement_id = ''' + str(measurement_number) + ";", con=engine)
    print(df_data)
    mean_data = {}
    mean_data['mean_tearoffforce']              = round(df_data['tearoffforce'].mean(), 3)
    mean_data['std_tearoffforce']               = round(df_data['tearoffforce'].std(), 3)
    mean_data['mean_corrected_tearoffforce']    = round(df_data['tearoffforce'].mean()*df_data['correction_factor_tearoffforce'].mean(), 3)
    mean_data['std_corrected_tearoffforce']     = round(df_data['tearoffforce'].std()*df_data['correction_factor_tearoffforce'].std(), 3)
    total_number                                = len(df_data)

    try:
        mean_data['Grade1'] = round(len(df_data[df_data['grade']=='1'])/total_number*100, 1)
        mean_data['Grade2'] = round(len(df_data[df_data['grade']=='2'])/total_number*100, 1)
        mean_data['Grade3'] = round(len(df_data[df_data['grade']=='3'])/total_number*100, 1)
        mean_data['Grade4'] = round(len(df_data[df_data['grade']=='4'])/total_number*100, 1)
        mean_data['Grade5'] = round(len(df_data[df_data['grade']=='5'])/total_number*100, 1)
    except:
        pass
    print(mean_data)

    # make plots
    # show histogram:
    df = pd.read_sql_query(
        sql=db.select([PulltestResult.tearoffforce,
                       PulltestResult.correction_factor_tearoffforce,
                       PulltestResult.grade,
                        ]),
        con=engine
    )

    print("Type:", type(df))
    print("Type:", df)
    fig = px.histogram(df, x="tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="correction_factor_tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_corrected_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="grade", nbins=50)
    # fig.add_vline(x=PulltestResults_data.grade, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)



    limit_colors = {}
    try:
        limit_colors['temp'] = green_color if 19.0 <= PulltestResults_data.air_temperature < 24.0 else red_color
        limit_colors['hum'] = green_color if 45.0 <= PulltestResults_data.air_humidity < 65.0 else red_color
    except:
        limit_colors['temp'] = "#ffffff0"
        limit_colors['hum'] = "#ffffff0"

    class pulltest_detailForm(FlaskForm):
        operator        = SelectField(u'Operator', default=PulltestResults_data.operator, choices=operator_list, validators=[DataRequired()])
        station         = SelectField(u'Station', default=station_default, choices=station_list, validators=[DataRequired()])
        assemblystep    = SelectField(u'Assemblystep', choices=assemblystep_list)
        module          = SelectField(u'Module', default=PulltestResults_data.module, choices=module_list, validators=[DataRequired()])
        grade           = SelectField(u'Grade', default=PulltestResults_data.manual_grade, choices=grade_list, validators=[])
        status          = SelectField(u'Station', default=PulltestResults_data.measurement_status, choices=status_list, validators=[])
        comment         = TextAreaField(u'Comment', default=PulltestResults_data.comment)
        submit          = SubmitField("Update")

    form = pulltest_detailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        print(form.operator.data, form.station.data, form.comment.data)
        PulltestResults_data.operator           = form.operator.data
        PulltestResults_data.station            = form.station.data
        PulltestResults_data.module             = form.module.data
        PulltestResults_data.measurement_status = form.status.data
        PulltestResults_data.comment            = form.comment.data
        PulltestResults_data.manual_grade       = form.grade.data
        PulltestResults_data.assemblystep       = form.assemblystep.data

        db.session.add(PulltestResults_data)
        db.session.commit()
        return redirect(url_for("flask_pulltest.pulltest_overview"))

    return render_template("pulltest_detail.html", graphJSON_list=[fig_tearoffforce, fig_corrected_tearoffforce, fig_pulltest_db_grade],
                           limit_colors = limit_colors,
                           data=PulltestResults_data,
                           mean_data=mean_data,
                           form=form,
                           module_list=module_list)

@flask_pulltest.route('/id_<int:measurement_number>/data', methods=["GET", "POST"])
def pulltest_detail_data(measurement_number):
    db_table_class = PulltestResult
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(pulltest_measurement_id=measurement_number)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data


@flask_pulltest.route('/product', methods=["GET", "POST"])
def pulltest_product():

    class pulltestproductForm(FlaskForm):
        id = IntegerField(u'ID')
        used_bool_status        = SelectField(u'Used for 2S Modules', choices=[(1,"True"), (0, "False")], validators=[DataRequired()])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Submit / Update")

    form = pulltestproductForm()
    if form.validate_on_submit():
        flash('Submission successfull!', 'success')
        product_id = form.id.data
        PulltestProduct_data = PulltestProduct.query.filter_by(id=product_id).first()
        PulltestProduct_data._2S_Modules_used = bool(int(form.used_bool_status.data))
        PulltestProduct_data.comment=form.comment.data
        db.session.add(PulltestProduct_data)
        db.session.commit()
        return redirect(url_for("flask_pulltest.pulltest_product"))
    return render_template("pulltest_product.html", form=form)

@flask_pulltest.route('/product/data')
def pulltest_product_data():
    query = PulltestProduct.query.all()
    total_filtered = PulltestProduct.query.count()
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = PulltestProduct.query.offset(start).limit(length).all()
    # print([data.to_dict() for data in query])
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': PulltestProduct.query.count(),
        'draw': request.args.get('draw', type=int),
    }


@flask_pulltest.route('/correctionfactor', methods=["GET", "POST"])
def pulltest_correctionfactor():

    class pulltestcorrectionfactorForm(FlaskForm):
        correction_factor_module    = DecimalField(u'Module correction factor', validators=(Optional(),))
        correction_factor_HV        = DecimalField(u'HV correction factor', validators=(Optional(),))
        comment                     = TextAreaField(u'Comment', validators=(Optional(),))
        submit                      = SubmitField("Submit / Update")

    form = pulltestcorrectionfactorForm()
    if form.validate_on_submit():
        flash('Submission successful!', 'success')
        correction_factor = PulltestCorrectionfactor(
            correction_factor_module    = form.correction_factor_module.data,
            correction_factor_HV        = form.correction_factor_HV.data,
            comment                     = form.comment.data
        )

        db.session.add(correction_factor)
        db.session.commit()
        return redirect(url_for("flask_pulltest.pulltest_correctionfactor"))
    return render_template("pulltest_correctionfactor.html", form=form)

@flask_pulltest.route('/correctionfactor/data')
def pulltest_correctionfactor_data():
    query = PulltestCorrectionfactor.query.all()
    total_filtered = PulltestCorrectionfactor.query.count()
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = PulltestCorrectionfactor.query.offset(start).limit(length).all()
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': PulltestCorrectionfactor.query.count(),
        'draw': request.args.get('draw', type=int),
    }



# @flask_pulltest.route('/pulltest_data_valid')
# # API page to deliver data to Overview website
# def pulltest_data_valid():
#     query = PulltestResults.query.filter_by(status="1")
#     total_filtered = query.count()
#     # pagination
#     start = request.args.get('start', type=int)
#     length = request.args.get('length', type=int)
#     query = query.offset(start).limit(length)
#
#     json_data = {'data': [data.to_dict_reference() for data in query],
#         'recordsFiltered': total_filtered,
#         'recordsTotal': PulltestResults.query.count(),
#         'draw': request.args.get('draw', type=int),
#          }
#     json_string = json.dumps(json_data, default=str)
#     json_string = json_string.replace("NaN", "null")
#     print(json_string)
#     json_data = json.loads(json_string)
#     return json_data

# @flask_pulltest.route('/module_id_<int:measurement_number>', methods=["GET", "POST"])
# def pulltest_detail_module(measurement_number):
#     PulltestResults_data = PulltestResultsModule.query.filter_by(id=measurement_number).first()
#
#     # query data from general dables
#     station_data = GeneralStation.query.all()
#     station_list_id = []
#     station_list_name = []
#     for station in station_data:
#         station_list_id.append(station.id)
#         station_list_name.append(station.name)
#     station_list = list(zip(station_list_id, station_list_name))
#
#     operator_data = GeneralOperators.query.all()
#     operator_list_id = []
#     operator_list_name = []
#     for operator in operator_data:
#         operator_list_id.append(operator.id)
#         operator_list_name.append(str(operator.family_name + " " + operator.first_name))
#     operator_list = list(zip(operator_list_id, operator_list_name))
#
#     status_data = GeneralStatus.query.all()
#     status_value = []
#     status_name = []
#     for status in status_data:
#         status_value.append(status.id)
#         status_name.append(status.name)
#     status_list = list(zip(status_value, status_name))
#
#     grade_data = GeneralGrade.query.all()
#     grade_list_id = []
#     grade_list_name = []
#     for grade in grade_data:
#         grade_list_id.append(grade.id)
#         grade_list_name.append(str(grade.value))
#     grade_list = list(zip(grade_list_id, grade_list_name))
#
#     module_data = ModuleOverview.query.all()
#     module_value = []
#     module_name = []
#     for module in module_data:
#         module_value.append(module.id)
#         module_name.append(module.aachen_module_id)
#     module_list = list(zip(module_value, module_name))
#
#     # make plots
#     from application import engine
#     # show histogram:
#     df = pd.read_sql_query(
#         sql=db.select([PulltestResultsModule.pulltest_db_tearoffforce,
#                        PulltestResultsModule.corrected_tearoffforce,
#                        PulltestResultsModule.pulltest_db_gradecode,
#                         ]),
#         con=engine
#     )
#
#     print("Type:", type(df))
#     print("Type:", df)
#     fig = px.histogram(df, x="pulltest_db_tearoffforce", nbins=50)
#     fig.add_vline(x=PulltestResults_data.pulltest_db_tearoffforce, line_width=3, line_dash="solid", line_color="red")
#     fig.update_layout(bargap=0.1)
#     fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#
#     fig = px.histogram(df, x="corrected_tearoffforce", nbins=50)
#     fig.add_vline(x=PulltestResults_data.corrected_tearoffforce, line_width=3, line_dash="solid", line_color="red")
#     fig.update_layout(bargap=0.1)
#     fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#
#     fig = px.histogram(df, x="pulltest_db_gradecode", nbins=50)
#     fig.add_vline(x=PulltestResults_data.pulltest_db_gradecode, line_width=3, line_dash="solid", line_color="red")
#     fig.update_layout(bargap=0.1)
#     fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#
#
#     limit_colors = {}
#     try:
#         limit_colors['temp'] = green_color if 19.0 <= PulltestResults_data.air_temperature < 24.0 else red_color
#         limit_colors['hum'] = green_color if 45.0 <= PulltestResults_data.air_humidity < 65.0 else red_color
#     except:
#         limit_colors['temp'] = "#ffffff0"
#         limit_colors['hum'] = "#ffffff0"
#
#     class pulltest_detailForm(FlaskForm):
#         operator        = SelectField(u'Operator', default=PulltestResults_data.operator, choices=operator_list, validators=[DataRequired()])
#         station         = SelectField(u'Station', default=PulltestResults_data.station, choices=station_list, validators=[DataRequired()])
#         grade           = SelectField(u'Grade', default=PulltestResults_data.manual_grade, choices=grade_list, validators=[DataRequired()])
#         status          = SelectField(u'Station', default=PulltestResults_data.measurement_status, choices=status_list, validators=[DataRequired()])
#         comment         = TextAreaField(u'Comment', default=PulltestResults_data.comment)
#         submit          = SubmitField("Update")
#
#     form = pulltest_detailForm()
#     if form.validate_on_submit():
#         flash('Submission successful !', 'success')
#         print(form.operator.data, form.station.data, form.comment.data)
#         PulltestResults_data.operator           = form.operator.data
#         PulltestResults_data.station            = form.station.data
#         PulltestResults_data.module             = form.module_reference.data
#         PulltestResults_data.status             = form.status.data
#         PulltestResults_data.comment            =form.comment.data
#
#         db.session.add(PulltestResults_data)
#         db.session.commit()
#         return redirect(url_for("flask_pulltest.pulltest_overview"))
#
#     return render_template("pulltest_detail.html", graphJSON_list=[fig_tearoffforce, fig_corrected_tearoffforce, fig_pulltest_db_grade], limit_colors = limit_colors, data=PulltestResults_data, form=form)

# @flask_pulltest.route('/HV_id_<int:measurement_number>', methods=["GET", "POST"])
# def pulltest_detail_HV(measurement_number):
#     PulltestResults_data = PulltestResultsHV.query.filter_by(id=measurement_number).first()
#
#     # query data from general dables
#     station_data = GeneralStation.query.all()
#     station_list_id = []
#     station_list_name = []
#     for station in station_data:
#         station_list_id.append(station.id)
#         station_list_name.append(station.Station_name)
#     station_list = list(zip(station_list_id, station_list_name))
#
#     operator_data = GeneralOperators.query.all()
#     operator_list_id = []
#     operator_list_name = []
#     for operator in operator_data:
#         operator_list_id.append(operator.id)
#         operator_list_name.append(str(operator.Family_Name + " " + operator.First_Name))
#     operator_list = list(zip(operator_list_id, operator_list_name))
#
#     status_data = GeneralStatus.query.all()
#     status_value = []
#     status_name = []
#     for status in status_data:
#         status_value.append(status.Value)
#         status_name.append(status.Name)
#     status_list = list(zip(status_value, status_name))
#
#     module_data = ModuleOverview.query.all()
#     module_value = []
#     module_name = []
#     for module in module_data:
#         module_value.append(module.id)
#         module_name.append(module.Aachen_Module_id)
#     module_list = list(zip(module_value, module_name))
#
#     class pulltest_detailForm(FlaskForm):
#         operator        = SelectField(u'Operator', default=PulltestResults_data.operator, choices=operator_list, validators=[DataRequired()])
#         module_reference= SelectField(u'Module', default=PulltestResults_data.module, choices=module_list, validators=[DataRequired()])
#         station         = SelectField(u'Station', default=PulltestResults_data.station, choices=station_list, validators=[DataRequired()])
#         status          = SelectField(u'Status', default=PulltestResults_data.status, choices=status_list, validators=[DataRequired()])
#         comment         = TextAreaField(u'Comment', default=PulltestResults_data.comment)
#         submit          = SubmitField("Update")
#
#     form = pulltest_detailForm()
#     if form.validate_on_submit():
#         flash('Submission successful !', 'success')
#         print(form.operator.data, form.station.data, form.comment.data)
#         PulltestResults_data.operator           = form.operator.data
#         PulltestResults_data.station            = form.station.data
#         PulltestResults_data.module             = form.module_reference.data
#         PulltestResults_data.status             = form.status.data
#         PulltestResults_data.comment            =form.comment.data
#
#         db.session.add(PulltestResults_data)
#         db.session.commit()
#         return redirect(url_for("flask_pulltest.pulltest_overview"))
#
#     return render_template("pulltest_detail.html", data=PulltestResults_data, form=form)

# @flask_pulltest.route('/<type>_timestamp_<timestamp_value>', methods=["GET", "POST"])
# def pulltest_detail_timestamp(type, timestamp_value):
#     if type == "module":
#         PulltestResults_data = PulltestResultsModule.query.filter_by(timestamp=timestamp_value).all()
#     if type == "HV":
#         PulltestResults_data = PulltestResultsHV.query.filter_by(timestamp=timestamp_value).all()
#
#     tearoffforce_list = []
#     corrected_tearoffforce = []
#     for value in PulltestResults_data:
#         tearoffforce_list.append(value.pulltest_db_tearoffforce)
#         corrected_tearoffforce.append(value.corrected_tearoffforce)
#
#     data = {}
#     data['mean_tearoffforce'] = round(np.mean(tearoffforce_list), 5)
#     data['std_tearoffforce'] = round(np.std(tearoffforce_list), 5)
#     data['mean_corrected_tearoffforce'] = round(np.mean(corrected_tearoffforce), 5)
#     data['std_corrected_tearoffforce'] = round(np.std(corrected_tearoffforce), 5)
#
#
#     # print(PulltestResults_data)
#     # data = 0
#     # make plots
#     # from application import engine
#     # df = pd.read_sql_query('SELECT "pulltest_db_tearoffforce", "corrected_tearoffforce", "pulltest_db_gradecode" \
#     #                         FROM "Pulltest_results_module" \
#     #                         WHERE "Pulltest_results_module.timestamp" LIKE \"' + str(timestamp_value) + ' \"', \
#     #                        con=engine)
#     #
#
#     # print("Type:", type(df))
#     # print("Type:", df)
#     # fig = px.histogram(df, x="pulltest_db_tearoffforce", nbins=50)
#     # fig.add_vline(x=PulltestResults_data.pulltest_db_tearoffforce, line_width=3, line_dash="solid", line_color="red")
#     # fig.update_layout(bargap=0.1)
#     # fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#     #
#     # fig = px.histogram(df, x="corrected_tearoffforce", nbins=50)
#     # fig.add_vline(x=PulltestResults_data.corrected_tearoffforce, line_width=3, line_dash="solid", line_color="red")
#     # fig.update_layout(bargap=0.1)
#     # fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#     #
#     # fig = px.histogram(df, x="pulltest_db_gradecode", nbins=50)
#     # fig.add_vline(x=PulltestResults_data.pulltest_db_gradecode, line_width=3, line_dash="solid", line_color="red")
#     # fig.update_layout(bargap=0.1)
#     # fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#
#     return render_template("pulltest_timestamp.html", data=data, type=type, timestamp_value=timestamp_value)


# @flask_pulltest.route('/<type>_timestamp_<timestamp_value>/data', methods=["GET", "POST"])
# def pulltest_detail_timestamp_data(type, timestamp_value):
#     print(timestamp_value)
#     if type == "module":
#         db_table_class = PulltestResultsModule
#         query = PulltestResultsModule.query.filter_by(timestamp=timestamp_value)
#     if type == "HV":
#         db_table_class = PulltestResultsHV
#         query = PulltestResultsHV.query.filter_by(timestamp=timestamp_value)
#     else:
#         return url_for(pulltest_overview)
#
#
#
#     column_list = []
#     for column_name in db_table_class.__dict__:
#         column_list.append(column_name)
#     # global filter
#     # query = db_table_class.query
#     total_filtered = query.count()
#     # sorting
#     order = []
#     i = 0
#     while True:
#         col_index = request.args.get(f'order[{i}][column]')
#         if col_index is None:
#             break
#         col_name = request.args.get(f'columns[{col_index}][data]')
#         if col_name not in column_list:
#             col_name = 'id'
#         descending = request.args.get(f'order[{i}][dir]') == 'desc'
#         col = getattr(db_table_class, col_name)
#         if descending:
#             col = col.desc()
#         order.append(col)
#         i += 1
#     if order:
#         query = query.order_by(*order)
#
#     # pagination
#     start = request.args.get('start', type=int)
#     length = request.args.get('length', type=int)
#     query = query.offset(start).limit(length)
#     # json data
#     json_data = {'data': [data.to_dict() for data in query],
#         'recordsFiltered': total_filtered,
#         'recordsTotal': db_table_class.query.count(),
#         'draw': request.args.get('draw', type=int),
#          }
#     json_string = json.dumps(json_data, default=str)
#     json_string = json_string.replace("NaN", "null")
#     json_data = json.loads(json_string)
#     return json_data
