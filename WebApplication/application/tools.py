import os
import jinja2
import paramiko
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG

from application                import app, db, bcrypt, mail
from application.models import *


flask_tools = Blueprint('flask_tools', __name__, template_folder="templates/tools", url_prefix="/tools")


@flask_tools.route('/jigs', methods=["GET", "POST"])
def tools_jigs():
    list_of_jigs = [("ToolsJigsBaremodule", "Baremodule"), ("ToolsJigsBonding", "Bonding"), ("ToolsJigsHVpigtaile", "HV pigtaile"), ("ToolsJigshybridegluing", "Hybrid Gluing"), ("ToolsJigskaptongluing", "Kapton gluing")]
    class ToolsAddJigForm(FlaskForm):
        category        = SelectField(u'Category', choices=list_of_jigs, validators=[DataRequired()])
        name            = StringField(u'Name', validators=[DataRequired()])
        number          = IntegerField(u'Number', validators=[DataRequired()])
        barcode         = StringField(u'Barcode', validators=[DataRequired()])
        # status        = SelectField(u'Station', default=IVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Create")

    form = ToolsAddJigForm()
    if form.validate_on_submit():
        flash('Submission successfull!', 'success')
        import types
        def str_to_class(s):
            if s in globals() and isinstance(globals()[s], type):
                return globals()[s]
            return None

        data_class = str_to_class(form.category.data)

        data = data_class(
            name=form.name.data,
            number=form.number.data,
            barcode=form.barcode.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        return redirect(url_for("flask_tools.tools_jigs"))
    return render_template("tool_jigs.html", form=form)

@flask_tools.route('/consumables', methods=["GET", "POST"])
def tools_consumables():
    list_of_consumables = [("1", "Test")]
    class ToolsAddConsumableForm(FlaskForm):
        category = SelectField(u'Category', choices=list_of_consumables, validators=[DataRequired()])
        name = StringField(u'Name', validators=[DataRequired()])
        # status        = SelectField(u'Station', default=IVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment = TextAreaField(u'Comment')
        submit = SubmitField("Create")
    class ToolsAddCategoryForm(FlaskForm):
        category = SelectField(u'Category', choices=list_of_consumables, validators=[DataRequired()])
        name = StringField(u'Name', validators=[DataRequired()])
        # status        = SelectField(u'Station', default=IVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment = TextAreaField(u'Comment')
        submit = SubmitField("Create")

    form = ToolsAddConsumableForm()
    form_category = ToolsAddCategoryForm()

    # if form_category.validate_on_submit():

    if form_category.submit.data and form_category.validate():
        print('cat', form_category.submit.data)
        print('form', form.submit.data)
        flash('Submission successfull!', 'success')

        data = ToolsConsumables(
            name=form_category.name.data,
            comment=form_category.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        return redirect(url_for("flask_tools.tools_consumables"))

    if form.submit.data and form.validate():
        flash('Submission successfull!', 'success')
        consumable_id = ToolsConsumables.query.filter_by(id=form.category.data).first().id
        print(consumable_id)

        data = ToolsConsumablesLog(
            consumable_id=consumable_id,
            name=form.name.data,
            comment=form.comment.data,
        )

        db.session.add(data)
        db.session.commit()
        return redirect(url_for("flask_tools.tools_consumables"))




    return render_template("tool_consumables.html", form=form, form_category=form_category)

@flask_tools.route('/workflow')
def tools_workflow():
    return render_template("tool_workflow.html")


@flask_tools.route('/consumables/data')
def consumables_data():
    query = ToolsConsumables.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsConsumables.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_tools.route('/consumables/data_log')
def consumables_log():
    query = ToolsConsumablesLog.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsConsumablesLog.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_tools.route('/jigs_baremodule/data')
def jigs_baremodule_data():
    query = ToolsJigsBaremodule.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsJigsBaremodule.query.count(),
        'draw': request.args.get('draw', type=int),
    }

@flask_tools.route('/jigs_bonding/data')
def jigs_bonding_data():
    query = ToolsJigsBonding.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsJigsBonding.query.count(),
        'draw': request.args.get('draw', type=int),
    }
@flask_tools.route('/jigs_HVpigtaile/data')
def jigs_HVpigtaile_data():
    query = ToolsJigsHVpigtaile.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsJigsHVpigtaile.query.count(),
        'draw': request.args.get('draw', type=int),
    }
@flask_tools.route('/jigs_hybridegluing/data')
def jigs_hybridegluing_data():
    query = ToolsJigshybridegluing.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsJigshybridegluing.query.count(),
        'draw': request.args.get('draw', type=int),
    }
@flask_tools.route('/jigs_kaptongluing/data')
def jigs_kaptongluing_data():
    query = ToolsJigskaptongluing.query
    total_filtered = query.count()

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    return {
        'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': ToolsJigskaptongluing.query.count(),
        'draw': request.args.get('draw', type=int),
    }