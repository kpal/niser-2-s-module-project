import psycopg2
import re
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import numpy as np
import pandas as pd
import paramiko
from sqlalchemy import create_engine


influx_host           = "http://444.555.666.777:8086"
influx_token          = "TOKEN"
influx_org            = "rwth"

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])

engine = create_engine('postgresql://'+db_username_DCA+':'+db_password_DCA+'@'+db_host_DCA + ':'+db_port_DCA+'/'+db_database_DCA)


# PROBE STATION - IV MEASUREMENT SENSORS
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect("123.123.124.184", username="User", password="Passwort")
sftp = ssh.open_sftp()
probestation_path = "C:/Users/User/Documents/Probe Station/Measurements/Sensors"



for sensors in sftp.listdir(probestation_path):
    print(sensors)
    try:
        for assemblystep in sftp.listdir(probestation_path + "/" + sensors):
            sensor_name = sensors

            # see if sensor is already inside of Database
            sensor_name = sensors
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            # check which metrology measurements are already inside DCA-DB
            cur = conn.cursor()
            cur.execute("""
            SELECT "id"
            FROM "Sensor_Overview"
            WHERE sensor_name = (%s)
            """, (sensor_name,))
            sensor_id_list = cur.fetchall()
            cur.close()

            if len(sensor_id_list) == 0:
                # sensor is not in database
                db_data = (sensor_name, "automatically added by python script")
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute(
                    'INSERT into "Sensor_Overview" (sensor_name, comment) VALUES (%s,%s)',
                    db_data)
                conn.commit()
                cur.close()
                conn.close()

                # get sensor id from database
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute("""
                SELECT "id"
                FROM "Sensor_Overview"
                WHERE sensor_name = (%s) AND comment = (%s)
                """, db_data)
                sensor_id = cur.fetchall()[0][0]
                cur.close()

            if len(sensor_id_list) == 1:
                # sensor is already in database
                sensor_id = sensor_id_list[0][0]


            print(assemblystep)
            try:
                for type in sftp.listdir(probestation_path + "/" + sensors + "/" + assemblystep):
                    if type == "I-V_Measurement":
                        try:
                            for measurement in sftp.listdir(probestation_path + "/" + sensors + "/" + assemblystep + "/" + type):
                                try:
                                    print(measurement)
                                    measurement_type = '_'.join(measurement.split("_")[0:2])

                                    for file in sftp.listdir(probestation_path + "/" + sensors + "/" + assemblystep + "/" + type + "/" + measurement):
                                        print(str(probestation_path + "/" + sensors + "/" + assemblystep + "/" + type + "/" + measurement))
                                        if file.split('_')[-1] == "AcquiredData.txt":
                                            complete_file_path = probestation_path + "/" + sensors + "/" + assemblystep + "/" + type + "/" + measurement + "/" + file
                                            print(file)
                                            measurement_time = '_'.join(file.split("_")[0:2])
                                            with sftp.open(complete_file_path, "r") as myfile:
                                                myfile.prefetch()
                                                df = pd.read_csv(myfile, decimal=',', delimiter="\t", names=['voltage', 'current', 'timestamp', 'humidity', 'temperature'], skiprows=9, encoding="latin1")
                                            df['voltage'] = df['voltage'].str.replace(",", ".")

                                            index = df.loc[pd.isna(df["timestamp"]), :].index.to_list()[0]
                                            # index = df['timestamp'].index[df['timestamp'].apply(np.isnan)]
                                            df = df.dropna()

                                            # df['ramp_direction'] = df['ramp_direction']
                                            df.loc[df.index <= index, 'ramp_direction'] = 'u'
                                            df.loc[df.index > index, 'ramp_direction'] = 'd'
                                            # df['ramp_direction'] = df.loc[df.index <= index, 'ramp_direction']

                                            with sftp.open(complete_file_path, "r") as myfile:
                                                myfile.prefetch()
                                                file_text = myfile.read()
                                            file_data = file_text.decode('latin1')
                                            file_data = file_data.replace("\t", "").replace("\r", "").split("\n")
                                            operator = file_data[0].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            module_id = file_data[1].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            sensor_name = file_data[2].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            assembly_step = file_data[3].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            comment = file_data[4].split(':')[1].replace('\t', '').replace('\n', '')
                                            number_of_repetitions = file_data[5].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            datapoints_number = len(df)
                                            date_time = datetime.datetime.strptime(file_data[6].replace('\t', '').replace('\n', '').replace(',', ''), "%d.%m.%Y %H:%M")

                                            # search operator in db for his id:
                                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                            cur = conn.cursor()
                                            cur.execute("""
                                            SELECT id
                                            FROM "General_Operators"
                                            WHERE "first_name" LIKE '%""" + str(operator) + "%'" + """ OR "family_name" LIKE '%""" + str(operator) + "%'"
                                            )
                                            operator_id_list = cur.fetchall()
                                            cur.close()
                                            if len(operator_id_list) != 0:
                                                operator_id = operator_id_list[0][0]
                                            else:
                                                operator_id = 2


                                            try:
                                                start_time = date_time - datetime.timedelta(minutes=10)
                                                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                                stop_time = date_time + datetime.timedelta(minutes=10)
                                                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                                                query_api = influx_client.query_api()
                                                query = 'from(bucket: "environement") \
                                                        |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                                        |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                                        |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                                        |> yield(name: "mean")'
                                                result = query_api.query(org=influx_org, query=query)
                                                influx_client.close()
                                                temperature = []
                                                humidity = []
                                                for table in result:
                                                    for record in table.records:
                                                        if record.get_field() == "humidity":
                                                            humidity.append(record.get_value())
                                                        if record.get_field() == "temperature":
                                                            temperature.append(record.get_value())

                                                temperature = round(np.mean(temperature), 2)
                                                humidity = round(np.mean(humidity), 2)
                                            except Exception as e:
                                                temperature = "NaN"
                                                humidity = "NaN"
                                                print("error while getting influx data", e)

                                            # see if data is already inside of database:
                                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                            cur = conn.cursor()
                                            cur.execute("""
                                            SELECT "id"
                                            FROM "IV_Measurement"
                                            WHERE timestamp = (%s) AND iv_file_sensor = (%s) AND datapoints_number = (%s) AND assembly_step = (%s)
                                            """, (date_time, sensor_name, datapoints_number, assemblystep, ))
                                            check_if_already_inside_db_iv_measurement = cur.fetchall()
                                            cur.close()
                                            print(check_if_already_inside_db_iv_measurement)

                                            if len(check_if_already_inside_db_iv_measurement) == 0:
                                                # data is not in database
                                                print('data is not already inside', str(check_if_already_inside_db_iv_measurement))

                                                # insert data in database
                                                db_data = (assemblystep, module_id, sensor_id, sensor_name, operator, operator_id, int(number_of_repetitions), datapoints_number, date_time, temperature, humidity, comment)
                                                print(db_data)
                                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                                cur = conn.cursor()
                                                cur.execute(
                                                    'INSERT into "IV_Measurement" (assembly_step, "iv_file_module", sensor, "iv_file_sensor", "iv_file_operator", "operator", "repetition_number", "datapoints_number", "timestamp", "air_temperature", "air_humidity", "comment") VALUES (%s,%s,%s,%s, %s,%s, %s, %s, %s, %s, %s, %s)',
                                                    db_data)
                                                conn.commit()
                                                cur.close()
                                                conn.close()

                                                # get iv_measurement id from database
                                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                                # check which metrology measurements are already inside DCA-DB
                                                cur = conn.cursor()
                                                cur.execute("""
                                                SELECT "id"
                                                FROM "IV_Measurement"
                                                WHERE timestamp = (%s)
                                                """, (date_time,))
                                                iv_measurement_id = cur.fetchall()[0][0]
                                                cur.close()

                                                # add all detailed measurements into Result Table
                                                df['IV_measurement_id'] = iv_measurement_id
                                                df.to_sql('IV_Measurement_Results', engine, if_exists='append', index=False)
                                except Exception as e:
                                    print(e)
                                    pass
                        except Exception as e:
                            print(e)
                            pass
            except Exception as e:
                print(e)
                pass
    except Exception as e:
        print(e)
        pass




# PROBE STATION - IV MEASUREMENT MODULES
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect("134.61.4.184", username="User", password="Passwort")
sftp = ssh.open_sftp()
probestation_path = "C:/Users/User/Documents/Probe Station/Measurements/Modules"

for modules in sftp.listdir(probestation_path):
    print(modules)
    try:
        for assemblystep in sftp.listdir(probestation_path + "/" + modules):

            # see if sensor is already inside of Database
            module_name = modules
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            # check which metrology measurements are already inside DCA-DB
            cur = conn.cursor()
            cur.execute("""
            SELECT "id"
            FROM "Module_Overview"
            WHERE cern_module_id = (%s)
            """, (module_name,))
            module_id_list = cur.fetchall()
            cur.close()

            if len(module_id_list) == 0:
                # module is not in database
                db_data = (module_name, "automatically added by probestation iv python script")
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute(
                    'INSERT into "Module_Overview" (cern_module_id, comment) VALUES (%s,%s)',
                    db_data)
                conn.commit()
                cur.close()
                conn.close()

                # get module id from database
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute("""
                SELECT "id"
                FROM "Module_Overview"
                WHERE cern_module_id = (%s) AND comment = (%s)
                """, db_data)
                module_db_id = cur.fetchall()[0][0]
                cur.close()

            if len(module_id_list) == 1:
                # sensor is already in database
                module_db_id = module_id_list[0][0]


            print(assemblystep)
            try:
                for type in sftp.listdir(probestation_path + "/" + modules + "/" + assemblystep):
                    if type == "I-V_Measurement":
                        try:
                            for measurement in sftp.listdir(probestation_path + "/" + modules + "/" + assemblystep + "/" + type):
                                try:
                                    print(measurement)
                                    measurement_type = '_'.join(measurement.split("_")[0:2])

                                    for file in sftp.listdir(probestation_path + "/" + modules + "/" + assemblystep + "/" + type + "/" + measurement):
                                        print(str(probestation_path + "/" + modules + "/" + assemblystep + "/" + type + "/" + measurement))
                                        if file.split('_')[-1] == "AcquiredData.txt":
                                            complete_file_path = probestation_path + "/" + modules + "/" + assemblystep + "/" + type + "/" + measurement + "/" + file
                                            print(file)
                                            measurement_time = '_'.join(file.split("_")[0:2])
                                            with sftp.open(complete_file_path, "r") as myfile:
                                                myfile.prefetch()
                                                df = pd.read_csv(myfile, decimal=',', delimiter="\t", names=['voltage', 'current', 'timestamp', 'humidity', 'temperature'], skiprows=9, encoding="latin1")
                                            df['voltage'] = df['voltage'].str.replace(",", ".")

                                            index = df.loc[pd.isna(df["timestamp"]), :].index.to_list()[0]
                                            # index = df['timestamp'].index[df['timestamp'].apply(np.isnan)]
                                            df = df.dropna()

                                            # df['ramp_direction'] = df['ramp_direction']
                                            df.loc[df.index <= index, 'ramp_direction'] = 'u'
                                            df.loc[df.index > index, 'ramp_direction'] = 'd'
                                            # df['ramp_direction'] = df.loc[df.index <= index, 'ramp_direction']

                                            with sftp.open(complete_file_path, "r") as myfile:
                                                myfile.prefetch()
                                                file_text = myfile.read()
                                            file_data = file_text.decode('latin1')
                                            file_data = file_data.replace("\t", "").replace("\r", "").split("\n")
                                            operator = file_data[0].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            module_id = file_data[1].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            sensor_name = file_data[2].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            assembly_step = file_data[3].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            comment = file_data[4].split(':')[1].replace('\t', '').replace('\n', '')
                                            number_of_repetitions = file_data[5].split(':')[1].replace('\t', '').replace('\n', '').replace(' ', '')
                                            datapoints_number = len(df)
                                            date_time = datetime.datetime.strptime(file_data[6].replace('\t', '').replace('\n', '').replace(',', ''), "%d.%m.%Y %H:%M")

                                            # search operator in db for his id:
                                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                            cur = conn.cursor()
                                            cur.execute("""
                                            SELECT id
                                            FROM "General_Operators"
                                            WHERE "first_name" LIKE '%""" + str(operator) + "%'" + """ OR "family_name" LIKE '%""" + str(operator) + "%'"
                                            )
                                            operator_id_list = cur.fetchall()
                                            cur.close()
                                            if len(operator_id_list) != 0:
                                                operator_id = operator_id_list[0][0]
                                            else:
                                                operator_id = 2


                                            try:
                                                start_time = date_time - datetime.timedelta(minutes=10)
                                                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                                stop_time = date_time + datetime.timedelta(minutes=10)
                                                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                                                query_api = influx_client.query_api()
                                                query = 'from(bucket: "environement") \
                                                        |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                                        |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                                        |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                                        |> yield(name: "mean")'
                                                result = query_api.query(org=influx_org, query=query)
                                                influx_client.close()
                                                temperature = []
                                                humidity = []
                                                for table in result:
                                                    for record in table.records:
                                                        if record.get_field() == "humidity":
                                                            humidity.append(record.get_value())
                                                        if record.get_field() == "temperature":
                                                            temperature.append(record.get_value())

                                                temperature = round(np.mean(temperature), 2)
                                                humidity = round(np.mean(humidity), 2)
                                            except Exception as e:
                                                temperature = "NaN"
                                                humidity = "NaN"
                                                print("error while getting influx data", e)

                                            # see if data is already inside of database:
                                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                            cur = conn.cursor()
                                            cur.execute("""
                                            SELECT "id"
                                            FROM "IV_Measurement"
                                            WHERE timestamp = (%s) AND iv_file_module = (%s) AND iv_file_sensor = (%s) AND datapoints_number = (%s) AND assembly_step = (%s)
                                            """, (date_time, module_name, sensor_name, datapoints_number, assemblystep, ))
                                            check_if_already_inside_db_iv_measurement = cur.fetchall()
                                            cur.close()
                                            print(check_if_already_inside_db_iv_measurement)

                                            if len(check_if_already_inside_db_iv_measurement) == 0:
                                                # data is not in database
                                                print('data is not already inside', str(check_if_already_inside_db_iv_measurement))

                                                # insert data in database
                                                db_data = (assemblystep, module_id, module_db_id, sensor_name, operator, operator_id, int(number_of_repetitions), datapoints_number, date_time, temperature, humidity, comment)
                                                print(db_data)
                                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                                cur = conn.cursor()
                                                cur.execute(
                                                    'INSERT into "IV_Measurement" (assembly_step, "iv_file_module", module, "iv_file_sensor", "iv_file_operator", "operator", "repetition_number", "datapoints_number", "timestamp", "air_temperature", "air_humidity", "comment") VALUES (%s,%s,%s,%s, %s,%s, %s, %s, %s, %s, %s, %s)',
                                                    db_data)
                                                conn.commit()
                                                cur.close()
                                                conn.close()

                                                # get iv_measurement id from database
                                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                                # check which metrology measurements are already inside DCA-DB
                                                cur = conn.cursor()
                                                cur.execute("""
                                                SELECT "id"
                                                FROM "IV_Measurement"
                                                WHERE timestamp = (%s)
                                                """, (date_time,))
                                                iv_measurement_id = cur.fetchall()[0][0]
                                                cur.close()

                                                # add all detailed measurements into Result Table
                                                df['IV_measurement_id'] = iv_measurement_id
                                                df.to_sql('IV_Measurement_Results', engine, if_exists='append', index=False)
                                except Exception as e:
                                    print(e)
                                    pass
                        except Exception as e:
                            print(e)
                            pass
            except Exception as e:
                print(e)
                pass
    except Exception as e:
        print(e)
        pass
        