import webbrowser
import requests
import time

response = requests.post("http://lx3bcms02.physik.rwth-aachen.de/metrology/get_data")
print(response.status_code)

if response.status_code == 200:
	print("wait 5 sec")
	time.sleep(5)
	webbrowser.open("http://lx3bcms02.physik.rwth-aachen.de/metrology/overview")