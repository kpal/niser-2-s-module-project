import os
import paramiko
import influxdb_client
import psycopg2
import datetime
import mmap
import shutil
from sqlalchemy import create_engine
import sqlalchemy
import csv
import numpy as np
import time
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS



dsm_root_path = "C:/Users/Admin/Desktop/Test_Copy_Max_ssh"
local_root_path = "/WebApplication/application/static/data/metrology"

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect("123.123.4123.123", username="Admin", password="Passwort")
sftp = ssh.open_sftp()
module_folders = sftp.listdir(dsm_root_path)

influx_host           = "http://444.555.666.777:8086"
influx_token          = "TOKEN"
influx_org            = "rwth"

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])

conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
# check which metrology measurements are already inside DCA-DB
cur = conn.cursor()
cur.execute("""
SELECT "metrology_module", "timestamp"
FROM "Metrology_Sensor_Sensor_Alignment"
""")
dca_sensor_sensor = cur.fetchall()
cur.close()

cur = conn.cursor()
cur.execute("""
SELECT "metrology_module", "timestamp"
FROM "Metrology_Sensor_Bridge_Alignment"
""")
dca_sensor_bridge = cur.fetchall()
cur.close()

cur = conn.cursor()
cur.execute("""
SELECT "metrology_module", "timestamp"
FROM "Metrology_Height_Profile"
""")
dca_height_profile = cur.fetchall()
cur.close()


# get automatic grading limit values
cur = conn.cursor()
cur.execute("""
SELECT "id", "rotation_urad", "x_shift_um", "y_shift_um"
FROM "Metrology_Limit"
ORDER BY "timestamp" ASC
LIMIT 1
""")
metrology_limit = cur.fetchall()
cur.close()
limit_id            = int(metrology_limit[0][0])
limit_rotation_urad = float(metrology_limit[0][1])
limit_x_shift_um    = float(metrology_limit[0][2])
limit_y_shift_um    = float(metrology_limit[0][3])



conn.close()

for modules in module_folders:
    for folder in sftp.listdir(dsm_root_path+"/"+modules):
        if folder == "alignment":
            for measurements in sftp.listdir(dsm_root_path + "/" + modules + "/alignment"):
                try:
                    local_measurement_timestamp = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S")
                    local_measurement_identification = (modules, local_measurement_timestamp)
                except:
                    continue
                if local_measurement_identification not in dca_sensor_sensor:
                    try:
                        if "keypoints.asdf" in sftp.listdir(dsm_root_path + "/" + modules + "/alignment/" + measurements):
                            files_path  = modules + "/alignment/" + measurements + "/"
                            module_id   = modules
                            type        = "alignment"
                            timestamp   = str(datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S"))
                            print("alignment", modules, measurements)
                            os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                            sftp.get(dsm_root_path + "/" + files_path + "keypoints.asdf", local_root_path + "/" + files_path + "keypoints.asdf")
                            sftp.get(dsm_root_path + "/" + files_path + "alignment.asdf", local_root_path + "/" + files_path + "alignment.asdf")
                            time.sleep(1)
                            with open(local_root_path + "/" + files_path + 'alignment.asdf', 'r', encoding='iso-8859-1') as file:
                                for line in file:
                                    if line.startswith('mounted'):
                                        mounting_mode = str(line.split(' ')[1]).replace('\n', '')
                                        print('mounting mode: ', mounting_mode)
                                    if line.startswith('fn'):
                                        config_file = str(line.split('\\')[-1]).replace('\n', '')
                                        print('config_file: ', config_file)
                                    if line.startswith('name'):
                                        module_name_check = str(line.split(' ')[1]).replace('\n', '')
                                        print('module_name_check: ', module_name_check)

                            for plot in sftp.listdir(dsm_root_path + "/" + modules + "/alignment/" + measurements + "/Plots"):
                                files_path = modules + "/alignment/" + measurements + "/Plots/"
                                if plot[-3:] == "png":
                                    print(dsm_root_path + "/" + files_path + plot)
                                    os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                                    sftp.get(dsm_root_path + "/" + files_path + plot, local_root_path + "/" + files_path + plot)
                                elif plot[-3:] == "txt":
                                    print(dsm_root_path + "/" + files_path + plot)
                                    os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                                    sftp.get(dsm_root_path + "/" + files_path + plot, local_root_path + "/" + files_path + plot)
                                    csv_data = []
                                    with open(local_root_path + "/" + files_path + 'results.txt', mode='r') as file:
                                        csvFile = csv.reader(file, delimiter='\t')
                                        for lines in csvFile:
                                            csv_data.append(lines)
                                    sensor_sensor_rotation = float(csv_data[-1][1])
                                    sensor_sensor_shift_x = float(csv_data[-1][5])
                                    sensor_sensor_shift_y = float(csv_data[-1][-1])

                                else:
                                    try:
                                        for subplot in sftp.listdir(dsm_root_path + "/" + modules + "/alignment/" + measurements + "/Plots/" + plot):
                                            try:
                                                files_path =  modules + "/alignment/" + measurements + "/Plots/" + plot + "/"
                                                #print(dsm_root_path + "/" + files_path + subplot)
                                                os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                                                sftp.get(dsm_root_path + "/" + files_path + subplot, local_root_path + "/" + files_path + subplot)
                                            except FileNotFoundError:
                                                pass
                                            except Exception as error_unkown:
                                                print(error_unkown)
                                    except FileNotFoundError:
                                        pass
                                    except Exception as error_unkown:
                                        print(error_unkown)


                            # apply metrology filter limits:
                            if sensor_sensor_rotation <= limit_rotation_urad and sensor_sensor_shift_x <= limit_x_shift_um and sensor_sensor_shift_y <= limit_y_shift_um:
                                automatic_grade = 1 # grade = good
                            elif sensor_sensor_shift_y > limit_rotation_urad and sensor_sensor_shift_x > limit_x_shift_um and sensor_sensor_shift_y > limit_y_shift_um:
                                automatic_grade = 2 # grade = bad
                            else:
                                automatic_grade = 3 # grade = unclear

                            # search for module reference:
                            try:
                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                cur = conn.cursor()
                                cur.execute("""
                                SELECT "id"
                                FROM "Module_Overview"
                                WHERE "aachen_module_id" LIKE (%s) 
                                LIMIT 1
                                """, (str(module_id), ))
                                module_reference_data = cur.fetchall()
                                cur.close()
                                module_reference_id = module_reference_data[0][0]
                            except Exception as e:
                                module_reference_id = None
                                print("error while getting module reference: ", e)

                            # get information about temperature and humidity
                            try:
                                start_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") - datetime.timedelta(minutes=10)
                                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                stop_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") + datetime.timedelta(minutes=10)
                                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                                query_api = influx_client.query_api()
                                query = 'from(bucket: "environement") \
                                        |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                        |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                        |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                        |> yield(name: "mean")'
                                result = query_api.query(org=influx_org, query=query)
                                influx_client.close()
                                temperature = []
                                humidity = []
                                for table in result:
                                    for record in table.records:
                                        if record.get_field() == "humidity":
                                            humidity.append(record.get_value())
                                        if record.get_field() == "temperature":
                                            temperature.append(record.get_value())

                                temperature = np.mean(temperature)
                                humidity = np.mean(humidity)
                            except Exception as e:
                                temperature = "NaN"
                                humidity = "NaN"
                                print("error while getting influx data", e)


                            # data formatting and submission
                            db_data = (str(module_id), str(timestamp), sensor_sensor_rotation, sensor_sensor_shift_x, sensor_sensor_shift_y, str(files_path), config_file, mounting_mode, module_name_check, automatic_grade, limit_id, module_reference_id, temperature, humidity)

                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                            cur = conn.cursor()
                            cur.execute(
                                'INSERT into "Metrology_Sensor_Sensor_Alignment" ("metrology_module", "timestamp", "rotation_urad", "x_shift_um", "y_shift_um", "image_folder", "metrology_config_file", "metrology_config_mounted", "metrology_config_name", "automatic_grade", "automatic_grade_limit_id", "module", "air_temperature", "air_humidity") VALUES (%s, %s,%s, %s, %s, %s, %s, %s, %s,%s, %s, %s,%s,%s)',
                                db_data)
                            conn.commit()
                            cur.close()
                            conn.close()

                    except Exception as e:
                        print(e)




        if folder == "sensor_bridge":
            for measurements in sftp.listdir(dsm_root_path + "/" + modules + "/sensor_bridge"):
                try:
                    local_measurement_timestamp = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S")
                    local_measurement_identification = (modules, local_measurement_timestamp)
                except:
                    continue
                if local_measurement_identification not in dca_sensor_bridge:
                    try:
                        if "sensor_bridge_results.txt" in sftp.listdir(dsm_root_path + "/" + modules + "/sensor_bridge/" + measurements):
                            module_name     = modules
                            type            = "sensor_bridge"
                            timestamp       = str(datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S"))
                            files_path      = modules + "/sensor_bridge/" + measurements
                            print("sensor_bridge", modules, measurements)

                            os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                            sftp.get(dsm_root_path + "/" + files_path + "/sensor_bridge_results.txt", local_root_path + "/" + files_path + "/sensor_bridge_results.txt")

                            csv_data = []
                            with open(local_root_path + "/" + files_path + "/sensor_bridge_results.txt", 'r', encoding='iso-8859-1') as file:
                                csvFile = csv.reader(file, delimiter='\t')
                                for lines in csvFile:
                                    csv_data.append(lines)
                                    lines = [item for item in lines if item != '']
                                    if lines[0] == 'Left Bridge - Top Sensor':
                                        left_bridge_top_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        left_bridge_top_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                        left_bridge_top_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Left Bridge - Bot Sensor':
                                        left_bridge_bot_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        left_bridge_bot_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                        left_bridge_bot_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Right Bridge - Top Sensor':
                                        right_bridge_top_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        right_bridge_top_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                        right_bridge_top_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Right Bridge - Bot Sensor':
                                        right_bridge_bot_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        right_bridge_bot_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                        right_bridge_bot_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Upper Stump - Top Sensor':
                                        upper_bridge_top_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        upper_bridge_top_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                    # upper_bridge_top_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Upper Stump - Bot Sensor':
                                        upper_bridge_bot_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        upper_bridge_bot_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                    # upper_bridge_bot_sensor_rot = float(lines[3].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Lower Stump - Top Sensor':
                                        lower_stump_top_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        lower_stump_top_sensor_y = float(lines[2].replace('[', '').replace(']', ''))
                                    if lines[0] == 'Lower Stump - Bot Sensor':
                                        lower_stump_bot_sensor_x = float(lines[1].replace('[', '').replace(']', ''))
                                        lower_stump_bot_sensor_y = float(lines[2].replace('[', '').replace(']', ''))

                            # search for module reference:
                            try:
                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                cur = conn.cursor()
                                cur.execute("""
                                SELECT "id"
                                FROM "Module_Overview"
                                WHERE "aachen_module_id" LIKE (%s) 
                                LIMIT 1
                                """, (str(module_id), ))
                                module_reference_data = cur.fetchall()
                                cur.close()
                                module_reference_id = module_reference_data[0][0]
                            except Exception as e:
                                module_reference_id = None
                                print("error while getting module reference: ", e)

                            # get temperature and humidity data
                            try:
                                start_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") - datetime.timedelta(minutes=10)
                                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                stop_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") + datetime.timedelta(minutes=10)
                                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                                query_api = influx_client.query_api()
                                query = 'from(bucket: "environement") \
                                        |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                        |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                        |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                        |> yield(name: "mean")'
                                result = query_api.query(org=influx_org, query=query)
                                influx_client.close()
                                temperature = []
                                humidity = []
                                for table in result:
                                    for record in table.records:
                                        if record.get_field() == "humidity":
                                            humidity.append(record.get_value())
                                        if record.get_field() == "temperature":
                                            temperature.append(record.get_value())

                                temperature = np.mean(temperature)
                                humidity = np.mean(humidity)
                            except Exception as e:
                                temperature = "NaN"
                                humidity = "NaN"
                                print("error while getting influx data", e)




                            db_data = (str(module_name), str(timestamp),
                                       left_bridge_top_sensor_x,
                                       left_bridge_top_sensor_y,
                                       left_bridge_top_sensor_rot,

                                       left_bridge_bot_sensor_x,
                                       left_bridge_bot_sensor_y,
                                       left_bridge_bot_sensor_rot,

                                       right_bridge_bot_sensor_x,
                                       right_bridge_bot_sensor_y,
                                       right_bridge_bot_sensor_rot,

                                       right_bridge_bot_sensor_x,
                                       right_bridge_bot_sensor_y,
                                       right_bridge_bot_sensor_rot,

                                       upper_bridge_top_sensor_x,
                                       upper_bridge_top_sensor_y,

                                       upper_bridge_bot_sensor_x,
                                       upper_bridge_bot_sensor_y,

                                       lower_stump_top_sensor_x,
                                       lower_stump_top_sensor_y,

                                       lower_stump_bot_sensor_x,
                                       lower_stump_bot_sensor_y,
                                       module_reference_id,
                                       temperature,
                                       humidity
                                       )


                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                            cur = conn.cursor()
                            print('db_data_bridge', db_data)
                            cur.execute('INSERT into "Metrology_Sensor_Bridge_Alignment" ( \
                                "metrology_module", "timestamp", "Left_Bridge_Top_Sensor_x", \
                                "Left_Bridge_Top_Sensor_y", \
                                "Left_Bridge_Top_Sensor_rad", \
                                "Left_Bridge_Bot_Sensor_x", \
                                "Left_Bridge_Bot_Sensor_y", \
                                "Left_Bridge_Bot_Sensor_rad", \
                                "Right_Bridge_Top_Sensor_x", \
                                "Right_Bridge_Top_Sensor_y",\
                                "Right_Bridge_Top_Sensor_rad", \
                                "Right_Bridge_Bot_Sensor_x", \
                                "Right_Bridge_Bot_Sensor_y", \
                                "Right_Bridge_Bot_Sensor_rad", \
                                "Upper_Stump_Top_Sensor_x", \
                                "Upper_Stump_Top_Sensor_y", \
                                "Upper_Stump_Bot_Sensor_x", \
                                "Upper_Stump_Bot_Sensor_y",\
                                "Lower_Stump_Top_Sensor_x", \
                                "Lower_Stump_Top_Sensor_y", \
                                "Lower_Stump_Bot_Sensor_x", \
                                "Lower_Stump_Bot_Sensor_y", \
                                "module", \
                                "air_temperature", \
                                "air_humidity" \
                                ) VALUES (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s,%s,%s,%s, %s, %s, %s,%s, %s, %s,%s,%s,%s,%s,%s,%s)', db_data)
                            conn.commit()
                            cur.close()
                            conn.close()

                    except Exception as e:
                        print(e)

        if folder == "height_profile":
            for measurements in sftp.listdir(dsm_root_path + "/" + modules + "/height_profile"):
                try:
                    local_measurement_timestamp = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S")
                    local_measurement_identification = (modules, local_measurement_timestamp)
                except:
                    continue
                if local_measurement_identification not in dca_height_profile:
                    try:
                        if "height_profile.asdf" in sftp.listdir(dsm_root_path + "/" + modules + "/height_profile/" + measurements):
                            module_id   = modules
                            type        = "height_profile"
                            timestamp   = str(datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S"))
                            files_path = modules + "/height_profile/" + measurements
                            print("sensor_bridge", modules, measurements)

                            os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                            sftp.get(dsm_root_path + "/" + files_path + "/height_profile.asdf", local_root_path + "/" + files_path + "/height_profile.asdf")

                            for plot in sftp.listdir(dsm_root_path + "/" + modules + "/height_profile/" + measurements + "/plot"):
                                files_path = modules + "/height_profile/" + measurements + "/plot/"
                                if plot[-3:] == "png":
                                    print(dsm_root_path + "/" + files_path + plot)
                                    os.makedirs(local_root_path + "/" + files_path, exist_ok=True)
                                    sftp.get(dsm_root_path + "/" + files_path + plot, local_root_path + "/" + files_path + plot)

                            # search for module reference:
                            try:
                                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                                cur = conn.cursor()
                                cur.execute("""
                                SELECT "id"
                                FROM "Module_Overview"
                                WHERE "aachen_module_id" LIKE (%s) 
                                LIMIT 1
                                """, (str(module_id), ))
                                module_reference_data = cur.fetchall()
                                cur.close()
                                module_reference_id = module_reference_data[0][0]
                            except Exception as e:
                                module_reference_id = None
                                print("error while getting module reference: ", e)

                            # get temperature and humidity data
                            try:
                                start_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") - datetime.timedelta(minutes=10)
                                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                stop_time = datetime.datetime.strptime(measurements, "%Y-%m-%d %H-%M-%S") + datetime.timedelta(minutes=10)
                                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                                query_api = influx_client.query_api()
                                query = 'from(bucket: "environement") \
                                        |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                        |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                        |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                        |> yield(name: "mean")'
                                result = query_api.query(org=influx_org, query=query)
                                influx_client.close()
                                temperature = []
                                humidity = []
                                for table in result:
                                    for record in table.records:
                                        if record.get_field() == "humidity":
                                            humidity.append(record.get_value())
                                        if record.get_field() == "temperature":
                                            temperature.append(record.get_value())

                                temperature = np.mean(temperature)
                                humidity = np.mean(humidity)
                            except Exception as e:
                                temperature = "NaN"
                                humidity = "NaN"
                                print("error while getting influx data", e)


                            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                            cur = conn.cursor()
                            db_data = (module_id, timestamp, files_path, module_reference_id, temperature, humidity)
                            cur.execute('INSERT into "Metrology_Height_Profile" ("metrology_module", "timestamp", "image_folder", "module", "air_temperature", "air_humidity") VALUES (%s, %s, %s, %s, %s, %s)', db_data)
                            conn.commit()
                            cur.close()
                            conn.close()
                    except Exception as e:
                        print(e)



sftp.close()
ssh.close()


print("get new metrology data")