import psycopg2
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import numpy as np

db_username_pulltester = "db_bondtester"
db_password_pulltester = "passwort"
db_host_pulltester     = "host"
db_port_pulltester     = 5430
db_database_pulltester = "db_bondtester"

influx_host           = "http://444.555.666.777:8086"
influx_token          = "TOKEN"
influx_org            = "rwth"

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])



######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
###################################
###################################
###################################
# copy and check product table
###################################
###################################
conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
cur = conn.cursor()
cur.execute("""
SELECT id, name, headtype, creation
FROM tblproduct
""")
pulltest_query_product = cur.fetchall()
cur.close()
conn.close()

# check if id is already inside DCA-DB
conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
cur = conn.cursor()
cur.execute("""
SELECT pulltest_db_id, name, headtype, pulltest_db_timestamp
FROM "Pulltest_product"
""")
dca_query_product = cur.fetchall()
cur.close()
conn.close()


for i in range(len(pulltest_query_product)):
    if pulltest_query_product[i] not in dca_query_product:
        conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
        # this program is not yet registered in the DCA-DB
        print("insert of this into DCA-DB", pulltest_query_product[i])
        cur = conn.cursor()
        cur.execute("""
        INSERT INTO "Pulltest_product" (pulltest_db_id, name, headtype, pulltest_db_timestamp) VALUES (%s, %s, %s, %s)
        """, pulltest_query_product[i])
        conn.commit()
        cur.close()
        conn.close()
    else:
        print('data for this product id is already inside of the db table', pulltest_query_product[i])
        # please select true or false so that the results data is correctly copied

# Grade codes
conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
cur = conn.cursor()
cur.execute("""
SELECT code, description, resultclass, product
FROM tblgradecodes
""")
pulltest_query_grades = cur.fetchall()
cur.close()
conn.close()

# check if id is already inside DCA-DB
conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
cur = conn.cursor()
cur.execute("""
SELECT code, description, resultclass, product
FROM "Pulltest_Gradecode"
""")
dca_query_grades = cur.fetchall()
cur.close()
conn.close()


for i in range(len(pulltest_query_grades)):
    if pulltest_query_grades[i] not in dca_query_grades:
        # copy also gradecode
        conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
        # this program is not yet registered in the DCA-DB
        print("insert of this into DCA-DB grade code", pulltest_query_grades[i])
        cur = conn.cursor()
        cur.execute("""
        INSERT INTO "Pulltest_Gradecode" (code, description, resultclass, product) VALUES (%s, %s, %s, %s)
        """, pulltest_query_grades[i])
        conn.commit()
        cur.close()
        conn.close()
    else:
        print('data for this product id is already inside of the db table', pulltest_query_grades[i])
        # please select true or false so that the results data is correctly copied

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
# get product id used for pulltest module
conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
cur = conn.cursor()
cur.execute("""
SELECT pulltest_db_id
FROM "Pulltest_product"
WHERE "2s_modules_used" = true AND "pulltest_module" = true
""")
dca_product_module_true = cur.fetchall()
cur.close()
conn.close()
###################################
###################################
###################################
# Pulltest Module
###################################
###################################
###################################
for pulltest_product_id in dca_product_module_true:
    pulltest_product_id = pulltest_product_id[0]
    # get pulltest metadata for the relevant pulltest product id
    conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
    cur = conn.cursor()
    cur.execute("""
    SELECT DISTINCT lot, ts, product
    FROM tbltestresult
    WHERE product = (%s)
    """, ((pulltest_product_id), ))
    pulltest_query_results_timestamps = cur.fetchall()
    cur.close()
    conn.close()

    conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
    cur = conn.cursor()
    cur.execute("""
    SELECT DISTINCT pulltest_lot, timestamp, pulltest_product
    FROM "Pulltest_Measurement"
    WHERE pulltest_product = (%s)
    """, ((pulltest_product_id), ))
    DCA_query_results_timestamp = cur.fetchall()
    cur.close()
    conn.close()

    for measurement in pulltest_query_results_timestamps:
        if measurement not in DCA_query_results_timestamp:
            # get data from cleanroom monitoring system with temperature and humidity values
            try:
                start_time = measurement[1] - datetime.timedelta(minutes=10)
                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                stop_time = measurement[1] + datetime.timedelta(minutes=10)
                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                query_api = influx_client.query_api()
                query = 'from(bucket: "environement") \
                                |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                |> filter(fn: (r) => r["sensor"] == "rpi6" or r["sensor"] == "rpi7") \
                                |> yield(name: "mean")'
                result = query_api.query(org=influx_org, query=query)
                influx_client.close()
                temperature = []
                humidity = []
                for table in result:
                    for record in table.records:
                        if record.get_field() == "humidity":
                            humidity.append(record.get_value())
                        if record.get_field() == "temperature":
                            temperature.append(record.get_value())

                temperature = np.mean(temperature)
                humidity = np.mean(humidity)
            except Exception as e:
                temperature = "NaN"
                humidity = "NaN"
                print("error while getting influx data", e)

            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "create_ts", "correction_factor_module", id
                    FROM "Pulltest_correctionfactor"
                    WHERE create_ts <= %s
                    ORDER BY create_ts DESC
                    LIMIT 1
                    """, [measurement[1]])
            correctionfactor = cur.fetchall()
            cur.close()
            conn.close()
            correction_module = float(correctionfactor[0][1])
            correction_id = int(correctionfactor[0][2])

            data_lot            = measurement[0]
            data_product        = measurement[2]
            data_temperature    = temperature
            data_humidity       = humidity
            data_timestamp      = measurement[1]
            data_correctionid   = correction_id
            is_module           = True
            is_HV               = False


            # get comment data from pulltester
            conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
            cur = conn.cursor()
            cur.execute("""
            SELECT num, stringval
            FROM tblfieldvalues
            WHERE lot = %s AND product = %s
            """, (data_lot, data_product, ))
            pulltest_comment_data = cur.fetchall()
            print(pulltest_comment_data)
            cur.close()
            conn.close()



            pulltest_comment_title = pulltest_comment_data[0]
            pulltest_comment_lot = pulltest_comment_data[1]
            pulltest_comment_comment = pulltest_comment_data[2]
            pulltest_data = (data_lot, data_product, data_temperature, data_humidity, data_timestamp, data_correctionid, is_module, is_HV, pulltest_comment_title[1], pulltest_comment_lot[1], pulltest_comment_comment[1])

            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            print("insert of this into DCA-DB", measurement)
            cur = conn.cursor()
            cur.execute("""
            INSERT INTO "Pulltest_Measurement" (pulltest_lot, pulltest_product, air_temperature, air_humidity, timestamp, correction_factor_id, "is_module", "is_HV", "pulltest_comment_title", "pulltest_comment_lot", "pulltest_comment_comment") VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """, pulltest_data)
            conn.commit()
            cur.close()
            conn.close()


            # get pulltest measurement id:
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "id"
                    FROM "Pulltest_Measurement"
                    WHERE pulltest_lot = %s AND pulltest_product = %s AND timestamp = %s
                    """, (data_lot, data_product, data_timestamp, ))
            pulltest_measurement_id = cur.fetchall()[0][0]
            cur.close()
            conn.close()

            # get pulltest limit data
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "id", corrected_mean, corrected_std_percent, maximum_lifts
                    FROM "Pulltest_Limit"
                    WHERE create_ts <= %s
                    ORDER BY create_ts DESC
                    LIMIT 1
                    """, [data_timestamp])
            pulltest_limit_values = cur.fetchall()[0]
            cur.close()
            conn.close()

            limit_id = pulltest_limit_values[0]
            limit_corrected_mean = pulltest_limit_values[1]
            limit_corrected_std_percent = pulltest_limit_values[2]
            limit_maximum_lifts = pulltest_limit_values[3]

            # get data from pulltester
            conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
            cur = conn.cursor()
            cur.execute("""
            SELECT id, tearoffforce, destructed, isfurtflag, gradecode, resultclass, ts
            FROM tbltestresult
            WHERE lot = %s AND product = %s AND ts = %s
            """, (data_lot, data_product, data_timestamp, ))
            pulltest_data = cur.fetchall()
            print(pulltest_data)
            cur.close()
            conn.close()

            all_corrected_tearoffforce = []
            all_gradecode_1 =0
            all_gradecode_2 = 0
            all_gradecode_3 = 0
            all_gradecode_4 = 0
            all_gradecode_5 = 0
            total_measurements = 0

            # write all data to DCA-DB
            for detailed_measurement in pulltest_data:
                total_measurements += 1
                # available data
                # pulltest_measurement_id, data_lot, data_product, data_temperature, data_humidity, data_grade, data_timestamp, data_correctionid
                pulltest_db_id          = detailed_measurement[0]
                tearoffforce            = detailed_measurement[1]
                corrected_tearoffforce  = tearoffforce * correction_module
                all_corrected_tearoffforce.append(corrected_tearoffforce)

                destructed              = detailed_measurement[2]
                isfurtflag              = detailed_measurement[3]
                gradecode               = detailed_measurement[4]
                resultclass             = detailed_measurement[5]
                detailed_timestamp      = detailed_measurement[6]

                all_gradecode_1 = all_gradecode_1 + 1 if gradecode == 1 else all_gradecode_1
                all_gradecode_2 = all_gradecode_2 + 1 if gradecode == 2 else all_gradecode_2
                all_gradecode_3 = all_gradecode_3 + 1 if gradecode == 3 else all_gradecode_3
                all_gradecode_4 = all_gradecode_4 + 1 if gradecode == 4 else all_gradecode_4
                all_gradecode_5 = all_gradecode_5 + 1 if gradecode == 5 else all_gradecode_5

                pulltest_detail_data = (detailed_timestamp, pulltest_db_id, pulltest_measurement_id, tearoffforce, corrected_tearoffforce, isfurtflag, destructed, resultclass, gradecode, correction_id)

                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                print("insert of this into DCA-DB", measurement)
                cur = conn.cursor()
                cur.execute("""
                INSERT INTO "Pulltest_Results" (timestamp, pulltest_db_id, pulltest_measurement_id, tearoffforce, corrected_tearoffforce, isfurtflag, destructed, resultclass, grade, correction_factor_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                """, pulltest_detail_data)
                conn.commit()
                cur.close()
                conn.close()


            mean_corrected_tearoffforce = np.mean(all_corrected_tearoffforce)
            std_limit = mean_corrected_tearoffforce*limit_corrected_std_percent
            std_corrected_teaoffforce   = np.std(all_corrected_tearoffforce)

            automatic_grade_verdict = 3 # Unclear
            if mean_corrected_tearoffforce >= limit_corrected_mean or std_corrected_teaoffforce >= std_limit or all_gradecode_5 + all_gradecode_1 >= total_measurements*0.02:
                automatic_grade_verdict = 2 # 2 means Bad

            if mean_corrected_tearoffforce < limit_corrected_mean and std_corrected_teaoffforce < std_limit and all_gradecode_5 + all_gradecode_1 < total_measurements*0.02:
                automatic_grade_verdict = 1 # Good




            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            print("update GRADE", measurement)
            cur = conn.cursor()
            cur.execute("""
            UPDATE "Pulltest_Measurement" 
            SET automatic_grade = %s, number_of_tests = %s
            WHERE id = %s
            """, (automatic_grade_verdict, total_measurements, pulltest_measurement_id, ))
            conn.commit()
            cur.close()
            conn.close()



print('start HV')

# for Pulltest HV
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
# get product id used for pulltest module
conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
cur = conn.cursor()
cur.execute("""
SELECT id
FROM "Pulltest_product"
WHERE "2s_modules_used" = true AND "pulltest_HV" = true
""")
dca_product_module_true = cur.fetchall()
cur.close()
conn.close()
###################################
###################################
###################################
# Pulltest Module
###################################
###################################
###################################
for pulltest_product_id in dca_product_module_true:
    pulltest_product_id = pulltest_product_id[0]
    # get pulltest metadata for the relevant pulltest product id
    conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
    cur = conn.cursor()
    cur.execute("""
    SELECT DISTINCT lot, ts, product
    FROM tbltestresult
    WHERE product = (%s)
    """, ((pulltest_product_id), ))
    pulltest_query_results_timestamps = cur.fetchall()
    cur.close()
    conn.close()

    conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
    cur = conn.cursor()
    cur.execute("""
    SELECT DISTINCT pulltest_lot, timestamp, pulltest_product
    FROM "Pulltest_Measurement"
    WHERE pulltest_product = (%s)
    """, ((pulltest_product_id), ))
    DCA_query_results_timestamp = cur.fetchall()
    cur.close()
    conn.close()

    for measurement in pulltest_query_results_timestamps:
        if measurement not in DCA_query_results_timestamp:
            # get data from cleanroom monitoring system with temperature and humidity values
            try:
                start_time = measurement[1] - datetime.timedelta(minutes=10)
                start_time = start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                stop_time = measurement[1] + datetime.timedelta(minutes=10)
                stop_time = stop_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                influx_client = influxdb_client.InfluxDBClient(url=influx_host, token=influx_token, org=influx_org)
                query_api = influx_client.query_api()
                query = 'from(bucket: "environement") \
                                |> range(start: ' + start_time + ', stop: ' + stop_time + ') \
                                |> filter(fn: (r) => r["_measurement"] == "bme280" or r["_measurement"] == "bme680") \
                                |> filter(fn: (r) => r["_field"] == "humidity" or r["_field"] == "temperature") \
                                |> filter(fn: (r) => r["sensor"] == "rpi6" or r["sensor"] == "rpi7") \
                                |> yield(name: "mean")'
                result = query_api.query(org=influx_org, query=query)
                influx_client.close()
                temperature = []
                humidity = []
                for table in result:
                    for record in table.records:
                        if record.get_field() == "humidity":
                            humidity.append(record.get_value())
                        if record.get_field() == "temperature":
                            temperature.append(record.get_value())

                temperature = np.mean(temperature)
                humidity = np.mean(humidity)
            except Exception as e:
                temperature = "NaN"
                humidity = "NaN"
                print("error while getting influx data", e)

            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "create_ts", "correction_factor_module", id
                    FROM "Pulltest_correctionfactor"
                    WHERE create_ts <= %s
                    ORDER BY create_ts DESC
                    LIMIT 1
                    """, [measurement[1]])
            correctionfactor = cur.fetchall()
            cur.close()
            conn.close()
            correction_module = float(correctionfactor[0][1])
            correction_id = int(correctionfactor[0][2])


            data_lot            = measurement[0]
            data_product        = measurement[2]
            data_temperature    = temperature
            data_humidity       = humidity
            data_grade          = 1
            data_timestamp      = measurement[1]
            data_correctionid   = correction_id
            is_module           = False
            is_HV               = True
            pulltest_data = (data_lot, data_product, data_temperature, data_humidity, data_grade, data_timestamp, data_correctionid, is_module, is_HV)

            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            print("insert of this into DCA-DB", measurement)
            cur = conn.cursor()
            cur.execute("""
            INSERT INTO "Pulltest_Measurement" (pulltest_lot, pulltest_product, air_temperature, air_humidity, automatic_grade, timestamp, correction_factor_id, "is_module", "is_HV") VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
            """, pulltest_data)
            conn.commit()
            cur.close()
            conn.close()


            # get pulltest limit data
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "id", corrected_mean, corrected_std_percent, maximum_lifts
                    FROM "Pulltest_Limit"
                    WHERE create_ts <= %s
                    ORDER BY create_ts DESC
                    LIMIT 1
                    """, [data_timestamp])
            pulltest_limit_values = cur.fetchall()[0]
            cur.close()
            conn.close()

            limit_id = pulltest_limit_values[0]
            limit_corrected_mean = pulltest_limit_values[1]
            limit_corrected_std_percent = pulltest_limit_values[2]
            limit_maximum_lifts = pulltest_limit_values[3]


            # get data from pulltester
            conn = psycopg2.connect(host=db_host_pulltester, port=db_port_pulltester, database=db_database_pulltester, user=db_username_pulltester, password=db_password_pulltester)
            cur = conn.cursor()
            cur.execute("""
            SELECT id, tearoffforce, destructed, isfurtflag, gradecode, resultclass, ts
            FROM tbltestresult
            WHERE lot = %s AND product = %s AND ts = %s
            """, (data_lot, data_product, data_timestamp, ))
            pulltest_data = cur.fetchall()
            print(pulltest_data)
            cur.close()
            conn.close()


            # get pulltest measurement id:
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
                    SELECT "id"
                    FROM "Pulltest_Measurement"
                    WHERE pulltest_lot = %s AND pulltest_product = %s AND timestamp = %s
                    """, (data_lot, data_product, data_timestamp, ))
            pulltest_measurement_id = cur.fetchall()[0][0]
            cur.close()
            conn.close()


            all_corrected_tearoffforce = []
            all_gradecode_1 =0
            all_gradecode_2 = 0
            all_gradecode_3 = 0
            all_gradecode_4 = 0
            all_gradecode_5 = 0
            total_measurements = 0

            # write all data to DCA-DB
            for detailed_measurement in pulltest_data:
                total_measurements += 1
                # available data
                # pulltest_measurement_id, data_lot, data_product, data_temperature, data_humidity, data_grade, data_timestamp, data_correctionid
                pulltest_db_id          = detailed_measurement[0]
                tearoffforce            = detailed_measurement[1]
                corrected_tearoffforce  = tearoffforce * correction_module
                all_corrected_tearoffforce.append(corrected_tearoffforce)

                destructed              = detailed_measurement[2]
                isfurtflag              = detailed_measurement[3]
                gradecode               = detailed_measurement[4]
                resultclass             = detailed_measurement[5]
                detailed_timestamp      = detailed_measurement[6]

                all_gradecode_1 = all_gradecode_1 + 1 if gradecode == 1 else all_gradecode_1
                all_gradecode_2 = all_gradecode_2 + 1 if gradecode == 2 else all_gradecode_2
                all_gradecode_3 = all_gradecode_3 + 1 if gradecode == 3 else all_gradecode_3
                all_gradecode_4 = all_gradecode_4 + 1 if gradecode == 4 else all_gradecode_4
                all_gradecode_5 = all_gradecode_5 + 1 if gradecode == 5 else all_gradecode_5

                pulltest_detail_data = (detailed_timestamp, pulltest_db_id, pulltest_measurement_id, tearoffforce, corrected_tearoffforce, isfurtflag, destructed, resultclass, gradecode, correction_id)

                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                print("insert of this into DCA-DB", measurement)
                cur = conn.cursor()
                cur.execute("""
                INSERT INTO "Pulltest_Results" (timestamp, pulltest_db_id, pulltest_measurement_id, tearoffforce, corrected_tearoffforce, isfurtflag, destructed, resultclass, grade, correction_factor_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                """, pulltest_detail_data)
                conn.commit()
                cur.close()
                conn.close()


            mean_corrected_tearoffforce = np.mean(all_corrected_tearoffforce)
            std_limit = mean_corrected_tearoffforce*limit_corrected_std_percent
            std_corrected_teaoffforce   = np.std(all_corrected_tearoffforce)

            automatic_grade_verdict = 3 # Unclear
            if mean_corrected_tearoffforce >= limit_corrected_mean or std_corrected_teaoffforce >= std_limit or all_gradecode_5 + all_gradecode_1 >= total_measurements*0.02:
                automatic_grade_verdict = 2 # 2 means Bad

            if mean_corrected_tearoffforce < limit_corrected_mean and std_corrected_teaoffforce < std_limit and all_gradecode_5 + all_gradecode_1 < total_measurements*0.02:
                automatic_grade_verdict = 1 # Good




            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, database=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            print("update GRADE", measurement)
            cur = conn.cursor()
            cur.execute("""
            UPDATE "Pulltest_Measurement" 
            SET automatic_grade = %s, number_of_tests = %s
            WHERE id = %s
            """, (automatic_grade_verdict, total_measurements, pulltest_measurement_id, ))
            conn.commit()
            cur.close()
            conn.close()



