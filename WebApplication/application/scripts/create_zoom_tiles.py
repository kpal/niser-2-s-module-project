import sys
sys.path.append('/WebApplication/application/scripts/deepzoom')

import deepzoom
import os

for image_file in os.listdir():
    if str(image_file)[-3:] == "jpg":
        SOURCE = image_file
        print(image_file)
        creator = deepzoom.ImageCreator(tile_size=128, tile_overlap=2, tile_format="jpg", image_quality=1, resize_filter="bicubic", )
        creator.create(SOURCE, "zoomify_deep_zoom_tiles/"+ str(image_file) + ".dzi")

