import math
import numpy as np
import pandas as pd
from datetime import datetime, timedelta

from application import db, login_manager, engine
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, Float, Integer, Text, text, Numeric, ForeignKey, Boolean, Date, DateTime, TIMESTAMP, CHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from barcode import EAN13
from barcode.writer import SVGWriter
import barcode
EAN = barcode.get_barcode_class('Gs1_128')

Base = declarative_base()
metadata = Base.metadata

green_color = "#A5D6A7"   # if value is good
red_color   = "#FAC8C8"     # if value is bad
grey_color  = "#C8C8C8"    # if unclear or not checked
temperature_min     = 19
temperature_max     = 24
humidity_min        = 40
humidity_max        = 60

maximum_current_at_600V     = 7.25*10**6 # uA into pA
breakdown_voltage_B = 800 # over 800 Volt
breakdown_voltage_A = 1000 # over 1000 Volt
current_condition_B_factor = 2.5 # 800Volt vs 600Volt
current_condition_A_factor = 2.5 # 1000 Volt vs 800Volt

######################################################
######################################################
# Modules
######################################################
######################################################
class ModuleOverview(db.Model):
    __tablename__ = 'Module_Overview'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Module_Overview_id_seq\"'::regclass)"))
    aachen_module_id = Column(Text, unique=True)
    cern_module_id = Column(Text, unique=True)
    module_type = Column(Text)
    arrival_date = Column(Text)
    shipping_date = Column(Text)
    module_status = Column(Text)
    comment = Column(Text)
    feh_right = Column(Text)
    feh_left = Column(Text)
    seh = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    current_location = Column(Text)
    current_location_timesstamp = Column(DateTime)
    manual_grade = Column(ForeignKey('General_Grade.id'))
    spacing = Column(Text)
    manufacturer = Column(Text)
    cern_db_module_status = Column(Text)
    sensor_top = Column(ForeignKey('Sensor_Overview.sensor_name'))
    sensor_bottom = Column(ForeignKey('Sensor_Overview.sensor_name'))
    feh_right_cic = Column(Text)
    feh_left_cic = Column(Text)
    cern_db_update_ts = Column(DateTime)
    cern_db_description = Column(Text)
    aachen_construction_status = Column(Text)
    cmsdb_upload_status = Column(Boolean)
    cmsdb_upload_timestamp = Column(DateTime)

    General_Grade = relationship('GeneralGrade')
    Sensor_Overview_Bottom = relationship('SensorOverview', primaryjoin='ModuleOverview.sensor_bottom == SensorOverview.sensor_name')
    Sensor_Overview_Top = relationship('SensorOverview', primaryjoin='ModuleOverview.sensor_top == SensorOverview.sensor_name')


    def to_dict(self):
        data = self.cern_module_id
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')

        module_name = self.cern_module_id
        # print(module_name)
        query = '''SELECT defined.timestamp as defined, finished.timestamp as finished, bareFEH.timestamp as bareFEH, bareModule.timestamp as bareModule, bareSEH.timestamp as bareSEH, basePlate.timestamp as basePlate, \
                                bonded.timestamp as bonded, topEncapsulated.timestamp as topEncapsulated, bottomEncapsulated.timestamp as bottomEncapsulated, final.timestamp as final,  finalCold.timestamp as finalCold \
                               FROM "Module_Overview" module \
                               left join "Workflow_aachen_defined" defined on defined.module_name = module.cern_module_id \
                               left join "Workflow_aachen_finished" finished on finished.module_name = module.cern_module_id \
                               left join "Workflow_mod_bareFEH" bareFEH on bareFEH.module_name = module.cern_module_id \
                               left join "Workflow_mod_bareSEH" bareSEH on bareSEH.module_name = module.cern_module_id \
                               left join "Workflow_mod_bareModule" bareModule on bareModule.module_name = module.cern_module_id \
                               left join "Workflow_mod_basePlate" basePlate on basePlate.module_name = module.cern_module_id \
                               left join "Workflow_mod_bonded" bonded on bonded.module_name = module.cern_module_id \
                               left join "Workflow_mod_topEncapsulated" topEncapsulated on topEncapsulated.module_name = module.cern_module_id \
                               left join "Workflow_mod_bottomEncapsulated" bottomEncapsulated on bottomEncapsulated.module_name = module.cern_module_id \
                               left join "Workflow_mod_final" final on final.module_name = module.cern_module_id \
                               left join "Workflow_mod_finalCold" finalCold on finalCold.module_name = module.cern_module_id \
                               where module.cern_module_id = ''' + "'" + str(module_name) + "'"
        df = pd.read_sql_query(query, con=engine)
        columns = df.columns
        list_columns = []
        list_values = []
        for column in df:
            df[column] = df[column].fillna(pd.Timestamp(0))
            # print(df[column])
            list_columns.append(column)
            list_values.append(df[column].max())

        # print(df)
        # print(list_values)
        maximum_index = np.array(list_values).argmax()
        maximum_value = list_values[maximum_index]
        # print("index ", maximum_index)
        # print("value ", maximum_value)
        # maximum_index = np.where(list_values == maximum_value)

        last_assemblystep = str(list_columns[maximum_index]) + " ("+ str(list_values[maximum_index].date()) + ")"
        # last_assemblystep = maximum_index
        # print(list_values)
        # print(list_values[0])
        # print(type(list_values[0]))
        # last_assemblystep = "Database error - not known"

        return {
            'id': self.id,
            'aachen_module_id': self.aachen_module_id,
            'cern_module_id': self.cern_module_id,
            'module_type': self.module_type,
            'arrival_date': self.arrival_date,
            'shipping_date': self.shipping_date,
            'module_status': self.module_status,
            'comment': self.comment,
            'feh_1_id': self.feh_right,
            'feh_2_id': self.feh_left,
            'seh_id': self.seh,
            'barcode': barcode_image,
            'aachen_construction_status': self.aachen_construction_status,
            'last_assemblystep': last_assemblystep,
            'link': '<a href=/modules/module_'+str(self.cern_module_id)+' class="btn btn-primary active" aria-current="page">Link</a>',
            'construction_link': '<a href=/modules/update_module_status_'+str(self.cern_module_id)+' class="btn btn-primary active" aria-current="page">Update status in CMSDB</a>',
        }

class ModuleTool(db.Model):
    __tablename__ = 'Module_Tools'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Module_Tools_id_seq\"'::regclass)"))
    module_id = Column(ForeignKey('Module_Overview.id'))
    jig_baremodule = Column(ForeignKey('Tools_jigs_baremodule.id'))
    jig_bonding = Column(ForeignKey('Tools_jigs_bonding.id'))
    jig_HV_pigtaile = Column(ForeignKey('Tools_jigs_HVpigtaile.id'))

    Tools_jigs_HVpigtaile = relationship('ToolsJigsHVpigtaile')
    Tools_jigs_baremodule = relationship('ToolsJigsBaremodule')
    Tools_jigs_bonding = relationship('ToolsJigsBonding')
    module = relationship('ModuleOverview')


class SensorOverview(db.Model):
    __tablename__ = 'Sensor_Overview'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Sensor_Overview_id_seq\"'::regclass)"))
    sensor_name = Column(Text, unique=True)
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    current_location = Column(Text)
    current_location_timestamp = Column(DateTime)
    manual_grade = Column(ForeignKey('General_Grade.id'))
    module = Column(ForeignKey('Module_Overview.id'))
    sensor_position = Column(Text)
    sensor_known_problem = Column(Text)
    sensor_orientation = Column(Text)
    sensor_type = Column(Text)
    sensor_status = Column(Text)
    sensor_barcode = Column(Text)
    sensor_batch_number = Column(Text)
    sensor_description = Column(Text)
    sensor_id = Column(Integer)
    sensor_kind = Column(Text)
    sensor_kind_id = Column(Integer)
    sensor_manufacturer = Column(Text)
    sensor_production_date = Column(DateTime)
    sensor_assembly_timestamp = Column(DateTime)
    sensor_serial_number = Column(Text)
    sensor_location = Column(Text)
    cern_db_update_ts = Column(DateTime)

    General_Grade = relationship('GeneralGrade')
    Module_Overview = relationship('ModuleOverview', primaryjoin='SensorOverview.module == ModuleOverview.id')

    def to_dict(self):
        data = self.sensor_name
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')

        df = pd.read_sql_query(text(
            '''SELECT \
            bareSensor.timestamp as "1. Bare sensor", \
            polyimide.timestamp as "2. Mod Polyimide", \
            tail.timestamp as "3. tail", \
            tailBonded.timestamp as "4. tailBonded", \
            tailEncapsulated.timestamp as "5. tailEncapsulated" \
            ''' +
            '''FROM "Sensor_Overview" sensor ''' +
            '''left join "Workflow_mod_bareSensor" bareSensor on bareSensor.sensor_name = sensor.sensor_name ''' +
            '''left join "Workflow_mod_polyimide" polyimide on polyimide.sensor_name = sensor.sensor_name ''' +
            '''left join "Workflow_mod_tail" tail on tail.sensor_name = sensor.sensor_name ''' +
            '''left join "Workflow_mod_tailBonded" tailBonded on tailBonded.sensor_name = sensor.sensor_name ''' +
            '''left join "Workflow_mod_tailEncapsulated" tailEncapsulated on tailEncapsulated.sensor_name = sensor.sensor_name ''' +
            '''where sensor.sensor_name = ''' + "'" + str(self.sensor_name) + "'"), con=engine)

        columns = df.columns
        list_columns = []
        list_values = []
        for column in df:
            df[column] = df[column].fillna(pd.Timestamp(0))
            # print(df[column])
            list_columns.append(column)
            list_values.append(df[column].max())

        # print(df)
        # print(list_values)
        maximum_index = np.array(list_values).argmax()
        maximum_value = list_values[maximum_index]
        # print("index ", maximum_index)
        # print("value ", maximum_value)
        # maximum_index = np.where(list_values == maximum_value)

        last_assemblystep = str(list_columns[maximum_index]) + " (" + str(list_values[maximum_index].date()) + ")"
        # last_assemblystep = maximum_index
        # print(list_values)
        # print(list_values[0])
        # print(type(list_values[0]))
        # last_assemblystep = "Database error - not known"



        return {
            'id': self.id,
            'module': self.module,
            'sensor_name': self.sensor_name,
            'current_location': self.current_location,
            'comment': self.comment,
            'barcode': barcode_image,
            'last_assemblystep': last_assemblystep,
            'link': '<a href=/modules/sensor_'+str(self.sensor_name)+' class="btn btn-primary active" aria-current="page">Link</a>',
            'construction_link': '<a href=/modules/update_sensor_status_' + str(self.sensor_id) + ' class="btn btn-primary active" aria-current="page">Update status in CMSDB</a>',
        }

    def to_dict_iv_plot(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name,
            'comment': self.comment,
            'link': '<a href=/iv/plot/sensor_'+str(self.sensor_name)+' class="btn btn-primary active" aria-current="page">View Plot</a>',
        }
######################################################
######################################################
# General
######################################################
######################################################
class GeneralGrade(db.Model):
    __tablename__ = 'General_Grade'

    id              = Column(Integer, primary_key=True, server_default=text("nextval('\"General_Grade_id_seq\"'::regclass)"))
    value           = Column(Text, nullable=False, unique=True)
    comment         = Column(Text)
    color           = Column(Text)
    create_ts       = Column(DateTime, server_default=text("now()"))
    update_ts       = Column(DateTime)
    # metrology_height_reference  = db.relationship("MetrologyHeightProfile", back_populates="grade_reference")

    def to_dict(self):
        return {
            'id': self.id,
            'value': self.value,
            'comment': self.comment,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts
        }

class GeneralStatus(db.Model):
    __tablename__ = 'General_Status'

    id              = Column(Integer, primary_key=True, server_default=text("nextval('\"General_Grade_id_seq\"'::regclass)"))
    name           = Column(Text, nullable=False)
    comment         = Column(Text)
    create_ts       = Column(DateTime, server_default=text("now()"))
    update_ts       = Column(DateTime)

    def to_dict(self):
        data = str(self.name)
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')
        return {
            'id': self.id,
            'name': self.name,
            'comment': self.comment,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
            'barcode': barcode_image
        }
class GeneralOperators(db.Model):
    __tablename__ = 'General_Operators'
    id                  = Column(Integer, primary_key=True, server_default=text("nextval('\"General_Operators_id_seq\"'::regclass)"))
    family_name         = Column(Text)
    first_name          = Column(Text)
    position            = Column(Text)
    comment             = Column(Text)
    setup_metrology     = Column(Boolean)
    setup_pulltester    = Column(Boolean)
    identifier          = Column(Text, unique=True)
    is_used             = Column(Boolean)
    create_ts           = Column(DateTime, server_default=text("now()"))
    update_ts           = Column(DateTime)

    def to_dict(self):
        data = str(self.identifier)
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')
        return {
            'id': self.id,
            'family_name': self.family_name,
            'first_name': self.first_name,
            'position': self.position,
            'setup_metrology': self.setup_metrology,
            'setup_pulltester': self.setup_pulltester,
            'identifier': self.identifier,
            'is_used': self.is_used,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
            'comment': self.comment,
            'barcode': barcode_image
        }

class GeneralStation(db.Model):
    __tablename__ = 'General_Station'

    id          = Column(Integer, primary_key=True)
    name        = Column(Text)
    identifier  = Column(Text, unique=True)
    comment     = Column(Text)
    is_used     = Column(Boolean)
    create_ts   = Column(DateTime, server_default=text("now()"))
    update_ts   = Column(DateTime)
    def to_dict(self):
        data = str(self.identifier)
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')
        return {
            'id': self.id,
            'name': self.name,
            'identifier': self.identifier,
            'is_used': self.is_used,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
            'comment': self.comment,
            'barcode': barcode_image
        }

class GeneralAssemblystep(db.Model):
    __tablename__ = 'General_Assemblystep'

    id = Column(Integer, primary_key=True)
    name = Column(Text)
    comment = Column(Text)
    number = Column(Integer)
    value = Column(Integer)
    run_type = Column(Text)
    def to_dict(self):
        data = str(self.run_type)
        barcode_image = EAN(data)
        barcode_image = barcode_image.render().decode('utf-8')
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'comment': self.comment,
            'barcode': barcode_image,
            'run_type': self.run_type
        }
######################################################
######################################################
# Metrology
######################################################
######################################################
class MetrologyLimit(db.Model):
    __tablename__ = 'Metrology_Limit'

    id              = Column(Integer, primary_key=True, server_default=text("nextval('\"Metrology_Limit_id_seq\"'::regclass)"))
    rotation_urad   = Column(Numeric)
    x_shift_um      = Column(Numeric)
    y_shift_um      = Column(Numeric, nullable=False, server_default=text("nextval('\"Metrology_Limit_y_shift_um_seq\"'::regclass)"))
    comment         = Column(Text)
    create_ts       = Column(DateTime, server_default=text("now()"))
    update_ts       = Column(DateTime)
    timestamp       = Column(DateTime)
    def to_dict(self):
        return {
            'id': self.id,
            'rotation_urad': self.rotation_urad,
            'x_shift_um': self.x_shift_um,
            'y_shift_um': self.y_shift_um,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
            'comment': self.comment
        }

class MetrologyHeightProfile(db.Model):
    __tablename__ = 'Metrology_Height_Profile'

    id                  = Column(Integer, primary_key=True, server_default=text("nextval('\"Metrology_Hight_Profile_id_seq\"'::regclass)"))
    metrology_module    = Column(Text, nullable=False)
    timestamp           = Column(DateTime, nullable=False)
    image_folder        = Column(Text, nullable=False)
    create_ts           = Column(DateTime, server_default=text("now()"))
    module              = Column(ForeignKey('Module_Overview.id'))
    station             = Column(ForeignKey('General_Station.id'))
    operator            = Column(ForeignKey('General_Operators.id'))
    comment             = Column(Text)
    update_ts           = Column(DateTime)
    automatic_grade     = Column(ForeignKey('General_Grade.id'))
    manual_grade        = Column(ForeignKey('General_Grade.id'))
    module_assembly_status = Column(Text)
    air_temperature     = Column(Numeric(5, 2))
    air_humidity        = Column(Numeric(5, 2))
    measurement_status  = Column(ForeignKey('General_Status.id'))
    automatic_grade_limit_id = Column(ForeignKey('Metrology_Limit.id'))

    General_Grade = relationship('GeneralGrade', primaryjoin='MetrologyHeightProfile.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='MetrologyHeightProfile.manual_grade == GeneralGrade.id')
    General_Status = relationship('GeneralStatus', primaryjoin='MetrologyHeightProfile.measurement_status == GeneralStatus.id')
    automatic_grade_limit = relationship('MetrologyLimit')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators', primaryjoin='MetrologyHeightProfile.operator == GeneralOperators.id')
    General_Station = relationship('GeneralStation', primaryjoin='MetrologyHeightProfile.station == GeneralStation.id')


    def to_dict(self):
        return {
            'id': self.id,
            'timestamp': self.timestamp,
            'metrology_module': self.metrology_module,
            'module': self.Module_Overview.cern_module_id,
            'automatic_grade': self.automatic_grade,
            'comment': self.comment,
            'link': '<a href=/metrology/height_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        try:
            temp_color = green_color if 19.0 <= self.air_temperature < 24.0 else red_color
            humidity_color = green_color if 45.0 <= self.air_humidity < 65.0 else red_color
        except:
            temp_color = "#ffffff00"
            humidity_color = "#ffffff00"
        try:
            manual_grade_value = '<p style="background-color:' + self.General_Grade_man.color + '">' + self.General_Grade_man.value + '</p>'
        except:
            manual_grade_value = '<p style="background-color:' + "#ffffff00" + '">' + "n/a" + '</p>'
        try:
            operator_value = self.General_Operators.identifier
        except:
            operator_value = "n/a"
        try:
            station_value = self.General_Station.identifier
        except:
            station_value = "n/a"

        return {
            'id': self.id,
            'module': str(self.metrology_module) + "/" + str(self.Module_Overview.cern_module_id),
            'timestamp': self.timestamp,
            'temperature': '<p style="text-align: center; background-color:' + temp_color + '">' + str(self.air_temperature) + '</p>',
            'humidity': '<p style="text-align: center; background-color:' + humidity_color + '">' + str(self.air_humidity) + '</p>',
            'operator': operator_value,
            'station': station_value,
            # 'automatic_grade': '<p style="text-align: center; background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'manual_grade': manual_grade_value,
            'comment': self.comment,
            'link': '<a href=/metrology/height_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }

class MetrologySensorBridgeAlignment(db.Model):
    __tablename__ = 'Metrology_Sensor_Bridge_Alignment'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Metrology_Sensor_Bridge_Alignment_id_seq\"'::regclass)"))
    metrology_module = Column(Text, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    Left_Bridge_Top_Sensor_x = Column(Numeric, nullable=False)
    Left_Bridge_Top_Sensor_y = Column(Numeric, nullable=False)
    Left_Bridge_Top_Sensor_rad = Column(Numeric, nullable=False)
    Left_Bridge_Bot_Sensor_x = Column(Numeric, nullable=False)
    Left_Bridge_Bot_Sensor_y = Column(Numeric, nullable=False)
    Left_Bridge_Bot_Sensor_rad = Column(Numeric, nullable=False)
    Right_Bridge_Top_Sensor_x = Column(Numeric, nullable=False)
    Right_Bridge_Top_Sensor_y = Column(Numeric, nullable=False)
    Right_Bridge_Top_Sensor_rad = Column(Numeric, nullable=False)
    Right_Bridge_Bot_Sensor_x = Column(Numeric, nullable=False)
    Right_Bridge_Bot_Sensor_y = Column(Numeric, nullable=False)
    Right_Bridge_Bot_Sensor_rad = Column(Numeric, nullable=False)
    Upper_Stump_Top_Sensor_x = Column(Numeric, nullable=False)
    Upper_Stump_Top_Sensor_y = Column(Numeric, nullable=False)
    Upper_Stump_Bot_Sensor_x = Column(Numeric, nullable=False)
    Upper_Stump_Bot_Sensor_y = Column(Numeric, nullable=False)
    Lower_Stump_Top_Sensor_x = Column(Numeric, nullable=False)
    Lower_Stump_Top_Sensor_y = Column(Numeric, nullable=False)
    Lower_Stump_Bot_Sensor_x = Column(Numeric, nullable=False)
    Lower_Stump_Bot_Sensor_y = Column(Numeric, nullable=False)
    measurement_status = Column(ForeignKey('General_Status.id'))
    operator = Column(ForeignKey('General_Operators.id'), ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'), ForeignKey('General_Station.id'))
    module = Column(ForeignKey('Module_Overview.id'))
    create_ts = Column(DateTime, server_default=text("now()"))
    comment = Column(Text)
    update_ts = Column(DateTime, server_default=text("now()"))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    manual_grade = Column(ForeignKey('General_Grade.id'))
    module_assembly_status = Column(Text)
    automatic_grade_limit_id = Column(ForeignKey('Metrology_Limit.id'))

    General_Grade = relationship('GeneralGrade', primaryjoin='MetrologySensorBridgeAlignment.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='MetrologySensorBridgeAlignment.manual_grade == GeneralGrade.id')
    automatic_grade_limit = relationship('MetrologyLimit')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators', primaryjoin='MetrologySensorBridgeAlignment.operator == GeneralOperators.id')
    General_Station = relationship('GeneralStation', primaryjoin='MetrologySensorBridgeAlignment.station == GeneralStation.id')
    General_Status = relationship('GeneralStatus', primaryjoin='MetrologySensorBridgeAlignment.measurement_status == GeneralStatus.id')
    # module_reference = db.relationship("ModuleOverview", back_populates="metrology_bridge_reference")

    def to_dict(self):
        return {
            'id': self.id,
            'metrology_module': self.metrology_module,
            'timestamp': self.timestamp,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'comment': self.comment,
            'link': '<a href=/metrology/bridge_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
            try:
                temp_color = green_color if 19.0 <= float(self.air_temperature) < 24.0 else red_color
                humidity_color = green_color if 45.0 <= float(self.air_humidity) < 65.0 else red_color
            except Exception as e:
                temp_color = grey_color
                humidity_color = grey_color
            try:
                manual_grade_value = '<p style="background-color:' + self.General_Grade_man.color + '">' + self.General_Grade_man.value + '</p>'
            except:
                manual_grade_value = '<p style="background-color:' + "#ffffff00" + '">' + "n/a" + '</p>'
            try:
                operator_value = self.General_Operators.identifier
            except:
                operator_value = "n/a"
            try:
                station_value = self.General_Station.identifier
            except:
                station_value = "n/a"
            try:
                cern_module_id = self.Module_Overview.cern_module_id
            except:
                cern_module_id = self.metrology_module

            return {
                'id': self.id,
                'module': str(self.metrology_module) + "/" + str(cern_module_id),
                'timestamp': self.timestamp,
                'temperature': '<p style="text-align: center; background-color:' + temp_color + '">' + str(self.air_temperature) + '</p>',
                'humidity': '<p style="text-align: center; background-color:' + humidity_color + '">' + str(self.air_humidity) + '</p>',
                'operator': operator_value ,
                'station': station_value,
                # 'automatic_grade': '<p style="text-align: center; background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
                'manual_grade': manual_grade_value,
                'comment': self.comment,
                'link': '<a href=/metrology/bridge_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
            }

class MetrologySensorSensorAlignment(db.Model):
    __tablename__ = 'Metrology_Sensor_Sensor_Alignment'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Metrology_Sensor_Sensor_Alignment_id_seq\"'::regclass)"))
    metrology_module = Column(Text, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    rotation_urad = Column(Numeric)
    x_shift_um = Column(Numeric, nullable=False)
    y_shift_um = Column(Numeric, nullable=False)
    comment = Column(Text)
    image_folder = Column(Text, nullable=False)
    metrology_config_file = Column(Text, nullable=False)
    metrology_config_mounted = Column(Text, nullable=False)
    metrology_config_name = Column(Text, nullable=False)
    measurement_status = Column(ForeignKey('General_Status.id'))
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    module = Column(ForeignKey('Module_Overview.id'))
    create_ts = Column(DateTime, server_default=text("now()"))
    module_assembly_status = Column(Text)
    manual_grade = Column(ForeignKey('General_Grade.id'))
    update_ts = Column(DateTime)
    automatic_grade_limit_id = Column(ForeignKey('Metrology_Limit.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    cmsdb_upload_status = Column(Boolean)
    cmsdb_upload_timestamp = Column(DateTime)

    automatic_grade_limit = relationship('MetrologyLimit')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    General_Station = relationship('GeneralStation')
    # General_Status = relationship('GeneralStatus')
    # General_Status = relationship('GeneralStatus', primaryjoin='MetrologySensorSensorAlignment.measurement_status == GeneralStatus.id')
    General_Status = relationship('GeneralStatus', primaryjoin='MetrologySensorSensorAlignment.measurement_status == GeneralStatus.id')
    General_Grade = relationship('GeneralGrade', primaryjoin='MetrologySensorSensorAlignment.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='MetrologySensorSensorAlignment.manual_grade == GeneralGrade.id')

    # module_reference = db.relationship("ModuleOverview", back_populates="metrology_sensor_reference")

    def to_dict(self):
        try:
            cern_module_id = self.Module_Overview.cern_module_id
        except:
            cern_module_id = self.metrology_module
        return {
            'id': self.id,
            'metrology_module': self.metrology_module + " / " + cern_module_id,
            'timestamp': self.timestamp,
            'rotation_urad': self.rotation_urad,
            'x_shift_um': self.x_shift_um,
            'y_shift_um': self.y_shift_um,
            'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'comment': self.comment,

            'link': '<a href=/metrology/sensor_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        try:
            manual_grade_value = '<p style="background-color:' + self.General_Grade_man.color + '">' + self.General_Grade_man.value + '</p>'
        except:
            manual_grade_value = '<p style="background-color:' + "#ffffff00" + '">' + "n/a" + '</p>'
        try:
            operator_value = self.General_Operators.identifier
        except:
            operator_value = "n/a"
        try:
            station_value = self.General_Station.identifier
        except:
            station_value = "n/a"
        try:
            cern_module_id = self.Module_Overview.cern_module_id
        except:
            cern_module_id = self.metrology_module

        if self.cmsdb_upload_status == True:
            cmsdb_upload_status_color = "success"
        elif self.cmsdb_upload_status == False:
            cmsdb_upload_status_color = "danger"
        else:
            cmsdb_upload_status_color = "secondary"

        limit_data = MetrologyLimit.query.order_by("timestamp").limit(1).all()
        for limit in limit_data:
            limit_rotation = limit.rotation_urad
            limit_x_shift = limit.x_shift_um
            limit_y_shift = limit.y_shift_um

        rotation_color = red_color if abs(self.rotation_urad) >= limit_rotation else green_color
        x_shift_color = red_color if abs(self.x_shift_um) >= limit_x_shift else green_color
        y_shift_color = red_color if abs(self.y_shift_um) >= limit_y_shift else green_color

        temp_color = green_color if temperature_min <= self.air_temperature < temperature_max else red_color
        humidity_color = green_color if humidity_min <= self.air_humidity < humidity_max else red_color


        return {
            'id': self.id,
            'module': str(self.metrology_module) + "/" + str(cern_module_id),
            'timestamp': self.timestamp,
            'rotation_urad': '<p style="text-align: center; background-color:' + rotation_color + '">' + str(self.rotation_urad) + '</p>',
            'x_shift_um': '<p style="text-align: center; background-color:' + x_shift_color + '">' + str(self.x_shift_um) + '</p>',
            'y_shift_um': '<p style="text-align: center; background-color:' + y_shift_color + '">' + str(self.y_shift_um) + '</p>',
            'comment': self.comment,
            # 'status': self.General_Status.name,
            'temperature': '<p style="text-align: center; background-color:' + temp_color + '">' + str(self.air_temperature) + '</p>',
            'humidity': '<p style="text-align: center; background-color:' + humidity_color + '">' + str(self.air_humidity) + '</p>',
            'operator': operator_value ,
            'station': station_value,
            'automatic_grade': '<p style="text-align: center; background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'manual_grade': manual_grade_value,
            'link': '<a href=/metrology/sensor_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a> '
                                                              '<i class="fas fa-database text-' + cmsdb_upload_status_color + '"></i>',
        }
######################################################
######################################################
# Pulltest
######################################################
######################################################

class PulltestLimit(db.Model):
    __tablename__ = 'Pulltest_Limit'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_Limit_id_seq\"'::regclass)"))
    timestamp = Column(DateTime)
    uncorrected_factor = Column(Numeric)
    corrected_factor = Column(Numeric)
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)

class PulltestCorrectionfactor(db.Model):
    __tablename__ = 'Pulltest_correctionfactor'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_correction_factor_id_seq\"'::regclass)"))
    create_ts = Column(DateTime, nullable=False, unique=True, server_default=text("now()"))
    correction_factor_module = Column(Numeric)
    correction_factor_HV = Column(Numeric)
    comment = Column(Text)
    update_ts = Column(DateTime)

    def to_dict(self):
        return {
            'id': self.id,
            'correction_factor_module': self.correction_factor_module,
            'correction_factor_HV': self.correction_factor_HV,
            'comment': self.comment,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
            }
class PulltestProduct(db.Model):
    __tablename__   = 'Pulltest_product'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_product_id_seq\"'::regclass)"))
    name = Column(Text, nullable=False)
    pulltest_db_id = Column(Integer, nullable=False, unique=True)
    comment = Column(Text)
    headtype = Column(Text)
    _2s_modules_used = Column('2s_modules_used', Boolean)
    pulltest_module = Column('pulltest_module', Boolean)
    pulltest_HV = Column('pulltest_HV', Boolean)
    create_ts = Column(DateTime, nullable=False, unique=True, server_default=text("now()"))
    update_ts = Column(DateTime)
    type = Column(Text)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'pulltest_db_id': self.pulltest_db_id,
            'comment': self.comment,
            'headtype': self.headtype,
            '_2s_modules_used': self._2s_modules_used,
            'pulltest_module': self.pulltest_module,
            'pulltest_HV': self.pulltest_HV,
            'type': self.type,
            'create_ts': self.create_ts,
            'update_ts': self.update_ts,
        }

class PulltestMeasurement(db.Model):
    __tablename__ = 'Pulltest_Measurement'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_Measurement_id_seq\"'::regclass)"))
    module = Column(ForeignKey('Module_Overview.id'))
    pulltest_lot = Column(Integer, unique=True)
    pulltest_product = Column(ForeignKey('Pulltest_product.pulltest_db_id'))
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    manual_grade = Column(ForeignKey('General_Grade.id'))
    measurement_status = Column(ForeignKey('General_Status.id'))
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    pulltest_comment_title = Column(Text)
    pulltest_comment_lot = Column(Text)
    pulltest_comment_comment = Column(Text)
    timestamp = Column(DateTime, unique=True)
    correction_factor_id = Column(ForeignKey('Pulltest_correctionfactor.id'))
    is_module = Column(Boolean)
    is_HV = Column(Boolean)
    number_of_tests = Column(Integer)
    cmsdb_upload_status = Column(Boolean)
    cmsdb_upload_timestamp = Column(DateTime)
    position = Column(Text)
    assemblystep = Column(ForeignKey('General_Assemblystep.run_type'))

    General_Grade = relationship('GeneralGrade', primaryjoin='PulltestMeasurement.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='PulltestMeasurement.manual_grade == GeneralGrade.id')
    General_Status = relationship('GeneralStatus')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    Pulltest_product = relationship('PulltestProduct')
    General_Station = relationship('GeneralStation')
    General_Assemblystep = relationship('GeneralAssemblystep')

    def to_dict(self):
        return {
            'id': self.id,
            'lot': self.pulltest_lot,
            'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'number_of_tests': self.number_of_tests,
            'pulltest_comment_title': self.pulltest_comment_title,
            'pulltest_comment_lot': self.pulltest_comment_lot,
            'pulltest_comment_comment': self.pulltest_comment_comment,
            'timestamp': self.timestamp,
            'link': '<a href=/pulltest/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        try:
            operator_name = General_Operators.name
        except:
            operator_name = ""
        try:
            module_name = Module_Overview.cern_module_id
        except:
            module_name = self.module

        if self.cmsdb_upload_status == True:
            cmsdb_upload_status_color = "success"
        elif self.cmsdb_upload_status == False:
            cmsdb_upload_status_color = "danger"
        else:
            cmsdb_upload_status_color = "secondary"

        return {
            'module': module_name,
            'comment': self.comment,
            'operator': operator_name,
            'station': self.station,
            'product': self.pulltest_product,
            'id': self.id,
            'lot': self.pulltest_lot,
            'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'number_of_tests': self.number_of_tests,
            'timestamp': self.timestamp,
            'link': '<a href=/pulltest/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>'
                                                             '<i class="fas fa-database text-' + cmsdb_upload_status_color + '"></i>',
        }

# class PulltestResultsModule(db.Model):
#     __tablename__   = 'Pulltest_results_module'
#
#     id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_results_id_seq\"'::regclass)"))
#     pulltest_db_lot = Column(Integer)
#     module = Column(ForeignKey('Module_Overview.id'))
#     pulltest_db_tearoffforce = Column(Numeric, nullable=False)
#     pulltest_db_gradecode = Column(Integer)
#     pulltest_db_resultclass = Column(Text)
#     comment = Column(Text)
#     timestamp = Column(DateTime)
#     air_temperature = Column(Numeric(5, 2))
#     air_humidity = Column(Numeric(5, 2))
#     pulltest_db_grade = Column(Text)
#     measurement_status = Column(Text)
#     corrected_tearoffforce = Column(Numeric)
#     operator = Column(ForeignKey('General_Operators.id'))
#     station = Column(ForeignKey('General_Station.id'))
#     pulltest_db_id = Column(Integer, nullable=False)
#     pulltest_db_comment = Column(Text)
#     pulltest_db_module = Column(Text)
#     pulltest_db_product = Column(ForeignKey('Pulltest_product.pulltest_db_id'), nullable=False)
#     create_ts = Column(DateTime, server_default=text("now()"))
#     correctionfactor_id = Column(ForeignKey('Pulltest_correctionfactor.id'))
#     automatic_grade = Column(ForeignKey('General_Grade.id'))
#     manual_grade = Column(Integer)
#     automatic_grade_limit_id = Column(ForeignKey('Pulltest_Limit.id'))
#     update_ts = Column(DateTime)
#
#     General_Grade = relationship('GeneralGrade')
#     automatic_grade_limit = relationship('PulltestLimit')
#     correctionfactor = relationship('PulltestCorrectionFactor')
#     Module_Overview = relationship('ModuleOverview')
#     General_Operators = relationship('GeneralOperators')
#     Pulltest_product = relationship('PulltestProduct')
#     General_Station = relationship('GeneralStation')
#
#     # module_reference = db.relationship("ModuleOverview", back_populates="pulltest_module_reference")
#
#     def to_dict(self):
#         return {
#             'id': self.id,
#             'lot': self.pulltest_db_lot,
#             'module': self.module,
#             'tearoffforce': format(self.pulltest_db_tearoffforce, '.3f'),
#             'gradecode': self.pulltest_db_gradecode,
#             'resultclass': self.pulltest_db_resultclass,
#             'comment': self.comment,
#             'timestamp': self.timestamp,
#             'air_temperature': self.air_temperature,
#             'air_humidity': self.air_humidity,
#             'automatic_grade': self.automatic_grade,
#             'status': self.measurement_status,
#             'corrected_tearoffforce': format(self.corrected_tearoffforce, '.3f'),
#             'link': '<a href=/pulltest/module_id_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
#
#     def to_dict_all(self):
#         return {
#             'id': self.id,
#             'lot': self.pulltest_db_lot,
#             'module': self.module,
#             # 'module_cern_id': self.module_reference.CERN_Module_id,
#             'tearoffforce': self.pulltest_db_tearoffforce,
#             'gradecode': self.pulltest_db_gradecode,
#             'resultclass': self.pulltest_db_resultclass,
#             'comment': self.comment,
#             'timestamp': self.timestamp,
#             'air_temperature': self.air_temperature,
#             'air_humidity': self.air_humidity,
#             'automatic_grade': self.automatic_grade,
#             'manual_grade': self.manual_grade,
#             'status': self.measurement_status,
#             'corrected_tearoffforce': self.corrected_tearoffforce,
#             'operator': self.operator,
#             'station': self.station,
#             'pulltest_db_id': self.pulltest_db_id,
#             'pulltest_db_module': self.pulltest_db_module,
#             'pulltest_db_product': self.pulltest_db_product,
#             # 'pulltest_product_name': self.pulltest_product_reference.name,
#             'link': '<a href=/pulltest/module_timestamp_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
#
#     def to_dict_unique_time(self):
#         return {
#             'timestamp': self.timestamp,
#             'link': '<a href=/pulltest/id_'+str(self.timestamp.isoformat())+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
#
#
# class PulltestResultsHV(db.Model):
#     __tablename__   = 'Pulltest_results_HV'
#
#     id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_results_id_seq\"'::regclass)"))
#     pulltest_db_lot = Column(Integer)
#     module = Column(ForeignKey('Module_Overview.id'))
#     pulltest_db_tearoffforce = Column(Numeric, nullable=False)
#     pulltest_db_gradecode = Column(Integer)
#     pulltest_db_resultclass = Column(Text)
#     comment = Column(Text)
#     timestamp = Column(DateTime)
#     air_temperature = Column(Numeric(5, 2))
#     air_humidity = Column(Numeric(5, 2))
#     pulltest_db_grade = Column(Text)
#     measurement_status = Column(Text)
#     corrected_tearoffforce = Column(Numeric)
#     operator = Column(ForeignKey('General_Operators.id'))
#     station = Column(ForeignKey('General_Station.id'))
#     pulltest_db_id = Column(Integer, nullable=False)
#     pulltest_db_comment = Column(Text)
#     pulltest_db_module = Column(Text)
#     pulltest_db_product = Column(ForeignKey('Pulltest_product.pulltest_db_id'), nullable=False)
#     create_ts = Column(DateTime, server_default=text("now()"))
#     correctionfactor_id = Column(ForeignKey('Pulltest_correctionfactor.id'))
#     automatic_grade = Column(ForeignKey('General_Grade.id'))
#     manual_grade = Column(Integer)
#     automatic_grade_limit_id = Column(ForeignKey('Pulltest_Limit.id'))
#     update_ts = Column(DateTime)
#
#     General_Grade = relationship('GeneralGrade')
#     automatic_grade_limit = relationship('PulltestLimit')
#     correctionfactor = relationship('PulltestCorrectionFactor')
#     Module_Overview = relationship('ModuleOverview')
#     General_Operators = relationship('GeneralOperators')
#     Pulltest_product = relationship('PulltestProduct')
#     General_Station = relationship('GeneralStation')
#
#     # module_reference = db.relationship("ModuleOverview", back_populates="pulltest_HV_reference")
#     def to_dict(self):
#         return {
#             'id': self.id,
#             'module': self.module,
#             'tearoffforce': format(self.pulltest_db_tearoffforce, '.3f'),
#             'gradecode': self.pulltest_db_gradecode,
#             'resultclass': self.pulltest_db_resultclass,
#             'comment': self.comment,
#             'timestamp': self.timestamp,
#             'air_temperature': self.air_temperature,
#             'air_humidity': self.air_humidity,
#             'automatic_grade': self.automatic_grade,
#             'status': self.measurement_status,
#             'corrected_tearoffforce': format(self.corrected_tearoffforce, '.3f'),
#             'link': '<a href=/pulltest/HV_id_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
#     def to_dict_all(self):
#         return {
#             'id': self.id,
#             'lot': self.pulltest_db_lot,
#             'module': self.module,
#             # 'module_cern_id': self.module_reference.CERN_Module_id,
#             'tearoffforce': self.pulltest_db_tearoffforce,
#             'gradecode': self.pulltest_db_gradecode,
#             'resultclass': self.pulltest_db_resultclass,
#             'comment': self.comment,
#             'timestamp': self.timestamp,
#             'air_temperature': self.air_temperature,
#             'air_humidity': self.air_humidity,
#             'automatic_grade': self.automatic_grade,
#             'manual_grade': self.manual_grade,
#             'status': self.measurement_status,
#             'corrected_tearoffforce': self.corrected_tearoffforce,
#             'operator': self.operator,
#             'station': self.station,
#             'pulltest_db_id': self.pulltest_db_id,
#             'pulltest_db_module': self.pulltest_db_module,
#             'pulltest_db_product': self.pulltest_db_product,
#             # 'pulltest_product_name': self.pulltest_product_reference.name,
#             'link': '<a href=/pulltest/id_'+str(self.id)+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
#
#     def to_dict_unique_time(self):
#         return {
#             'timestamp': self.timestamp,
#             'link': '<a href=/pulltest/HV_timestamp_'+str(self.timestamp.isoformat())+' class="btn btn-primary active" aria-current="page">Link</a>',
#         }
class PulltestResult(db.Model):
    __tablename__ = 'Pulltest_Results'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Pulltest_Results_id_seq\"'::regclass)"))
    pulltest_measurement_id = Column(ForeignKey('Pulltest_Measurement.id'))
    tearoffforce = Column(Numeric(6, 3))
    correction_factor_tearoffforce = Column(Numeric(6, 3))
    resultclass = Column(Text)
    grade = Column(Text, comment='pulltest internal grade')
    correction_factor_id = Column(ForeignKey('Pulltest_correctionfactor.id'))
    isfurtflag = Column(Text)
    destructed = Column(Text)
    pulltest_db_id = Column(Integer)
    timestamp = Column(DateTime)

    correction_factor = relationship('PulltestCorrectionfactor')
    pulltest_measurement = relationship('PulltestMeasurement')

    def to_dict(self):
        return {
            'id': self.id,
            'pulltest_measurement_id': self.pulltest_measurement_id,
            'tearoffforce': self.tearoffforce,
            'corrected_tearoffforce': self.tearoffforce*self.correction_factor_tearoffforce,
            'resultclass': self.resultclass,
            'grade': self.grade,
            'correction_factor_id': self.correction_factor_id,
        }



class IVMeasurementResult(db.Model):
    __tablename__ = 'IV_Measurement_Results'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"IV_Measurement_Results_id_seq\"'::regclass)"))
    IV_measurement_id = Column(ForeignKey('IV_Measurement.id'))
    voltage = Column(Numeric(6, 2), comment='U [V]')
    current = Column(Numeric(9, 4), comment='I [pA]')
    timestamp = Column(Numeric(12, 1))
    humidity = Column(Numeric(6, 3), comment='setup humidity rel. H [%]')
    temperature = Column(Numeric(6, 3), comment='setup temperature  T_air [°C]')
    IV_measurement = relationship('IVMeasurement')


    def to_dict(self):
        dt_base = datetime(1904, 1, 1, 0, 0, 0)
        dt = dt_base + timedelta(seconds=float(self.timestamp))
        dt = dt.replace(microsecond=0)
        timestamp_datetime = dt
        return {
            'id': self.id,
            # 'IV_measurement_id': self.IV_measurement_id,
            'voltage': self.voltage,
            'current': self.current,
            'timestamp': timestamp_datetime,
            'humidity': self.humidity,
            'temperature': self.temperature,
        }
class IVMeasurement(db.Model):
    __tablename__ = 'IV_Measurement'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"IV_Measurement_id_seq\"'::regclass)"))
    sensor = Column(ForeignKey('Sensor_Overview.id'))
    iv_file_sensor = Column(Text)
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    repetition_number = Column(Integer)
    datapoints_number = Column(Integer)
    timestamp = Column(DateTime)
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    manual_grade = Column(ForeignKey('General_Grade.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    iv_file_module = Column(Text)
    iv_file_operator = Column(Text)
    assembly_step = Column(Text)
    bad_measurement = Column(Boolean, comment='if true, measurement is not shown anywhere')
    module = Column(ForeignKey('Module_Overview.id'))

    General_Grade = relationship('GeneralGrade', primaryjoin='IVMeasurement.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='IVMeasurement.manual_grade == GeneralGrade.id')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    General_Station = relationship('GeneralStation')

    def to_dict(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/iv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/iv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_grading(self):
        limit_data = {}
        limit_colors = {}

        df = pd.read_sql_query(text('''SELECT voltage, current, humidity, temperature, ramp_direction  FROM "IV_Measurement_Results" WHERE "IV_measurement_id" = ''' + str(self.id) + ";"), con=engine)
        maximum_voltage = df['voltage'].min()
        limit_data['maximum_voltage'] = maximum_voltage
        limit_colors['maximum_voltage'] = red_color
        if maximum_voltage <= -1000:
            limit_colors['maximum_voltage'] = green_color
        if -1000 < maximum_voltage <= -800:
            limit_colors['maximum_voltage'] = grey_color

        try:
            current_value_at_600V = round(df[df['voltage'] == -600]['current'].mean(), 2)
            current_value_at_800V = round(df[df['voltage'] == -800]['current'].mean(), 2)
            current_value_at_1000V = round(df[df['voltage'] == -1000]['current'].mean(), 2)
            limit_data['current_value_at_600V'] = current_value_at_600V
            limit_data['current_value_at_800V']   = current_value_at_800V
            limit_data['current_value_at_1000V']  = current_value_at_1000V
            limit_data['current_800/600'] = round(current_value_at_800V / current_value_at_600V, 1)
            limit_colors['current_800/600'] = green_color if limit_data['current_800/600'] < 2.5 else red_color
            limit_data['current_1000/800'] = round(current_value_at_1000V / current_value_at_800V, 1)
            limit_colors['current_1000/800'] = green_color if limit_data['current_1000/800'] < 2.5 else red_color

            limit_colors['current_value_at_600V'] = green_color if current_value_at_600V <= maximum_current_at_600V else red_color
        except:
            pass
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'maximum_voltage': '<p style="background-color:' + limit_colors['maximum_voltage'] + '">' + str(limit_data['maximum_voltage']) + '</p>',
            'current_value_at_600V': '<p style="background-color:' + limit_colors['current_value_at_600V'] + '">' + str(limit_data['current_value_at_600V']) + '</p>',
            'current_800/600': '<p style="background-color:' + limit_colors['current_800/600'] + '">' + str(limit_data['current_800/600']) + '</p>',
            'current_1000/800': '<p style="background-color:' + limit_colors['current_1000/800'] + '">' + str(limit_data['current_1000/800']) + '</p>',
            'link': '<a href=/iv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }

class IVtMeasurement(db.Model):
    __tablename__ = 'IVt_Measurement'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"IVt_Measurement_id_seq\"'::regclass)"))
    sensor = Column(ForeignKey('Sensor_Overview.id'))
    iv_file_sensor = Column(Text)
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    repetition_number = Column(Integer)
    datapoints_number = Column(Integer)
    timestamp = Column(DateTime)
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    manual_grade = Column(ForeignKey('General_Grade.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    iv_file_module = Column(Text)
    iv_file_operator = Column(Text)
    assembly_step = Column(Text)
    bad_measurement = Column(Boolean, comment='if true, measurement is not shown anywhere')
    module = Column(ForeignKey('Module_Overview.id'))

    General_Grade = relationship('GeneralGrade', primaryjoin='IVtMeasurement.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='IVtMeasurement.manual_grade == GeneralGrade.id')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    General_Station = relationship('GeneralStation')
    def to_dict(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/iv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/ivt/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_grading(self):
        limit_data = {}
        limit_colors = {}

        df = pd.read_sql_query(text('''SELECT voltage, current, humidity, temperature, ramp_direction  FROM "IV_Measurement_Results" WHERE "IV_measurement_id" = ''' + str(self.id) + ";"), con=engine)
        maximum_voltage = df['voltage'].min()
        limit_data['maximum_voltage'] = maximum_voltage
        limit_colors['maximum_voltage'] = red_color
        if maximum_voltage <= -1000:
            limit_colors['maximum_voltage'] = green_color
        if -1000 < maximum_voltage <= -800:
            limit_colors['maximum_voltage'] = grey_color

        try:
            current_value_at_600V = round(df[df['voltage'] == -600]['current'].mean(), 2)
            current_value_at_800V = round(df[df['voltage'] == -800]['current'].mean(), 2)
            current_value_at_1000V = round(df[df['voltage'] == -1000]['current'].mean(), 2)
            limit_data['current_value_at_600V'] = current_value_at_600V
            limit_data['current_value_at_800V']   = current_value_at_800V
            limit_data['current_value_at_1000V']  = current_value_at_1000V
            limit_data['current_800/600'] = round(current_value_at_800V / current_value_at_600V, 1)
            limit_colors['current_800/600'] = green_color if limit_data['current_800/600'] < 2.5 else red_color
            limit_data['current_1000/800'] = round(current_value_at_1000V / current_value_at_800V, 1)
            limit_colors['current_1000/800'] = green_color if limit_data['current_1000/800'] < 2.5 else red_color

            limit_colors['current_value_at_600V'] = green_color if current_value_at_600V <= maximum_current_at_600V else red_color
        except:
            pass
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'maximum_voltage': '<p style="background-color:' + limit_colors['maximum_voltage'] + '">' + str(limit_data['maximum_voltage']) + '</p>',
            'current_value_at_600V': '<p style="background-color:' + limit_colors['current_value_at_600V'] + '">' + str(limit_data['current_value_at_600V']) + '</p>',
            'current_800/600': '<p style="background-color:' + limit_colors['current_800/600'] + '">' + str(limit_data['current_800/600']) + '</p>',
            'current_1000/800': '<p style="background-color:' + limit_colors['current_1000/800'] + '">' + str(limit_data['current_1000/800']) + '</p>',
            'link': '<a href=/ivt/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
class IVtMeasurementResult(db.Model):
    __tablename__ = 'IVt_Measurement_Results'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"IVt_Measurement_Results_id_seq\"'::regclass)"))
    IVt_measurement_id = Column(ForeignKey('IVt_Measurement.id'))
    voltage = Column(Numeric(6, 2), comment='U [V]')
    current = Column(Numeric(10, 4), comment='I [pA]')
    timestamp = Column(Numeric(12, 1))
    humidity = Column(Numeric(6, 3), comment='setup humidity rel. H [%]')
    temperature = Column(Numeric(6, 3), comment='setup temperature  T_air [°C]')
    ramp_direction = Column(CHAR(1))

    IVt_measurement = relationship('IVtMeasurement')
    def to_dict(self):
        dt_base = datetime(1904, 1, 1, 0, 0, 0)
        dt = dt_base + timedelta(seconds=float(self.timestamp))
        timestamp_datetime = dt
        return {
            'id': self.id,
            # 'IV_measurement_id': self.IV_measurement_id,
            'voltage': self.voltage,
            'current': self.current,
            'timestamp': timestamp_datetime,
            'humidity': self.humidity,
            'temperature': self.temperature,
        }


class CMSIVMeasurement(db.Model):
    __tablename__ = 'CMS_IV_Measurement'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"CMS_IV_Measurement_id_seq\"'::regclass)"))
    cern_db_id = Column(Integer)
    begin_date = Column(Text)
    end_date = Column(Text)
    location = Column(Text)
    run_type = Column(Text)
    run_number = Column(Integer)
    description = Column(Text)
    name = Column(Text)
    sensor = Column(Text)
    mean_temp = Column(Float(53))
    mean_hum = Column(Float(53))
class CMSIVMeasurementResult(db.Model):
    __tablename__ = 'CMS_IV_Measurement_Results'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"CMS_IV_Measurement_Results_id_seq\"'::regclass)"))
    sensor = Column(Text)
    test_type = Column(Text, comment='U [V]')
    run_name = Column(Text)
    volts = Column(Float(53))
    currnt_amp = Column(Numeric(9, 4))
    temp_degc = Column(Numeric(6, 3))
    rh_prcnt = Column(Numeric(6, 3))
    run_number = Column(Integer)
    run_type = Column(Text)
    run_type_number = Column(Text)

class HVMeasurementResult(db.Model):
    __tablename__ = 'HV_Measurement_Results'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"HV_Measurement_Results_id_seq\"'::regclass)"))
    HV_measurement_id = Column(ForeignKey('HV_Measurement.id'))
    voltage = Column(Numeric(6, 2), comment='U [V]')
    current = Column(Numeric(9, 4), comment='I [pA]')
    timestamp = Column(Numeric(12, 1))
    humidity = Column(Numeric(6, 3), comment='setup humidity rel. H [%]')
    temperature = Column(Numeric(6, 3), comment='setup temperature  T_air [°C]')
    ramp_direction = Column(CHAR(1))

    HV_measurement = relationship('HVMeasurement')
    def to_dict(self):
        dt_base = datetime(1904, 1, 1, 0, 0, 0)
        dt = dt_base + timedelta(seconds=float(self.timestamp))
        timestamp_datetime = dt
        return {
            'id': self.id,
            # 'IV_measurement_id': self.IV_measurement_id,
            'voltage': self.voltage,
            'current': self.current,
            'timestamp': timestamp_datetime,
            'humidity': self.humidity,
            'temperature': self.temperature,
        }
class HVMeasurement(db.Model):
    __tablename__ = 'HV_Measurement'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"HV_Measurement_id_seq\"'::regclass)"))
    sensor = Column(ForeignKey('Sensor_Overview.id'))
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    repetition_number = Column(Integer)
    datapoints_number = Column(Integer)
    timestamp = Column(DateTime)
    automatic_grade = Column(ForeignKey('General_Grade.id'))
    manual_grade = Column(ForeignKey('General_Grade.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    iv_file_sensor = Column(Text)
    iv_file_module = Column(Text)
    iv_file_operator = Column(Text)
    assembly_step = Column(Text)
    bad_measurement = Column(Boolean, comment='if true, measurement is not shown anywhere')
    module = Column(ForeignKey('Module_Overview.id'))

    General_Grade = relationship('GeneralGrade', primaryjoin='HVMeasurement.automatic_grade == GeneralGrade.id')
    General_Grade_man = relationship('GeneralGrade', primaryjoin='HVMeasurement.manual_grade == GeneralGrade.id')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    General_Station = relationship('GeneralStation')
    def to_dict(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/hv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/hv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_grading(self):
        limit_data = {}
        limit_colors = {}

        df = pd.read_sql_query(text('''SELECT voltage, current, humidity, temperature, ramp_direction  FROM "IV_Measurement_Results" WHERE "IV_measurement_id" = ''' + str(self.id) + ";"), con=engine)
        maximum_voltage = df['voltage'].min()
        limit_data['maximum_voltage'] = maximum_voltage
        limit_colors['maximum_voltage'] = red_color
        if maximum_voltage <= -1000:
            limit_colors['maximum_voltage'] = green_color
        if -1000 < maximum_voltage <= -800:
            limit_colors['maximum_voltage'] = grey_color

        try:
            current_value_at_600V = round(df[df['voltage'] == -600]['current'].mean(), 2)
            current_value_at_800V = round(df[df['voltage'] == -800]['current'].mean(), 2)
            current_value_at_1000V = round(df[df['voltage'] == -1000]['current'].mean(), 2)
            limit_data['current_value_at_600V'] = current_value_at_600V
            limit_data['current_value_at_800V']   = current_value_at_800V
            limit_data['current_value_at_1000V']  = current_value_at_1000V
            limit_data['current_800/600'] = round(current_value_at_800V / current_value_at_600V, 1)
            limit_colors['current_800/600'] = green_color if limit_data['current_800/600'] < 2.5 else red_color
            limit_data['current_1000/800'] = round(current_value_at_1000V / current_value_at_800V, 1)
            limit_colors['current_1000/800'] = green_color if limit_data['current_1000/800'] < 2.5 else red_color

            limit_colors['current_value_at_600V'] = green_color if current_value_at_600V <= maximum_current_at_600V else red_color
        except:
            pass
        return {
            'id': self.id,
            'iv_file_sensor': self.iv_file_sensor,
            'iv_file_module': self.iv_file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'datapoints_number': self.datapoints_number,
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'maximum_voltage': '<p style="background-color:' + limit_colors['maximum_voltage'] + '">' + str(limit_data['maximum_voltage']) + '</p>',
            'current_value_at_600V': '<p style="background-color:' + limit_colors['current_value_at_600V'] + '">' + str(limit_data['current_value_at_600V']) + '</p>',
            'current_800/600': '<p style="background-color:' + limit_colors['current_800/600'] + '">' + str(limit_data['current_800/600']) + '</p>',
            'current_1000/800': '<p style="background-color:' + limit_colors['current_1000/800'] + '">' + str(limit_data['current_1000/800']) + '</p>',
            'link': '<a href=/hv/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }


class SurfaceInspection(db.Model):
    __tablename__ = 'Surface_Inspection'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Surface_Inspection_id_seq\"'::regclass)"))
    sensor = Column(ForeignKey('Sensor_Overview.id'))
    operator = Column(ForeignKey('General_Operators.id'))
    station = Column(ForeignKey('General_Station.id'))
    timestamp = Column(DateTime)
    manual_grade = Column(ForeignKey('General_Grade.id'))
    air_temperature = Column(Numeric(5, 2))
    air_humidity = Column(Numeric(5, 2))
    comment = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    update_ts = Column(DateTime)
    file_sensor = Column(Text)
    file_module = Column(Text)
    file_operator = Column(Text)
    assembly_step = Column(Text)
    bad_measurement = Column(Boolean, comment='if true, measurement is not shown anywhere')
    module = Column(ForeignKey('Module_Overview.id'))

    General_Grade = relationship('GeneralGrade')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    General_Station = relationship('GeneralStation')
    def to_dict(self):
        return {
            'id': self.id,
            'file_sensor': self.file_sensor,
            'file_module': self.file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/si/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
    def to_dict_all(self):
        return {
            'id': self.id,
            'file_sensor': self.file_sensor,
            # 'file_module': self.file_module,
            # 'automatic_grade': '<p style="background-color:' + self.General_Grade.color + '">' + self.General_Grade.value + '</p>',
            'timestamp': self.timestamp,
            'assembly_step': self.assembly_step,
            'comment': self.comment,
            'link': '<a href=/si/id_' + str(self.id) + ' class="btn btn-primary active" aria-current="page">Link</a>',
        }
class SurfaceInspectionImage(db.Model):
    __tablename__ = 'Surface_Inspection_Images'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Surface_Inspection_Images_id_seq\"'::regclass)"))
    SI_measurement_id = Column(ForeignKey('Surface_Inspection_Images.id'))
    timestamp = Column(Numeric(12, 1))
    image_name = Column(Text)
    position_x = Column(Float(53))
    position_y = Column(Float(53))
    position_z = Column(Float(53))
    rotation = Column(Float(53))
    manual_grade = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    SI_measurement = relationship('SurfaceInspectionImage', remote_side=[id])

class ToolsConsumables(db.Model):
    __tablename__ = 'Tools_Consumables'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_Consumables_id_seq\"'::regclass)"))
    name = Column(Text)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)
    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'comment': self.comment,
        }

class ToolsConsumablesLog(db.Model):
    __tablename__ = 'Tools_Consumables_Log'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_Consumables_Log_id_seq\"'::regclass)"))
    consumable_id = Column(ForeignKey('Tools_Consumables.id'))
    comment = Column(Integer)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)
    name = Column(Text)

    consumable = relationship('ToolsConsumables', primaryjoin='ToolsConsumablesLog.consumable_id == ToolsConsumables.id')

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'category': self.consumable.name,
            'comment': self.comment,
        }
class ToolsJigsBaremodule(db.Model):
    __tablename__ = 'Tools_jigs_baremodule'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_jigs_baremodule_id_seq\"'::regclass)"))
    name = Column(Text)
    number = Column(Integer)
    barcode = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    def to_dict(self):
        import barcode
        data = self.barcode
        EAN = barcode.get_barcode_class('Gs1_128')
        barcode_image = EAN(data)
        barcode_image = barcode_image.render()
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'barcode': barcode_image.decode('utf-8'),
            'comment': self.comment,
        }

class ToolsJigsBonding(db.Model):
    __tablename__ = 'Tools_jigs_bonding'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_jigs_Bonding_id_seq\"'::regclass)"))
    name = Column(Text)
    number = Column(Integer)
    barcode = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    def to_dict(self):
        import barcode
        data = self.barcode
        EAN = barcode.get_barcode_class('Gs1_128')
        barcode_image = EAN(data)
        barcode_image = barcode_image.render()
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'barcode': barcode_image.decode('utf-8'),
            'comment': self.comment,
        }

class ToolsJigsHVpigtaile(db.Model):
    __tablename__ = 'Tools_jigs_HVpigtaile'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_jigs_HVpigtaile_id_seq\"'::regclass)"))
    name = Column(Text)
    number = Column(Integer)
    barcode = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    def to_dict(self):
        data = self.barcode
        barcode_image = EAN(data)
        barcode_image = barcode_image.render()
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'barcode': barcode_image.decode('utf-8'),
            'comment': self.comment,
        }

class ToolsJigshybridegluing(db.Model):
    __tablename__ = 'Tools_jigs_hybridegluing'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_jigs_hybridegluing_id_seq\"'::regclass)"))
    name = Column(Text)
    number = Column(Integer)
    barcode = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    def to_dict(self):
        import barcode
        data = self.barcode
        EAN = barcode.get_barcode_class('Gs1_128')
        barcode_image = EAN(data)
        barcode_image = barcode_image.render()
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'barcode': barcode_image.decode('utf-8'),
            'comment': self.comment,
        }

class ToolsJigskaptongluing(db.Model):
    __tablename__ = 'Tools_jigs_kaptongluing'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Tools_jigs_kaptongluing_id_seq\"'::regclass)"))
    name = Column(Text)
    number = Column(Integer)
    barcode = Column(Integer)
    comment = Column(Text)
    create_ts = Column(DateTime)
    update_ts = Column(DateTime)

    def to_dict(self):
        import barcode
        data = self.barcode
        EAN = barcode.get_barcode_class('Gs1_128')
        barcode_image = EAN(data)
        barcode_image = barcode_image.render()
        return {
            'id': self.id,
            'name': self.name,
            'number': self.number,
            'barcode': barcode_image.decode('utf-8'),
            'comment': self.comment,
        }





class WorkflowModPolyimide(db.Model):
    __tablename__ = 'Workflow_mod_polyimide'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_polyimide_id_seq\"'::regclass)"))
    sensor_name = Column(ForeignKey('Sensor_Overview.sensor_name'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    kapton_gluing_jig = Column(ForeignKey('Tools_jigs_kaptongluing.barcode'))
    measurement_metrology_sensor_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)

    Tools_jigs_kaptongluing = relationship('ToolsJigskaptongluing')
    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    def to_dict(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name + '<a href=/modules/sensor_'+str(self.sensor_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'jig': self.kapton_gluing_jig,
            'comment': self.comment,
            'timestamp': self.timestamp,
            # 'edit': '<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal_'+str(self.id)+'"><span class="fa fa-edit"></span> Edit</button>' +
            #         '<div class="modal fade" id="exampleModal_'+str(self.id)+'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
            #              '<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Edit for ' + str(self.id) +
            #         '</div></div></div></div>',
            'edit': '<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal_'+str(self.id)+'"><span class="fa fa-edit"></span> Edit</button>'
                    '<div class="modal fade" id="exampleModal_'+str(self.id)+'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Edit for ' + str(self.id) + '</h5> <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div>'+
                    '<div class="modal-body"><form class="user" action="" method="post"><div class="table-responsive"><table class="table table-bordered table-striped table-hover"><tbody>' +
                    '<tr><th scope="row">Sensor</th><td><input type="text" id="sensor" name="sensor" value="'+str(self.sensor_name)+'"><br></td></tr>'+
                    '<tr><th scope="row">Operator</th><td>  <input type="text" id="operator" name="operator" value="'+str(self.operator)+'"><br></td></tr>'+
                    '<tr><th scope="row">Grade</th><td><input type="text" id="grade" name="grade" value="'+str(self.grade)+'"><br></td></tr>'+
                    '<tr><th scope="row">Jig</th><td><input type="text" id="jig" name="jig" value="'+str(self.kapton_gluing_jig)+'"><br></td></tr>'+
                    '<tr><th scope="row">Timestamp</th><td><input type="text" id="timestamp" name="timestamp" value="'+str(self.timestamp)+'"><br></td></tr>'+
                    '<tr><th scope="row">Comment</th><td><input type="text" id="comment" name="comment" value="' + str(self.comment) + '"><br></td></tr>' +
                    '<tr><th scope="row">Sensor Metrology</th><td><input type="checkbox" id="measurement_metrology_sensor_done" name="measurement_metrology_sensor_done" value="'+str(self.measurement_metrology_sensor_done)+'"><br></td></tr>'+
                    '<tr><th scope="row">IV-Sensor</th><td><input type="checkbox" id="measurement_iv_done" name="measurement_iv_done" value="'+str(self.measurement_iv_done)+'"><br></td></tr>'+
                    '</tbody></table></div>'+
                    '<div class="modal-footer"><button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'+
                    '<div class="form-group">  <input type="submit" value="Save"></div></div></form>'+
                    '</div></div></div></div>'

        }


class WorkflowModTailBonded(db.Model):
    __tablename__ = 'Workflow_mod_tailBonded'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_tailBonded_id_seq\"'::regclass)"))
    sensor_name = Column(ForeignKey('Sensor_Overview.sensor_name'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    hv_gluing_jig = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)
    measurement_pulltest_done = Column(Boolean)

    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    def to_dict(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name + '<a href=/modules/sensor_'+str(self.sensor_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'jig': self.hv_gluing_jig,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModTailEncapsulated(db.Model):
    __tablename__ = 'Workflow_mod_tailEncapsulated'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_tailEncapsulated_id_seq\"'::regclass)"))
    sensor_name = Column(ForeignKey('Sensor_Overview.sensor_name'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    hv_gluing_jig = Column(Text)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)

    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')

    def to_dict(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name + '<a href=/modules/sensor_'+str(self.sensor_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'jig': self.hv_gluing_jig,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModTail(db.Model):
    __tablename__ = 'Workflow_mod_tail'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_tail_id_seq\"'::regclass)"))
    sensor_name = Column(Text)
    operator = Column(Text)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    hv_gluing_jig = Column(Text)
    create_ts = Column(DateTime)
    measurement_iv_done = Column(Boolean)
    measurement_metrology_sensor_done = Column(Boolean)
    def to_dict(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name + '<a href=/modules/sensor_'+str(self.sensor_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'jig': self.hv_gluing_jig,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBareSensor(db.Model):
    __tablename__ = 'Workflow_mod_bareSensor'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_bareSensor_id_seq\"'::regclass)"))
    sensor_name = Column(ForeignKey('Sensor_Overview.sensor_name'))
    operator = Column(ForeignKey('General_Operators.identifier'))
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)
    measurement_metrology_sensor_done = Column(Boolean)

    General_Operators = relationship('GeneralOperators')
    Sensor_Overview = relationship('SensorOverview')
    def to_dict(self):
        return {
            'id': self.id,
            'sensor_name': self.sensor_name + '<a href=/modules/sensor_'+str(self.sensor_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }





class WorkflowModBareFEH(db.Model):
    __tablename__ = 'Workflow_mod_bareFEH'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"mod_bareFEH_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)
    left_feh = Column(Text)
    right_feh = Column(Text)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        left_feh_text = '' if self.left_feh is None else str(self.left_feh) + '&nbsp <a href=/modules/sensor_' + str(self.left_feh) + ' aria-current="page"><i class="fas fa-link"></i></a>'
        right_feh_text = '' if self.right_feh is None else str(self.right_feh) + '&nbsp <a href=/modules/sensor_'+str(self.right_feh)+' aria-current="page"><i class="fas fa-link"></i></a>'

        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'left_feh': left_feh_text,
            'right_feh': right_feh_text,
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBareModule(db.Model):
    __tablename__ = 'Workflow_mod_bareModule'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_bareModule_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'))
    operator = Column(ForeignKey('General_Operators.identifier'))
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)
    top_sensor = Column(ForeignKey('Sensor_Overview.sensor_name'))
    bottom_sensor = Column(ForeignKey('Sensor_Overview.sensor_name'))

    Sensor_Overview = relationship('SensorOverview', primaryjoin='WorkflowModBareModule.bottom_sensor == SensorOverview.sensor_name')
    Sensor_Overview1 = relationship('SensorOverview', primaryjoin='WorkflowModBareModule.top_sensor == SensorOverview.sensor_name')
    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        top_sensor_text = '' if self.top_sensor is None else str(self.top_sensor) + '&nbsp <a href=/modules/sensor_' + str(self.top_sensor) + ' aria-current="page"><i class="fas fa-link"></i></a>'
        bottom_sensor_text = '' if self.top_sensor is None else str(self.bottom_sensor) + '&nbsp <a href=/modules/sensor_'+str(self.bottom_sensor)+' aria-current="page"><i class="fas fa-link"></i></a>'

        return {
            'id': self.id,
            'module_name': str(self.module_name) + '&nbsp <a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'top_sensor': top_sensor_text,
            'bottom_sensor': bottom_sensor_text,
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBareSEH(db.Model):
    __tablename__ = 'Workflow_mod_bareSEH'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"mod_bareSEH_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_iv_done = Column(Boolean)
    seh         = Column(Text)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        seh_text = '' if self.seh is None else str(self.seh) + '&nbsp <a href=/modules/seh_'+str(self.seh)+' aria-current="page"><i class="fas fa-link"></i></a>'

        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'seh': seh_text,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBasePlate(db.Model):
    __tablename__ = 'Workflow_mod_basePlate'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_db.ModelPlate_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime)
    measurement_iv_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBonded(db.Model):
    __tablename__ = 'Workflow_mod_bonded'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_bonded_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_functionaltest_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)
    measurement_pulltest_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModBottomEncapsulated(db.Model):
    __tablename__ = 'Workflow_mod_bottomEncapsulated'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_bottomEncapsulated_data_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_functionaltest_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModFinal(db.Model):
    __tablename__ = 'Workflow_mod_final'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"Workflow_mod_final_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'))
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_functionaltest_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModFinalCold(db.Model):
    __tablename__ = 'Workflow_mod_finalCold'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"mod_finalCold_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'))
    operator = Column(ForeignKey('General_Operators.identifier'))
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_functionaltest_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowModTopEncapsulated(db.Model):
    __tablename__ = 'Workflow_mod_topEncapsulated'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"mod_topEncapsulated_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    grade = Column(Text)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))
    measurement_functionaltest_done = Column(Boolean)
    measurement_iv_done = Column(Boolean)

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')
    def to_dict(self):
        return {
            'id': self.id,
            'module_name': self.module_name + '<a href=/modules/module_'+str(self.module_name)+' aria-current="page"><i class="fas fa-link"></i></a>',
            'operator': self.operator,
            'grade': self.grade,
            'comment': self.comment,
            'timestamp': self.timestamp,
        }

class WorkflowAachenDefined(db.Model):
    __tablename__ = 'Workflow_aachen_defined'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"aachen_defined_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')

class WorkflowAachenFinished(db.Model):
    __tablename__ = 'Workflow_aachen_finished'

    id = Column(Integer, primary_key=True, server_default=text("nextval('\"aachen_finished_id_seq\"'::regclass)"))
    module_name = Column(ForeignKey('Module_Overview.cern_module_id'), nullable=False)
    operator = Column(ForeignKey('General_Operators.identifier'), nullable=False)
    comment = Column(Text)
    timestamp = Column(DateTime)
    create_ts = Column(DateTime, server_default=text("now()"))

    Module_Overview = relationship('ModuleOverview')
    General_Operators = relationship('GeneralOperators')

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(UserMixin, db.Model):
    id              = db.Column(db.Integer, primary_key=True)
    username        = db.Column(db.String(30), unique=True, nullable=False)
    name            = db.Column(db.String(30), unique=False, nullable=True)
    surname         = db.Column(db.String(30), unique=False, nullable=True)
    email           = db.Column(db.String(130), unique=True, nullable=False)
    phone           = db.Column(db.String(130), unique=False, nullable=True)
    club            = db.Column(db.String(30), unique=False, nullable=True)
    function        = db.Column(db.String(30), unique=False, nullable=True)
    password        = db.Column(db.String(260), unique=False, nullable=False)
    last_seen       = db.Column(db.String(30), nullable=True, default=datetime.utcnow)

    def __repr(self):
        return f"User'{self.username}', '{self.email}')"
    def set_password(self, password):
        self.password = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password, password)