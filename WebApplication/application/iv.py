import os
import json
import jinja2
import numpy as np
import paramiko
import psycopg2
import pandas as pd
import plotly
import plotly.express as px
from plotly.subplots import make_subplots
from plotly.graph_objs.scatter.marker import Line
import plotly.graph_objects as go
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG
from sqlalchemy import create_engine

from application                    import app, db, bcrypt, mail
from application.models             import *
from application                    import engine

flask_iv = Blueprint('flask_iv', __name__, template_folder="templates/iv", url_prefix="/iv")

green_color = "#A5D6A7"   # if value is good
red_color   = "#F48FB1"     # if value is bad
grey_color  = "grey"    # if unclear or not checked
temperature_min     = 19
temperature_max     = 24
humidity_min        = 40
humidity_max        = 60

maximum_current_at_600V     = 7.25*10**3 # uA into nA
breakdown_voltage_B = 800 # over 800 Volt
breakdown_voltage_A = 1000 # over 1000 Volt
current_condition_B_factor = 2.5 # 800Volt vs 600Volt
current_condition_A_factor = 2.5 # 1000 Volt vs 800Volt

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])
engine = create_engine('postgresql://'+db_username_DCA+':'+db_password_DCA+'@'+db_host_DCA + ':'+db_port_DCA+'/'+db_database_DCA)



@flask_iv.route('/get_data', methods=['GET', 'POST'])
# trigger external script to update and pull new data from the Metrology PC
def iv_get_data():
    if request.method == 'POST':
        flash('Submission successful GET DATA script is performed!', 'success')
        os.system("nohup python application/scripts/iv_measurement.py &&")
    return redirect(url_for("flask_iv.iv_overview"))

@flask_iv.route('/overview')
def iv_overview():
    return render_template("iv_overview.html")

@flask_iv.route('/overview/data')
def iv_overview_data():
    db_table_class = IVMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter(IVMeasurement.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_grading() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data






    mean_data = {}
    mean_data['mean_tearoffforce']              = round(df_data['tearoffforce'].mean(), 3)
    mean_data['std_tearoffforce']               = round(df_data['tearoffforce'].std(), 3)
    mean_data['mean_corrected_tearoffforce']    = round(df_data['corrected_tearoffforce'].mean(), 3)
    mean_data['std_corrected_tearoffforce']     = round(df_data['corrected_tearoffforce'].std(), 3)
    total_number                                = len(df_data)
    mean_data['Grade1'] = round(len(df_data[df_data['grade']=='1'])/total_number*100, 1)
    mean_data['Grade2'] = round(len(df_data[df_data['grade']=='2'])/total_number*100, 1)
    mean_data['Grade3'] = round(len(df_data[df_data['grade']=='3'])/total_number*100, 1)
    mean_data['Grade4'] = round(len(df_data[df_data['grade']=='4'])/total_number*100, 1)
    mean_data['Grade5'] = round(len(df_data[df_data['grade']=='5'])/total_number*100, 1)
    print(mean_data)

    # make plots
    # show histogram:
    df = pd.read_sql_query(
        sql=db.select([PulltestResult.tearoffforce,
                       PulltestResult.corrected_tearoffforce,
                       PulltestResult.grade,
                        ]),
        con=engine
    )

    print("Type:", type(df))
    print("Type:", df)
    fig = px.histogram(df, x="tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="corrected_tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_corrected_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="grade", nbins=50)
    # fig.add_vline(x=PulltestResults_data.grade, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

@flask_iv.route('/sensor')
def iv_sensor():
    return render_template("iv_sensor.html")

@flask_iv.route('/sensor/data')
def iv_sensor_data():
    db_table_class = IVMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter(IVMeasurement.sensor.isnot(None)).filter(IVMeasurement.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_grading() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data






    mean_data = {}
    mean_data['mean_tearoffforce']              = round(df_data['tearoffforce'].mean(), 3)
    mean_data['std_tearoffforce']               = round(df_data['tearoffforce'].std(), 3)
    mean_data['mean_corrected_tearoffforce']    = round(df_data['corrected_tearoffforce'].mean(), 3)
    mean_data['std_corrected_tearoffforce']     = round(df_data['corrected_tearoffforce'].std(), 3)
    total_number                                = len(df_data)
    mean_data['Grade1'] = round(len(df_data[df_data['grade']=='1'])/total_number*100, 1)
    mean_data['Grade2'] = round(len(df_data[df_data['grade']=='2'])/total_number*100, 1)
    mean_data['Grade3'] = round(len(df_data[df_data['grade']=='3'])/total_number*100, 1)
    mean_data['Grade4'] = round(len(df_data[df_data['grade']=='4'])/total_number*100, 1)
    mean_data['Grade5'] = round(len(df_data[df_data['grade']=='5'])/total_number*100, 1)
    print(mean_data)

    # make plots
    # show histogram:
    df = pd.read_sql_query(
        sql=db.select([PulltestResult.tearoffforce,
                       PulltestResult.corrected_tearoffforce,
                       PulltestResult.grade,
                        ]),
        con=engine
    )

    print("Type:", type(df))
    print("Type:", df)
    fig = px.histogram(df, x="tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="corrected_tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_corrected_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="grade", nbins=50)
    # fig.add_vline(x=PulltestResults_data.grade, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

@flask_iv.route('/module')
def iv_module():
    return render_template("iv_module.html")

@flask_iv.route('/module/data')
def iv_module_data():
    db_table_class = IVMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter(IVMeasurement.module.isnot(None)).filter(IVMeasurement.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_grading() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data






    mean_data = {}
    mean_data['mean_tearoffforce']              = round(df_data['tearoffforce'].mean(), 3)
    mean_data['std_tearoffforce']               = round(df_data['tearoffforce'].std(), 3)
    mean_data['mean_corrected_tearoffforce']    = round(df_data['corrected_tearoffforce'].mean(), 3)
    mean_data['std_corrected_tearoffforce']     = round(df_data['corrected_tearoffforce'].std(), 3)
    total_number                                = len(df_data)
    mean_data['Grade1'] = round(len(df_data[df_data['grade']=='1'])/total_number*100, 1)
    mean_data['Grade2'] = round(len(df_data[df_data['grade']=='2'])/total_number*100, 1)
    mean_data['Grade3'] = round(len(df_data[df_data['grade']=='3'])/total_number*100, 1)
    mean_data['Grade4'] = round(len(df_data[df_data['grade']=='4'])/total_number*100, 1)
    mean_data['Grade5'] = round(len(df_data[df_data['grade']=='5'])/total_number*100, 1)
    print(mean_data)

    # make plots
    # show histogram:
    df = pd.read_sql_query(
        sql=db.select([PulltestResult.tearoffforce,
                       PulltestResult.corrected_tearoffforce,
                       PulltestResult.grade,
                        ]),
        con=engine
    )

    print("Type:", type(df))
    print("Type:", df)
    fig = px.histogram(df, x="tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="corrected_tearoffforce", nbins=50)
    fig.add_vline(x=mean_data['mean_corrected_tearoffforce'], line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_corrected_tearoffforce = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    fig = px.histogram(df, x="grade", nbins=50)
    # fig.add_vline(x=PulltestResults_data.grade, line_width=3, line_dash="solid", line_color="red")
    fig.update_layout(bargap=0.1)
    fig_pulltest_db_grade = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

@flask_iv.route('/plot')
def iv_plot():
    return render_template("iv_plot_sensors.html")
@flask_iv.route('/plot/data')
def iv_plot_data():
    db_table_class = Sensor_Overview
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "sensor_name")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
@flask_iv.route('/plot/sensor_<sensor_name>')
def iv_plot_sensor(sensor_name):
    plotly_layout = json.dumps({'margin': dict(l=0, r=0, t=50, b=0)})
    plotly_config = json.dumps({'displaylogo': False,
                                'toImageButtonOptions': {
                                    'format': 'png',  # one of png, svg, jpeg, webp
                                    'filename': 'custom_image',
                                    'height': 1000,
                                    'width': 1400,
                                    'scale': 2  # Multiply title/legend/axis/canvas sizes by this factor
                                },
                                'scrollZoom': True,
                                'displayModeBar': True,
                                'modeBarButtonsToAdd': ['drawline',
                                                        'drawopenpath',
                                                        'drawclosedpath',
                                                        'drawcircle',
                                                        'drawrect',
                                                        'eraseshape'
                                                        ]
                                })

    # plot all data as reference
    df_measurements = pd.read_sql_query('''SELECT "id", iv_file_sensor, iv_file_operator, assembly_step, timestamp  FROM "IV_Measurement" ''' + ";", con=engine)
    df_all = pd.read_sql_query('''SELECT "IV_measurement_id", voltage, current, ramp_direction  FROM "IV_Measurement_Results" ''' + ";", con=engine)
    df_measuremend_ids = df_all["IV_measurement_id"].unique()

    df_measurements['timestamp'] = pd.to_datetime(df_measurements['timestamp']).dt.date

    fig = go.Figure()
    for i in df_measuremend_ids:
        fig.add_trace(go.Line(
            x=df_all[df_all['IV_measurement_id'] == i]['voltage'],
            y=df_all[df_all['IV_measurement_id'] == i]['current'],
            name=str(df_measurements[df_measurements['id'] == i]['iv_file_sensor'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['iv_file_operator'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['timestamp'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['assembly_step'].unique()[0]), mode='lines+markers'))



    # CERN DATABASE:
    import sys
    from io import StringIO
    sys.path.append('application/scripts')
    from rhapi import RhApi
    url = "https://cmsdca.cern.ch/trk_rhapi"
    rh = RhApi(url, debug=False, sso="login")

    for sensor_name in df_measurements['iv_file_sensor'].unique():
        df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(sensor_name) + "'")), sep=",")
        run_numbers = df_cern['RUN_NUMBER'].unique()
        print(run_numbers)
        # df_cern['INSERTION_TIME'] = pd.to_datetime(df_cern['INSERTION_TIME']).dt.date

        for i in run_numbers:
            print(i)
            df_cern_details = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.datasets p where p.RUN_ID ='" + str(i) + "'")), sep=",")
            try:
                db_user = str(df_cern_details[df_cern_details['RUN_ID'] == i]['INSERTION_USER'].unique()[0])
                db_time = str(df_cern_details[df_cern_details['RUN_ID'] == i]['INSERTION_TIME'].unique()[0])
                db_number = str(df_cern_details[df_cern_details['RUN_ID'] == i]['RUN_TYPE_NUMBER'].unique()[0])
            except:
                db_user = str()
                db_time = str()
                db_number = str()

            print(df_cern_details)
            fig.add_trace(go.Line(
                x=df_cern[df_cern['RUN_NUMBER'] == i]['VOLTS'],
                y=df_cern[df_cern['RUN_NUMBER'] == i]['CURRNT_AMP'],
                name="CMS " + " - " + str(sensor_name) + " - " + db_user + " - " + db_time + " - " + db_number, mode='lines+markers'))


    fig.update_layout(
        title_text="IV-Curve",
        autosize=True,
        height=1000,
        paper_bgcolor="LightSteelBlue",
    )

    fig.update_layout(dict(updatemenus=[
        dict(
            type="buttons",
            direction="left",
            buttons=list([
                dict(
                    args=["visible", "legendonly"],
                    label="Deselect All",
                    method="restyle"
                ),
                dict(
                    args=["visible", True],
                    label="Select All",
                    method="restyle"
                )
            ]),
            pad={"r": 10, "t": 10},
            showactive=False,
            x=1,
            xanchor="left",
            y=0,
            yanchor="top"
        ),
        dict(active=1,
             buttons=list([
                 dict(label='Log Scale',
                      method='update',
                      args=[{'visible': [True, True]},
                            {'title': 'Log scale',
                             'yaxis': {'type': 'log'}}]),
                 dict(label='Linear Scale',
                      method='update',
                      args=[{'visible': [True, False]},
                            {'title': 'Linear scale',
                             'yaxis': {'type': 'linear'}}])
             ]),
         ),
    ]
    ))
    fig.update_xaxes(title_text="Voltage in Volt")
    fig.update_yaxes(title_text="Current in pA")
    fig.update_xaxes(autorange="reversed")
    fig.update_yaxes(autorange="reversed")

    fig_all = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    fig.write_html("file.html")

    return render_template("iv_plot.html", graphJSON_list=fig_all, plotly_layout=plotly_layout, plotly_config=plotly_config)



@flask_iv.route('/id_<int:measurement_number>', methods=["GET", "POST"])
def iv_detail(measurement_number):
    IVMeasurement_data = IVMeasurement.query.filter_by(id=measurement_number).first()
    limit_data = {}
    limit_colors = {}
    try:
        limit_colors['temp'] = green_color if temperature_min <= IVMeasurement_data.air_temperature < temperature_max else red_color
        limit_colors['hum'] = green_color if humidity_min <= IVMeasurement_data.air_humidity < humidity_max else red_color
    except:
        limit_colors['temp'] = "#ffffff0"
        limit_colors['hum'] = "#ffffff0"


    all_ids_raw = pd.read_sql_query('''SELECT id, timestamp  FROM "IV_Measurement" ORDER BY timestamp; ''', con=engine)
    all_ids = np.flip(all_ids_raw['id'].to_list())
    all_sensors = np.flip(all_ids_raw['timestamp'].to_list())

    # query data from general dables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.aachen_module_id)
    module_list = list(zip(module_value, module_name))


    df = pd.read_sql_query('''SELECT voltage, current, humidity, temperature, ramp_direction  FROM "IV_Measurement_Results" WHERE "IV_measurement_id" = ''' + str(measurement_number) + ";", con=engine)
    maximum_voltage = df['voltage'].min()
    limit_data['maximum_voltage'] = maximum_voltage
    limit_colors['maximum_voltage'] = red_color
    if maximum_voltage <= -1000:
        limit_colors['maximum_voltage'] = green_color
    if -1000 < maximum_voltage <= -800:
        limit_colors['maximum_voltage'] = grey_color

    try:
        current_value_at_600V = round(df[df['voltage'] == -600]['current'].mean(), 2)
        current_value_at_800V = round(df[df['voltage'] == -800]['current'].mean(), 2)
        current_value_at_1000V = round(df[df['voltage'] == -1000]['current'].mean(), 2)
        limit_data['current_value_at_600V']     = current_value_at_600V
        # limit_data['current_value_at_800V']   = current_value_at_800V
        # limit_data['current_value_at_1000V']  = current_value_at_1000V
        limit_data['current_800/600']           = round(current_value_at_800V /current_value_at_600V ,1)
        limit_colors['current_800/600'] = green_color if limit_data['current_800/600'] < 2.5 else red_color
        limit_data['current_1000/800']          = round(current_value_at_1000V / current_value_at_800V ,1)
        limit_colors['current_1000/800'] = green_color if limit_data['current_1000/800'] < 2.5 else red_color

        limit_colors['current_value_at_600V'] = green_color if current_value_at_600V <= maximum_current_at_600V else red_color
    except:
        pass

    plotly_layout = json.dumps({'margin': dict(l=0, r=0, t=50, b=0)})
    plotly_config = json.dumps({'displaylogo': False,
                                'toImageButtonOptions': {
                                    'format': 'png',  # one of png, svg, jpeg, webp
                                    'filename': 'custom_image',
                                    'height': 1000,
                                    'width': 1400,
                                    'scale': 2  # Multiply title/legend/axis/canvas sizes by this factor
                                },
                                'scrollZoom': True,
                                'displayModeBar': True,
                                'modeBarButtonsToAdd': ['drawline',
                                                        'drawopenpath',
                                                        'drawclosedpath',
                                                        'drawcircle',
                                                        'drawrect',
                                                        'eraseshape'
                                                        ]
                                })

    fig = (make_subplots(rows=2,
                      cols=1,
                      row_heights=[1, 0.7],
                      subplot_titles=('IV-Curve Measurement', 'Environemental Conditions.'),
                      specs=[[{"secondary_y": False}], [{"secondary_y": True}]],
                      print_grid=True,
                      vertical_spacing=0.1,
                        )
            )
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "u"]['voltage'],
        y=df[df['ramp_direction'] == "u"]['current'],
        name="IV-Curve up",
        mode='lines+markers',
        line=dict(color='black', width=2),),
        row=1, col=1, secondary_y=False,)
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "d"]['voltage'],
        y=df[df['ramp_direction'] == "d"]['current'],
        name="IV-Curve down",
        mode='lines+markers',
        line=dict(color='red', width=2, dash='dot'),),
        row=1, col=1, secondary_y=False,)
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "u"]['voltage'],
        y=df[df['ramp_direction'] == "u"]['temperature'],
        name="Temperature up", showlegend=True, mode='lines+markers',
        line=dict(color='orange', width=2),),
        row=2, col=1, secondary_y=True,)
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "u"]['voltage'],
        y=df[df['ramp_direction'] == "u"]['humidity'],
        name="rel. Humidity up", showlegend = True, mode='lines+markers',
        line=dict(color='royalblue', width=1), ),
        row=2, col=1, secondary_y=False,)
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "d"]['voltage'],
        y=df[df['ramp_direction'] == "d"]['temperature'],
        name="Temperature down", showlegend=True, mode='lines+markers',
        line=dict(color='darkorange', width=2, dash='dot'),),
        row=2, col=1, secondary_y=True,)
    fig.add_trace(go.Line(
        x=df[df['ramp_direction'] == "d"]['voltage'],
        y=df[df['ramp_direction'] == "d"]['humidity'],
        name="rel. Humidity down", showlegend = True, mode='lines+markers',
        line=dict(color='darkblue', width=1, dash='dot'), ),
        row=2, col=1, secondary_y=False,)

    fig.add_vline(x=-600, line_width=3, line_dash="dash", line_color="green")
    fig.add_vline(x=-1000, line_width=3, line_dash="solid", line_color="black")
    fig.add_hrect(y0=-7500, y1=-20000, line_width=3, fillcolor="red", opacity=0.1)


    fig.update_layout(
        title_text="IV-Curve"
    )
    fig.update_layout(
        autosize=False,
        width=1200,
        height=1000,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
        paper_bgcolor="LightSteelBlue",
    )

    # iv data plot
    fig.update_xaxes(title_text="Voltage in Volt", row=1, col=1)
    fig.update_yaxes(title_text="Current in nA", secondary_y=False, row=1, col=1)

    # temp and hum plot
    fig.update_xaxes(title_text="Voltage in Volt", row=2, col=1)
    fig.update_yaxes(title_text="Temperature in °C", secondary_y=True, row=2, col=1)
    fig.update_yaxes(title_text="Humidity in %H", secondary_y=False, row=2, col=1)
    # all legend reversed
    fig.update_xaxes(autorange="reversed", row=1, col=1)
    fig.update_yaxes(autorange="reversed", row=1, col=1)
    fig.update_xaxes(autorange="reversed", row=2, col=1)

    # set axes range
    min_humidity = df['humidity'].min()
    max_humidity = df['humidity'].max()
    fig.update_yaxes(row=2, col=1, range=[min_humidity-3, max_humidity+3])
    min_temp = df['temperature'].min()
    max_temp = df['temperature'].max()
    fig.update_yaxes(secondary_y=True, row=2, col=1, range=[min_temp-1.5, max_temp+1.5])


    fig_plot = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    sensor_name = IVMeasurement_data.iv_file_sensor

    fig = go.Figure()
    fig.add_trace(go.Line(
        x=df['voltage'],
        y=df['current'],
        name="IV-Curve - " + str(round(df['temperature'].mean(), 2)) + "°C - " + str(round(df['humidity'].mean(), 2)) + "%H",
        mode='lines+markers',
        line=dict(color='black', width=2),))


    df_info = pd.read_sql_query('SELECT run_type, run_number, location, begin_date, description, mean_temp, mean_hum   FROM "CMS_IV_Measurement"' + "WHERE sensor = '" + str(sensor_name) + "';", con=engine)

    for i in range(len(df_info)):
        run_number = df_info.iloc[i]['run_number']
        run_type = df_info.iloc[i]['run_type']
        mean_temp = df_info.iloc[i]['mean_temp']
        mean_hum = df_info.iloc[i]['mean_hum']
        df_measurement = pd.read_sql_query('SELECT volts, currnt_amp  FROM "CMS_IV_Measurement_Results"' + "WHERE run_type = '" + str(run_type) + "' and run_number = '" + str(run_number) + "';", con=engine)

        fig.add_trace(go.Line(
            x=df_measurement['volts'],
            y=df_measurement['currnt_amp'],
            name=str(df_info.iloc[i]['begin_date']) + " - " + str(df_info.iloc[i]['location']) + " - " + str(df_info.iloc[i]['run_type']) + " - "  +   str(mean_temp) + "°C - " + str(mean_hum) + "%H" + " - " + str(df_info.iloc[i]['description']),
            mode='markers',
        ))



    fig.update_layout(
        title_text="IV-Curve CMS DB",
        autosize=True,
        height=1000,
        paper_bgcolor="LightSteelBlue",
    )

    fig.update_layout(dict(updatemenus=[
        dict(
            type="buttons",
            direction="left",
            buttons=list([
                dict(
                    args=["visible", "legendonly"],
                    label="Deselect All",
                    method="restyle"
                ),
                dict(
                    args=["visible", True],
                    label="Select All",
                    method="restyle"
                )
            ]),
            pad={"r": 10, "t": 10},
            showactive=False,
            x=1,
            xanchor="left",
            y=0,
            yanchor="top"
        ),
        dict(active=1,
             buttons=list([
                 dict(label='Log Scale',
                      method='update',
                      args=[{'visible': [True, True]},
                            {'title': 'Log scale',
                             'yaxis': {'type': 'log'}}]),
                 dict(label='Linear Scale',
                      method='update',
                      args=[{'visible': [True, False]},
                            {'title': 'Linear scale',
                             'yaxis': {'type': 'linear'}}])
             ]),
         ),
    ]
    ))
    fig.update_xaxes(title_text="Voltage in Volt")
    fig.update_yaxes(title_text="Current in nA")
    fig.update_xaxes(autorange="reversed")
    fig.update_yaxes(autorange="reversed")
    # fig.update_xaxes(range=[1.5, 4.5])
    # fig.update_yaxes(range=[3, 9])

    fig_cern = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)


    class IV_detailForm(FlaskForm):
        operator        = SelectField(u'Operator', default=IVMeasurement_data.operator, choices=operator_list, validators=[DataRequired()])
        station         = SelectField(u'Station', default=IVMeasurement_data.station, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'Grade', default=IVMeasurement_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        # status          = SelectField(u'Station', default=IVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=IVMeasurement_data.comment)
        submit          = SubmitField("Update")

    form = IV_detailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        print(form.operator.data, form.station.data, form.comment.data)
        IVMeasurement_data.operator           = form.operator.data
        IVMeasurement_data.station            = form.station.data
        # IVMeasurement_data.measurement_status = form.status.data
        IVMeasurement_data.comment            = form.comment.data
        IVMeasurement_data.manual_grade       = form.grade.data

        db.session.add(IVMeasurement_data)
        db.session.commit()
        return redirect(url_for("flask_iv.iv_detail"))


    return render_template("iv_detail.html", data=IVMeasurement_data, graphJSON_list=[fig_plot, fig_cern], limit_data=limit_data, limit_colors = limit_colors, form=form, plotly_layout=plotly_layout, plotly_config=plotly_config, all_ids=all_ids,all_sensors=all_sensors)

@flask_iv.route('/sensor_name_<sensor_name>/update_cmsdb', methods=["GET", "POST"])
def iv_detail_update_cmsdb(sensor_name):

    # CERN DATABASE:
    import sys
    from io import StringIO
    sys.path.append('application/scripts')
    from rhapi import RhApi
    url = "https://cmsdca.cern.ch/trk_rhapi"

    rh = RhApi(url, debug=False, sso="login")
    df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(sensor_name) + "'")), sep=",")
    df_cern_unique = pd.DataFrame()
    df_cern_unique['RUN_NUMBER'] = df_cern['RUN_NUMBER']
    df_cern_unique['RUN_TYPE'] = df_cern['RUN_TYPE']
    df_cern_unique = df_cern_unique.drop_duplicates()
    df_cern_unique = df_cern_unique.sort_values(by=['RUN_NUMBER'])

    # run_numbers = df_cern['RUN_NUMBER'].unique()
    # run_type = df_cern['RUN_NUMBER'].unique()



    for i in range(len(df_cern_unique)):
        sql_string = "select * from trker_cmsr.runs p where p.RUN_NUMBER = " + str(df_cern_unique['RUN_NUMBER'][i]) + " and p.RUN_TYPE = '" + str(df_cern_unique['RUN_TYPE'][i]) + "'"

        df_cern_runs = pd.read_csv(StringIO(rh.csv(sql_string)), sep=",")
        df_cern_runs['BEGIN_DATE'] = pd.to_datetime(df_cern_runs['BEGIN_DATE']).dt.date

        g = df_cern_unique['RUN_NUMBER'][i]
        h = df_cern_unique['RUN_TYPE'][i]

        mean_temp = round(df_cern['TEMP_DEGC'].loc[((df_cern['RUN_NUMBER'] == g) & (df_cern['RUN_TYPE'] == h))].mean(), 2)
        mean_hum = round(df_cern['RH_PRCNT'].loc[((df_cern['RUN_NUMBER'] == g) & (df_cern['RUN_TYPE'] == h))].mean(), 2)
        sensor_name = sensor_name

        for a in range(len(df_cern_runs)):
            # check if measurements are already inside DCA-DB
            conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
            cur = conn.cursor()
            cur.execute("""
            SELECT "id"
            FROM "CMS_IV_Measurement"
            WHERE sensor = (%s) and cern_db_id = (%s) and begin_date = (%s) and end_date = (%s) and location = (%s) and mean_temp = (%s) and mean_hum = (%s) and run_type = (%s) and run_number = (%s) and description = (%s) and name = (%s)
            """, (sensor_name, int(df_cern_runs.iloc[a]['ID']), str(df_cern_runs.iloc[a]['BEGIN_DATE']), str(df_cern_runs.iloc[a]['END_DATE']), str(df_cern_runs.iloc[a]['LOCATION']), mean_temp, mean_hum, str(df_cern_runs.iloc[a]['RUN_TYPE']), int(df_cern_runs.iloc[a]['RUN_NUMBER']), str(df_cern_runs.iloc[a]['DESCRIPTION']), str(df_cern_runs.iloc[a]['NAME'])))
            database_id = cur.fetchall()
            cur.close()
            if len(database_id) == 0:
                # insert the data
                db_data = (sensor_name, int(df_cern_runs.iloc[a]['ID']), str(df_cern_runs.iloc[a]['BEGIN_DATE']), str(df_cern_runs.iloc[a]['END_DATE']), str(df_cern_runs.iloc[a]['LOCATION']), mean_temp, mean_hum, str(df_cern_runs.iloc[a]['RUN_TYPE']), int(df_cern_runs.iloc[a]['RUN_NUMBER']), str(df_cern_runs.iloc[a]['DESCRIPTION']), str(df_cern_runs.iloc[a]['NAME']))
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute(
                    'INSERT into "CMS_IV_Measurement" (sensor, cern_db_id, begin_date, end_date, location, mean_temp, mean_hum, run_type, run_number, description, name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                    db_data)
                conn.commit()
                cur.close()
                conn.close()
            else:
                print('dataset is already inside database')


            for a in range(len(df_cern)):
                conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                cur = conn.cursor()
                cur.execute("""
                    SELECT "id"
                    FROM "CMS_IV_Measurement_Results"
                    WHERE sensor = (%s) and test_type = (%s) and run_name = (%s) and volts = (%s) and currnt_amp = (%s) and temp_degc = (%s) and rh_prcnt = (%s) and run_number = (%s) and run_type = (%s) and run_type_number = (%s)
                    """, (str(df_cern.iloc[a]['SENSOR']), str(df_cern.iloc[a]['TEST_TYPE']), str(df_cern.iloc[a]['RUN_NAME']), float(df_cern.iloc[a]['VOLTS']), float(df_cern.iloc[a]['CURRNT_AMP']),
                          float(df_cern.iloc[a]['TEMP_DEGC']), float(df_cern.iloc[a]['RH_PRCNT']), int(df_cern.iloc[a]['RUN_NUMBER']), str(df_cern.iloc[a]['RUN_TYPE']) , str(df_cern.iloc[a]['RUN_TYPE_NUMBER'])))
                database_id = cur.fetchall()
                print(database_id)
                cur.close()
                if len(database_id) == 0:
                    # insert the data
                    db_data = (str(df_cern.iloc[a]['SENSOR']), str(df_cern.iloc[a]['TEST_TYPE']), str(df_cern.iloc[a]['RUN_NAME']), float(df_cern.iloc[a]['VOLTS']), float(df_cern.iloc[a]['CURRNT_AMP']),
                          float(df_cern.iloc[a]['TEMP_DEGC']), float(df_cern.iloc[a]['RH_PRCNT']), int(df_cern.iloc[a]['RUN_NUMBER']), str(df_cern.iloc[a]['RUN_TYPE']) , str(df_cern.iloc[a]['RUN_TYPE_NUMBER']))
                    conn = psycopg2.connect(host=db_host_DCA, port=db_port_DCA, dbname=db_database_DCA, user=db_username_DCA, password=db_password_DCA)
                    cur = conn.cursor()
                    cur.execute(
                        'INSERT into "CMS_IV_Measurement_Results" (sensor, test_type, run_name, volts, currnt_amp, temp_degc, rh_prcnt, run_number, run_type, run_type_number) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        db_data)
                    conn.commit()
                    cur.close()
                    conn.close()
                    print('added dataset')
                else:
                    print('dataset is already inside database')





    return "data is successfully updated"



@flask_iv.route('/id_<int:measurement_number>/data')
def iv_detail_data(measurement_number):
    db_table_class = IVMeasurementResult
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(IV_measurement_id=measurement_number)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.asc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
