import os
import json
import jinja2
import numpy as np
import paramiko
import pandas as pd
import datetime
import plotly
import plotly.express as px
from plotly.subplots import make_subplots
from plotly.graph_objs.scatter.marker import Line
import plotly.graph_objects as go
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG

from application                    import app, db, bcrypt, mail
from application.models             import *
from application                    import engine

flask_si = Blueprint('flask_si', __name__, template_folder="templates/surface_inspection", url_prefix="/si")

green_color = "#A5D6A7"   # if value is good
red_color   = "#F48FB1"     # if value is bad
grey_color  = "grey"    # if unclear or not checked
temperature_min     = 19
temperature_max     = 24
humidity_min        = 40
humidity_max        = 60


@flask_si.route('/get_data', methods=['POST'])
# trigger external script to update and pull new data from the Metrology PC
def si_get_data():
    if request.method == 'POST':
        flash('Submission successful GET DATA script is performed!', 'success')
        os.system("nohup python application/scripts/surface_inspection.py &&")
    return redirect(url_for("flask_si.si_overview"))

@flask_si.route('/overview')
def si_overview():
    return render_template("si_overview.html")

@flask_si.route('/overview/data')
def si_overview_data():
    db_table_class = SurfaceInspection
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter(SurfaceInspection.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_si.route('/sensor')
def si_sensor():
    return render_template("si_sensor.html")

@flask_si.route('/sensor/data')
def si_sensor_data():
    db_table_class = SurfaceInspection
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter(SurfaceInspection.sensor.isnot(None)).filter(SurfaceInspection.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_si.route('/module')
def si_module():
    return render_template("si_module.html")

@flask_si.route('/module/data')
def si_module_data():
    db_table_class = SurfaceInspection
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(file_sensor=None).filter(SurfaceInspection.bad_measurement.isnot(True))
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data





@flask_si.route('/plot')
def si_plot():
    plotly_layout = json.dumps({'margin': dict(l=0, r=0, t=50, b=0)})
    plotly_config = json.dumps({'displaylogo': False,
                                'toImageButtonOptions': {
                                    'format': 'png',  # one of png, svg, jpeg, webp
                                    'filename': 'custom_image',
                                    'height': 1000,
                                    'width': 1400,
                                    'scale': 2  # Multiply title/legend/axis/canvas sizes by this factor
                                },
                                'scrollZoom': True,
                                'displayModeBar': True,
                                'modeBarButtonsToAdd': ['drawline',
                                                        'drawopenpath',
                                                        'drawclosedpath',
                                                        'drawcircle',
                                                        'drawrect',
                                                        'eraseshape'
                                                        ]
                                })

    # plot all data as reference
    df_measurements = pd.read_sql_query('''SELECT "id", iv_file_sensor, iv_file_operator, assembly_step, timestamp  FROM "IV_Measurement" ''' + ";", con=engine)
    df_all = pd.read_sql_query('''SELECT "IV_measurement_id", voltage, current, ramp_direction  FROM "IV_Measurement_Results" ''' + ";", con=engine)
    df_measuremend_ids = df_all["IV_measurement_id"].unique()

    df_measurements['timestamp'] = pd.to_datetime(df_measurements['timestamp']).dt.date

    fig = go.Figure()
    for i in df_measuremend_ids:
        fig.add_trace(go.Line(
            x=df_all[df_all['IV_measurement_id'] == i]['voltage'],
            y=df_all[df_all['IV_measurement_id'] == i]['current'],
            name=str(df_measurements[df_measurements['id'] == i]['iv_file_sensor'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['iv_file_operator'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['timestamp'].unique()[0]) + " - " + str(df_measurements[df_measurements['id'] == i]['assembly_step'].unique()[0]), mode='lines+markers'))



    # CERN DATABASE:
    import sys
    from io import StringIO
    sys.path.append('application/scripts')
    from rhapi import RhApi
    url = "https://cmsdca.cern.ch/trk_rhapi"
    rh = RhApi(url, debug=False, sso="login")

    for sensor_name in df_measurements['iv_file_sensor'].unique():
        df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(sensor_name) + "'")), sep=",")
        run_numbers = df_cern['RUN_NUMBER'].unique()
        print(run_numbers)
        # df_cern['INSERTION_TIME'] = pd.to_datetime(df_cern['INSERTION_TIME']).dt.date

        for i in run_numbers:
            print(i)
            df_cern_details = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.datasets p where p.RUN_ID ='" + str(i) + "'")), sep=",")
            try:
                db_user = str(df_cern_details[df_cern_details['RUN_ID'] == i]['INSERTION_USER'].unique()[0])
                db_time = str(df_cern_details[df_cern_details['RUN_ID'] == i]['INSERTION_TIME'].unique()[0])
                db_number = str(df_cern_details[df_cern_details['RUN_ID'] == i]['RUN_TYPE_NUMBER'].unique()[0])
            except:
                db_user = str()
                db_time = str()
                db_number = str()

            print(df_cern_details)
            fig.add_trace(go.Line(
                x=df_cern[df_cern['RUN_NUMBER'] == i]['VOLTS'],
                y=df_cern[df_cern['RUN_NUMBER'] == i]['CURRNT_AMP'],
                name="CMS " + " - " + str(sensor_name) + " - " + db_user + " - " + db_time + " - " + db_number, mode='lines+markers'))


    fig.update_layout(
        title_text="IV-Curve",
        autosize=True,
        height=1000,
        paper_bgcolor="LightSteelBlue",
    )

    fig.update_layout(dict(updatemenus=[
        dict(
            type="buttons",
            direction="left",
            buttons=list([
                dict(
                    args=["visible", "legendonly"],
                    label="Deselect All",
                    method="restyle"
                ),
                dict(
                    args=["visible", True],
                    label="Select All",
                    method="restyle"
                )
            ]),
            pad={"r": 10, "t": 10},
            showactive=False,
            x=1,
            xanchor="left",
            y=0,
            yanchor="top"
        ),
        dict(active=1,
             buttons=list([
                 dict(label='Log Scale',
                      method='update',
                      args=[{'visible': [True, True]},
                            {'title': 'Log scale',
                             'yaxis': {'type': 'log'}}]),
                 dict(label='Linear Scale',
                      method='update',
                      args=[{'visible': [True, False]},
                            {'title': 'Linear scale',
                             'yaxis': {'type': 'linear'}}])
             ]),
         ),
    ]
    ))
    fig.update_xaxes(title_text="Voltage in Volt")
    fig.update_yaxes(title_text="Current in pA")
    fig.update_xaxes(autorange="reversed")
    fig.update_yaxes(autorange="reversed")

    fig_all = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    fig.write_html("file.html")

    return render_template("hv_plot.html", graphJSON_list=fig_all, plotly_layout=plotly_layout, plotly_config=plotly_config)



@flask_si.route('/id_<int:measurement_number>', methods=["GET", "POST"])
def si_detail(measurement_number):
    Surface_Inspection_data = SurfaceInspection.query.filter_by(id=measurement_number).first()

    all_ids_raw = pd.read_sql_query('''SELECT id, timestamp  FROM "Surface_Inspection" ORDER BY timestamp; ''', con=engine)
    all_ids = np.flip(all_ids_raw['id'].to_list())
    all_sensors = np.flip(all_ids_raw['timestamp'].to_list())

    images_data = pd.read_sql_query('''SELECT id, image_name  FROM "Surface_Inspection_Images" WHERE "SI_measurement_id" = ''' + str(measurement_number), con=engine)
    # print(images_data)
    images_data["path"] = str(Surface_Inspection_data.file_sensor) + "/" + str(Surface_Inspection_data.assembly_step) + "/" + str(Surface_Inspection_data.timestamp.strftime("%Y-%m-%d_%H-%M-%S")) + "/zoomify_deep_zoom_tiles/"
    images_data["extension"] = ".dzi"
    images = (images_data["path"]+images_data['image_name']+images_data["extension"]).to_list()
    print(url_for('static', filename='data/surface_inspection/Sensors' + images[3]))

    # query data from general dables
    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.aachen_module_id)
    module_list = list(zip(module_value, module_name))


    class HV_detailForm(FlaskForm):
        operator        = SelectField(u'Operator', default=Surface_Inspection_data.operator, choices=operator_list, validators=[DataRequired()])
        station         = SelectField(u'Station', default=Surface_Inspection_data.station, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'Grade', default=Surface_Inspection_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        # status          = SelectField(u'Station', default=Surface_Inspection_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=Surface_Inspection_data.comment)
        submit          = SubmitField("Update")

    form = HV_detailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        print(form.operator.data, form.station.data, form.comment.data)
        Surface_Inspection_data.operator           = form.operator.data
        Surface_Inspection_data.station            = form.station.data
        # Surface_Inspection_data.measurement_status = form.status.data
        Surface_Inspection_data.comment            = form.comment.data
        Surface_Inspection_data.manual_grade       = form.grade.data

        db.session.add(Surface_Inspection_data)
        db.session.commit()
        return redirect(url_for("flask_si.si_detail"))


    return render_template("si_detail.html", data=Surface_Inspection_data,  form=form, images=images, all_ids=all_ids, all_sensors=all_sensors)

@flask_si.route('/id_<int:measurement_number>/data')
def si_detail_data(measurement_number):
    db_table_class = SurfaceInspectionImages
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(SI_measurement_id=measurement_number)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
