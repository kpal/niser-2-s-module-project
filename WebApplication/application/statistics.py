import os
import jinja2
import paramiko
import pandas as pd
import influxdb_client
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG
from sqlalchemy import create_engine

from application                import app, db, bcrypt, mail
from application.models import *

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])
engine = create_engine('postgresql://'+db_username_DCA+':'+db_password_DCA+'@'+db_host_DCA + ':'+db_port_DCA+'/'+db_database_DCA)



flask_statistics = Blueprint('flask_statistics', __name__, template_folder="templates/statistics", url_prefix="/statistics")


@flask_statistics.route('/')
def statistics_overview():
    data = {}
    data['number_modules'] = ModuleOverview.query.distinct(ModuleOverview.id).distinct().count()

    data['metrology_sensor_measurements_total'] = MetrologySensorSensorAlignment.query.distinct(MetrologySensorSensorAlignment.id).distinct().count()
    data['metrology_bridge_measurements_total'] = MetrologySensorBridgeAlignment.query.distinct(MetrologySensorBridgeAlignment.id).distinct().count()
    data['metrology_height_measurements_total'] = MetrologyHeightProfile.query.distinct(MetrologyHeightProfile.id).distinct().count()

    data['surface_inspection_measurements_total'] = SurfaceInspection.query.distinct(SurfaceInspection.id).distinct().count()
    data['surface_inspection_images_total'] = SurfaceInspectionImage.query.distinct(SurfaceInspectionImage.id).distinct().count()

    # from pathlib import Path
    # data['file_size_metrology'] = subprocess.check_output("tree -h -d --du ").decode()
    # data['file_size_surface_inspection'] = sum([f.stat().st_size for f in Path(url_for('static', filename='data/surface_inspection')).glob("**/*")])


    sql_query = "select schemaname as table_schema, relname as table_name, pg_size_pretty(pg_relation_size(relid)) as data_size " \
                "from pg_catalog.pg_statio_user_tables " \
                "order by pg_relation_size(relid) desc;"
    postgres_size = pd.read_sql(sql_query, engine)
    postgres_size = postgres_size.to_html()


    return render_template("statistics_overview.html", data=data, postgres_size=postgres_size)
