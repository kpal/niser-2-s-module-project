from flask_wtf          import FlaskForm
from flask_wtf.file     import FileField, FileRequired
from wtforms            import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
import lxml
import csv
from bs4 import BeautifulSoup
import pandas as pd
import pandas_access


##############
##############
# FLASK FORMS
##############
##############
class LoginForm(FlaskForm):
	username    = StringField  (u'Username'  , validators=[DataRequired()])
	password    = PasswordField(u'Password'  , validators=[DataRequired()])
	submit = SubmitField("Login")

class RegisterForm(FlaskForm):
	name        = StringField  (u'Name'      , validators=[DataRequired(), Length(min=1, max=30)])
	surname		= StringField  (u'Surname'   , validators=[DataRequired(), Length(min=1, max=30)])
	username    = StringField  (u'Username'  , validators=[DataRequired(), Length(min=1, max=30)])
	password    = PasswordField(u'Password'  , validators=[DataRequired(), Length(min=3, max=130)])
	email       = StringField  (u'Email'     , validators=[DataRequired(), Email()])
	phone  		= StringField  (u'phone'     , validators=[Length(min=6, max=20), Regexp(regex='[0-9]')])
	club		= StringField  (u'club'      , validators=[Length(min=1, max=30)])
	function	= StringField  (u'club_function', validators=[Length(min=1, max=30)])
	acceptAGB 	= BooleanField ('I confirm that all the above informations are correct and that I am authorized to take decisions in the name of the stated club', validators=[DataRequired()])
	submit 		= SubmitField  ("Register")



class RegisterForm(FlaskForm):
	name        = StringField  (u'Name'      , validators=[DataRequired(), Length(min=1, max=30)])
	surname		= StringField  (u'Surname'   , validators=[DataRequired(), Length(min=1, max=30)])
	username    = StringField  (u'Username'  , validators=[DataRequired(), Length(min=1, max=30)])
	password    = PasswordField(u'Password'  , validators=[DataRequired(), Length(min=3, max=130)])
	email       = StringField  (u'Email'     , validators=[DataRequired(), Email()])
	phone  		= StringField  (u'phone'     , validators=[Length(min=6, max=20), Regexp(regex='[0-9]')])
	club		= StringField  (u'club'      , validators=[Length(min=1, max=30)])
	function	= StringField  (u'club_function', validators=[Length(min=1, max=30)])
	acceptAGB 	= BooleanField ('I confirm that all the above informations are correct and that I am authorized to take decisions in the name of the stated club', validators=[DataRequired()])
	submit 		= SubmitField  ("Register")
