import os
import sys
import time
import uuid
import lxml
import http.client
import urllib
import random
import datetime
from bs4 import BeautifulSoup
#from datetime import datetime
import threading
from flask                      import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_login                import LoginManager, login_required, login_user, logout_user, current_user, UserMixin
from flask_sqlalchemy           import SQLAlchemy
from werkzeug.utils             import secure_filename
from turbo_flask                import Turbo
from werkzeug.datastructures    import  FileStorage
from application                import app, db, bcrypt, mail

from flask_mail                 import Message
from flask_login                import login_user, current_user, logout_user, login_required
from email_validator            import validate_email, EmailNotValidError
import subprocess
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
import fillpdf
from fillpdf import fillpdfs

from .metrology import flask_metrology
app.register_blueprint(flask_metrology)

from .general import flask_general
app.register_blueprint(flask_general)

from .pulltest import flask_pulltest
app.register_blueprint(flask_pulltest)

from .modules import flask_modules
app.register_blueprint(flask_modules)

from .statistics import flask_statistics
app.register_blueprint(flask_statistics)

from .tools import flask_tools
app.register_blueprint(flask_tools)

from .iv import flask_iv
app.register_blueprint(flask_iv)

from .ivt import flask_ivt
app.register_blueprint(flask_ivt)

from .hv import flask_hv
app.register_blueprint(flask_hv)

from .surface_inspection import flask_si
app.register_blueprint(flask_si)

from .upload import flask_upload
app.register_blueprint(flask_upload)

from .workflow import flask_workflow
app.register_blueprint(flask_workflow)


########
########
# FLASK functions
########
########
@app.before_request
def before_request():
    session.permanent = True

# from here only webpages that require login
@app.route('/')
def home():
    return render_template("index.html")



@app.route('/about')
def about():
    return render_template("about.html")

#
# @app.route('/register', methods=["GET", "POST"])
# def register():
#     if current_user.is_authenticated:
#         return redirect(url_for('home'))
#     form = RegisterForm()
#     if form.validate_on_submit():
#         user = User(name=form.name.data,
#                     surname=form.surname.data,
#                     username=form.username.data,
#                     email=form.email.data,
#                     phone=form.phone.data,
#                     club=form.club.data,
#                     function=form.function.data)
#         user.set_password(form.password.data)
#         db.session.add(user)
#         db.session.commit()
#         flash(f'Account created for {form.username.data}! Please check if the confirmation email has arrived.', 'success')
#         try:
#             msg = Message(subject = 'Account registration!',
#                           recipients=[form.email.data],
#                           bcc = "backup@swim.beckers.lu",
#                           sender = ("SWIM Meet Management Application", 'noreply@swim.beckers.lu'),
#                           reply_to = "support@swim.beckers.lu"
#                           )
#             msg.body = "Hello! Thank you for your account registration. You can now login into your account. \n \n Please be aware that every action your take is only valid if it is confirmed by email. "
#             mail.send(msg)
#             pushover(f'New Account created for {form.username.data}')
#         except:
#             pass
#
#         return redirect(url_for("login"))
#
#     return render_template("register.html", title="Register Form", form=form)
#
#
#
#
# @app.route('/login', methods=["GET", "POST"])
# def login():
#     if current_user.is_authenticated:
#         return redirect(url_for('home'))
#     form = LoginForm()
#     if form.validate_on_submit():
#         username = form.username.data
#         password = form.password.data
#         user = User.query.filter_by(username=username).first()
#         if user is None or not user.check_password(form.password.data):
#             flash(f'Login failed! Please check username and password.', 'danger')
#         else:
#             login_user(user, remember=True)
#             flash(f'You ({username}) are now logged in !', 'success')
#             next_page = request.args.get('next')
#             try:
#                 pushover(f'login succesfull for {username}')
#             except:
#                 pass
#             if not next_page or url_parse(next_page).netloc != '':
#                 next_page = url_for('home')
#             return redirect(next_page)
#     return render_template("login.html", title="Login Form", form=form)

# @app.route("/logout")
# def logout():
#     logout_user()
#     return redirect(url_for('home'))
#
# @app.route("/profile")
# #@login_required
# def profile():
#     username = current_user.username
#     user = User.query.filter_by(username=username).first_or_404()
#
#     return render_template('profile.html', title='Account', user=user)
#
#
# @app.route("/admin")
# #@login_required
# def admin():
#     all_users = User.query.order_by(User.last_seen)
#     return render_template('admin.html', title='Admin', all_users=all_users)
#


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.datetime.utcnow()
        db.session.commit()


@app.errorhandler(404)
def error_404(error):
    return render_template('errors/error_404.html')


@app.errorhandler(403)
def error_403(error):
    return render_template('errors/error_403.html')


@app.errorhandler(500)
def error_500(error):
    return render_template('errors/error_500.html')



#
# from sqlalchemy import exc
# @app.errorhandler(exc.SQLAlchemyError)
# def handle_db_exceptions(error):
#     app.logger.error(error)
#     db.session.rollback()
#     return render_template("about.html")