import os
import json
import jinja2
import numpy as np
import paramiko
import pandas as pd
import matplotlib.pyplot as plt
from sqlalchemy import or_, and_
import plotly
from datetime import datetime
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import influxdb_client
from datetime import datetime, timezone, timedelta
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG
from application.models import *
from application                import app, db, bcrypt, mail, engine
import sys
from io import StringIO
sys.path.append('application/scripts')
from rhapi import RhApi
import sys
from io import StringIO
from cmsdbldr_client import LoaderClient

url = "https://cmsdca.cern.ch/trk_rhapi"
url_upload = "https://cmsdca.cern.ch/trk_loader/trker/int2r"

flask_modules = Blueprint('flask_modules', __name__, template_folder="templates/modules", url_prefix="/modules")

db_username_DCA        = str(os.environ['DB_USER'])
db_password_DCA        = str(os.environ['DB_PASSWORD'])
db_host_DCA            = str(os.environ['DB_HOSTNAME'])
db_port_DCA            = str(os.environ['DB_PORT'])
db_database_DCA        = str(os.environ['DB_Database'])




def datatables_json(db_table_class):
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data



@flask_modules.route('/modules')
def general_modules():
    return render_template("general_modules.html")
@flask_modules.route('/sensors')
def general_sensors():
    return render_template("general_sensors.html")
@flask_modules.route('/feh')
def general_feh():
    rh = RhApi(url, debug=False, sso="login")
    df_cern = pd.read_csv(StringIO(rh.csv("select p.ID, p.MANUFACTURER, p.KIND_OF_PART, p.BARCODE, p.SERIAL_NUMBER, p.PRODUCTION_DATE,  "
                                          "loc.LOCATION_NAME, loc.INSTITUTITON_NAME from trker_cmsr.p6640 p inner join trker_cmsr.parts parts on parts.ID = p.ID inner join trker_cmsr.trkr_locations_v loc on loc.LOCATION_ID = parts.LOCATION_ID where loc.LOCATION_NAME = 'Aachen 1B'")), sep=",")

    return render_template("general_feh.html", data=df_cern.to_html())

@flask_modules.route('/seh')
def general_seh():
    rh = RhApi(url, debug=False, sso="login")
    df_cern = pd.read_csv(StringIO(rh.csv("select p.ID, p.MANUFACTURER, p.KIND_OF_PART, p.BARCODE, p.SERIAL_NUMBER, p.PRODUCTION_DATE,  "
                                          "loc.LOCATION_NAME, loc.INSTITUTITON_NAME from trker_cmsr.p6660 p inner join trker_cmsr.parts parts on parts.ID = p.ID inner join trker_cmsr.trkr_locations_v loc on loc.LOCATION_ID = parts.LOCATION_ID where loc.LOCATION_NAME = 'Aachen 1B'")), sep=",")


    return render_template("general_seh.html", data=df_cern.to_html())
@flask_modules.route('/modules_data')
def general_module_data():
    json_data = datatables_json(ModuleOverview)
    return json_data
@flask_modules.route('/sensors_data')
def general_sensors_data():
    json_data = datatables_json(SensorOverview)
    return json_data

@flask_modules.route('/add_module', methods=['GET', 'POST'])
def general_add_module():
    class general_add_ModuleForm(FlaskForm):
        aachen_module_id    = StringField(u'Aachen_Module_id')
        cern_module_id      = StringField(u'CERN_Module_id', validators=[DataRequired()])
        timestamp           = StringField()
        operator            = StringField()
        # Module_type         = StringField(u'Module_type', validators=[DataRequired()])
        # arrival_date        = StringField(u'arrival_date (to Aachen)')
        # shipping_date       = StringField(u'shipping date (from Aachen)')
        # aachen_construction_status              = SelectField(u'Status', choices=["aachen_defined", "aachen_started"])
        comment             = TextAreaField(u'Comment')
        submit              = SubmitField("Add")

    form = general_add_ModuleForm()
    if form.validate_on_submit():
        flash('Submission successfull!', 'success')
        ModuleOverview_data = ModuleOverview(
            aachen_module_id=form.aachen_module_id.data,
            cern_module_id=form.cern_module_id.data,
            comment=form.comment.data,
            aachen_construction_status = "aachen_underConstruction"
        )
        db.session.add(ModuleOverview_data)

        if form.timestamp.data == None:
            timestamp =  datetime.now()
        else:
            timestamp = form.timestamp.data

        WorkflowAachenDefined_data = WorkflowAachenDefined(
            module_name=form.cern_module_id.data,
            operator=form.operator.data,
            timestamp = timestamp,
            comment=form.comment.data,
        )
        db.session.add(WorkflowAachenDefined_data)
        db.session.commit()

    return render_template("add_module.html", form=form)

@flask_modules.route('/data_under_construction', methods=['GET', 'POST'])
def module_under_construction_data():
    db_table_class = ModuleOverview
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter(and_(db_table_class.aachen_construction_status != 'aachen_finished', db_table_class.aachen_construction_status != 'aachen_shipped'))

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "id")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data


@flask_modules.route('/data_under_construction_sensor')
def module_under_construction_data_sensor():
    db_table_class = SensorOverview
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    # query = db_table_class.query.filter(and_(db_table_class.aachen_construction_status != 'aachen_finished', db_table_class.aachen_construction_status != 'aachen_shipped'))
    query = db_table_class.query

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "id")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data




@flask_modules.route('/update_module_status_<cern_module_id>', methods=['GET', 'POST'])
def update_module_status_cmsdb(cern_module_id):
    data = ModuleOverview.query.filter_by(cern_module_id=cern_module_id).first()

    class update_module_status_cmsdbForm(FlaskForm):
        # run_type        = StringField(u'run_type', validators=[DataRequired()])
        # location        = StringField(u'Location', validators=[DataRequired()])
        # operator        = StringField(u'operator', validators=[DataRequired()])
        # StringField(u'run_type', validators=[DataRequired()])
        submit = SubmitField("I confirm to upload")

    form = update_module_status_cmsdbForm()

    # create XML file
    templateFilePath = jinja2.FileSystemLoader('application/templates/upload/DatabaseTemplates')
    jinjaEnv = jinja2.Environment(loader=templateFilePath)
    jTemplate = jinjaEnv.get_template("template_module_constructionstatus.xml")
    data = ModuleOverview.query.filter_by(cern_module_id=cern_module_id).first()

    # format Datapoints for XML-file
    # CERN MODULE LAYOUT: 2S_18_5_AAC-00001 and 2S_40_6_AAC-00001
    cern_module_id_spacing = str(cern_module_id.split('_')[1][0])+"."+str(cern_module_id.split('_')[1][1]) + ' mm'
    cern_module_id_cooling = cern_module_id.split('_')[2]
    try:
        institute_number = data.current_location.split(" ")[1]
    except:
        institute_number = ""

    data_dict = []
    print(data.sensor_top)
    print(type(data.sensor_top))
    if str(data.sensor_top) != "None":
        df = pd.read_sql_query('''SELECT data.timestamp FROM "Workflow_mod_bareModule" data where data.module_name = ''' + "'" + str(cern_module_id) + "' ORDER BY timestamp Limit 1", con=engine)
        data_dict.append(
            {
                'kind': "2S Sensor",
                'serial_number': data.sensor_top,
                'comment': str(data.Sensor_Overview_Top.comment) if data.Sensor_Overview_Top.comment != None else '',
                'timestamp': df['timestamp'][0]
            }
        )
    if str(data.sensor_bottom) != "None":
        df = pd.read_sql_query('''SELECT data.timestamp FROM "Workflow_mod_bareModule" data where data.module_name = ''' + "'" + str(cern_module_id) + "' ORDER BY timestamp Limit 1", con=engine)
        data_dict.append(
            {
                'kind': "2S Sensor",
                'serial_number': data.sensor_bottom,
                'comment': str(data.Sensor_Overview_Bottom.comment) if data.Sensor_Overview_Bottom.comment != None else '',
                'timestamp': df['timestamp'][0]
            }
        )
    if str(data.seh) != "None":
        df = pd.read_sql_query('''SELECT data.timestamp FROM "Workflow_mod_bareSEH" data where data.module_name = ''' + "'" + str(cern_module_id) + "' ORDER BY timestamp Limit 1", con=engine)
        data_dict.append(
            {
                'kind': "2S Service Hybrid",
                'serial_number': data.seh,
                'comment': "SEH",
                'timestamp': df['timestamp'][0]
            }
        )
    if str(data.feh_right) != "None":
        df = pd.read_sql_query('''SELECT data.timestamp FROM "Workflow_mod_bareFEH" data where data.module_name = ''' + "'" + str(cern_module_id) + "' ORDER BY timestamp Limit 1", con=engine)
        data_dict.append(
            {
                'kind': "2S Front-end Hybrid",
                'serial_number': data.feh_right,
                'comment': "FEH right",
                'timestamp': df['timestamp'][0]
            }
        )
    if str(data.feh_left) != "None":
        df = pd.read_sql_query('''SELECT data.timestamp FROM "Workflow_mod_bareFEH" data where data.module_name = ''' + "'" + str(cern_module_id) + "' ORDER BY timestamp Limit 1", con=engine)
        data_dict.append(
            {
                'kind': "2S Front-end Hybrid",
                'serial_number': data.feh_left,
                'comment': "FEH Left",
                'timestamp': df['timestamp'][0]
            }
        )

    xml_data = {'serial_number': data.cern_module_id,
                'location': "Aachen " + institute_number,
                'institution': "RWTH Aachen University " + str(institute_number),
                'comment':  data.comment if data.comment != None else '',
                'structure': "2S Module",
                'contact_points': cern_module_id_cooling,
                'spacing': cern_module_id_spacing,
                'module_status': data.module_status if data.module_status != None else '',
                'id': data.cern_module_id,
                'timestamp': data.create_ts.replace(microsecond=0),
                'children': data_dict,
                }

    output = jTemplate.render(xml_data)
    xml_file = output

    if form.validate_on_submit():
        datetime.now()
        file_path = 'application/static/data/xml/construction/output_' + "2S Module" + " - " + str(data.cern_module_id) + " - " + str(datetime.now()) + '.xml'
        outFileH = open(file_path, 'w')
        outFileH.write(output)
        outFileH.close()


        print("try uploading data to cern db")

        cli = LoaderClient()
        cmd = ['--url', url_upload, '--test', '--login', file_path]

        responseCode = cli.run(cmd)
        if responseCode == 1:
            responseCode = "Upload successful"
            status_color = 'success'
            ModuleOverview.query.filter_by(cern_module_id=data.cern_module_id).update(dict(cmsdb_upload_status=True, cmsdb_upload_timestamp=datetime.now()))
            db.session.commit()
        elif responseCode == 4:
            responseCode = "Upload failed"
            status_color = 'danger'
            ModuleOverview.query.filter_by(cern_module_id=data.cern_module_id).update(dict(cmsdb_upload_status=False))
            db.session.commit()
        else:
            status_color = 'warning'
            ModuleOverview.query.filter_by(cern_module_id=data.cern_module_id).update(dict(cmsdb_upload_status=False))
            db.session.commit()


        print(responseCode)
        flash('Submission done - answer code of the server is ' + str(responseCode),  status_color)

        return redirect(url_for("flask_modules.general_add_module"))



    return render_template("module_construction_status_upload.html", data=data, form=form, xml_file=xml_file)



@flask_modules.route('/all')
def modules_all():
    return render_template("modules_all.html")

@flask_modules.route('/laufzettel')
def modules_laufzettel():
    return render_template("modules_laufzettel.html")

@flask_modules.route('/module_<cern_module_id>', methods=['GET', 'POST'])
def module_detail(cern_module_id):
    ModuleOverview_data = ModuleOverview.query.filter_by(cern_module_id=cern_module_id).first()
    # ModuleTool_data = ModuleTool.query.filter_by(cern_module_id=cern_module_id).first()

    try:
        # get sensor details from CERN DB:
        # CERN DATABASE:
        print("try getting data from cern db")
        url = "https://cmsdca.cern.ch/trk_rhapi"
        rh = RhApi(url, debug=False, sso="login")
        pNameLabel = ModuleOverview_data.cern_module_id
        query = "select pp.NAME_LABEL as MODULE_NAME, " \
                "pp.A2S_SENSOR_SPACING as SPACING, part.MANUFACTURER, " \
                "s.A2S_SENSOR_POSN as SENSOR_POSITION, " \
                "pp.ASTATUS as MODULE_STATUS, " \
                "pp.DESCRIPTION, " \
                "r.CHILD_NAME_LABEL AS PART_NAME_LABEL, " \
                "r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, " \
                "feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, " \
                "feh.ACIC_VERSION as CIC_VERSION, " \
                "feh.KIND_OF_PART as FEH_KIND_OF_PART, " \
                "seh.KIND_OF_PART as SEH_KIND_OF_PART, " \
                "manufacturers.COMMENT_DESCRIPTION as MANUFACTURER_COMMENT_DESCRIPTION, " \
                "s.KIND_OF_PART as SENSOR_KIND_OF_PART\
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9000 pp on pp.ID = part.ID \
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6640 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6660 seh on r.CHILD_SERIAL_NUMBER=seh.SERIAL_NUMBER\
                left join trker_cmsr.p1120 s on r.CHILD_NAME_LABEL=s.NAME_LABEL\
                left join trker_cmsr.trkr_manufacturers_v manufacturers on manufacturers.MANUFACTURER = pp.MANUFACTURER \
                where part.NAME_LABEL='" + pNameLabel + "'"

        # df_cern = pd.read_csv(StringIO(rh.csv(query)), sep=",")

        module_name = df_cern['MODULE_NAME'][0]

        ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(cern_db_description=df_cern['DESCRIPTION'][0]))
        ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(manufacturer=df_cern['MANUFACTURER'][0]))
        ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(cern_db_module_status=df_cern['MODULE_STATUS'][0]))
        ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(cern_db_update_ts=datetime.now()))
        ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(spacing=df_cern['SPACING'][0]))
        # ModuleOverview_data.cern_db_description     =
        # ModuleOverview_data.manufacturer            = df_cern['MANUFACTURER'][0]
        # ModuleOverview_data.cern_db_module_status   = df_cern['MODULE_STATUS'][0]
        # ModuleOverview_data.cern_db_update_ts       = datetime.now()
        # ModuleOverview_data.spacing                 = df_cern['SPACING'][0]
        # db.session.add(ModuleOverview_data)
        db.session.commit()

        for i in range(len(df_cern)):
            # print(df_cern.iloc[i])
            if df_cern.iloc[i]['SENSOR_KIND_OF_PART'] == '2S Sensor':
                if df_cern.iloc[i]['SENSOR_POSITION'] == 'TOP':
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(sensor_top=df_cern.iloc[i]['PART_NAME_LABEL']))
                    db.session.commit()
                if df_cern.iloc[i]['SENSOR_POSITION'] == 'BOTTOM':
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(sensor_bottom=df_cern.iloc[i]['PART_NAME_LABEL']))
                    db.session.commit()
            if df_cern.iloc[i]['SEH_KIND_OF_PART'] == '2S Service Hybrid':
                ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(seh=df_cern.iloc[i]['PART_SERIAL_NUMBER']))
                db.session.commit()
            if df_cern.iloc[i]['FEH_KIND_OF_PART'] == '2S Front-end Hybrid':
                if df_cern.iloc[i]['FEH_HYBRID_SIDE'] == 'Right':
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(feh_right=df_cern.iloc[i]['PART_SERIAL_NUMBER']))
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(feh_right_cic=df_cern.iloc[i]['CIC_VERSION']))
                    db.session.commit()
                if df_cern.iloc[i]['FEH_HYBRID_SIDE'] == 'Left':
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(feh_left=df_cern.iloc[i]['PART_SERIAL_NUMBER']))
                    ModuleOverview_data.query.filter_by(cern_module_id=module_name).update(dict(feh_right_cic=df_cern.iloc[i]['CIC_VERSION']))
                    db.session.commit()

    except Exception as e:
        print('Error getting CERN data - using local data copy: ', e)

    ModuleOverview_data = ModuleOverview.query.filter_by(cern_module_id=cern_module_id).first()

    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.cern_module_id)
    module_list = list(zip(module_value, module_name))

    sensor_data = SensorOverview.query.all()
    sensor_value = []
    sensor_name = []
    for sensor in sensor_data:
        sensor_value.append(sensor.id)
        sensor_name.append(sensor.sensor_name)
    sensor_list = list(zip(sensor_value, sensor_name))

    jig_baremodule_data = ToolsJigsBaremodule.query.all()
    value = []
    name = []
    for jig in jig_baremodule_data:
        value.append(jig.id)
        name.append(jig.name)
    jig_baremodule_list = list(zip(value, name))

    jig_bonding_data = ToolsJigsBonding.query.all()
    value = []
    name = []
    for jig in jig_bonding_data:
        value.append(jig.id)
        name.append(jig.name)
    jig_bonding_list = list(zip(value, name))

    jig_HV_pigtaile_data = ToolsJigsHVpigtaile.query.all()
    value = []
    name = []
    for jig in jig_HV_pigtaile_data:
        value.append(jig.id)
        name.append(jig.name)
    jig_HV_pigtaile_list = list(zip(value, name))


    # Get module construction data
    module_name = ModuleOverview_data.cern_module_id
    print(module_name)
    df = pd.read_sql_query(
        '''SELECT \
        defined.timestamp as "Aachen - Modulename is created", \
        bareModule.timestamp as "1. Bare module assembly", \
        basePlate.timestamp as "2. Bare module to baseplate", \
        bareFEH.timestamp as "3. FEH assembly", \
        bareSEH.timestamp as "4. SEH fully assembled", \
        bonded.timestamp as "5. Module wire-bonded", \
        topEncapsulated.timestamp as "6. After topEncapsulated", \
        bottomEncapsulated.timestamp as "7. After bottomEncapsulated", \
        final.timestamp as "8. Final module",  \
        finalCold.timestamp as "9. Final module cold", \
        finished.timestamp as "Aachen - Module construction is finished" \
        ''' +
        '''FROM "Module_Overview" module ''' +
        '''left join "Workflow_aachen_defined" defined on defined.module_name = module.cern_module_id ''' +
        '''left join "Workflow_aachen_finished" finished on finished.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareFEH" bareFEH on bareFEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareSEH" bareSEH on bareSEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareModule" bareModule on bareModule.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_basePlate" basePlate on basePlate.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bonded" bonded on bonded.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_topEncapsulated" topEncapsulated on topEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bottomEncapsulated" bottomEncapsulated on bottomEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_final" final on final.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_finalCold" finalCold on finalCold.module_name = module.cern_module_id ''' +
        '''where module.cern_module_id = ''' + "'" + str(module_name) + "'", con=engine)
    columns = df.columns
    list_columns = []
    list_values = []
    for column in df:
        df[column] = df[column].fillna("")
        list_columns.append(column)
        list_values.append(df[column].max())
    assembly_data = dict(zip(list_columns, list_values))
    list_assemblynames = list_columns
    list_timestamp_values = list_values

    df = pd.read_sql_query(
        '''SELECT \
        defined.comment as "Aachen - Modulename is created", \
        bareModule.comment as "1. Bare module assembly", \
        basePlate.comment as "2. Bare module to baseplate", \
        bareFEH.comment as "3. FEH assembly", \
        bareSEH.comment as "4. SEH fully assembled", \
        bonded.comment as "5. Module wire-bonded", \
        topEncapsulated.comment as "6. After topEncapsulated", \
        bottomEncapsulated.comment as "7. After bottomEncapsulated", \
        final.comment as "8. Final module",  \
        finalCold.comment as "9. Final module cold", \
        finished.comment as "Aachen - Module construction is finished" \
        ''' +
        '''FROM "Module_Overview" module ''' +
        '''left join "Workflow_aachen_defined" defined on defined.module_name = module.cern_module_id ''' +
        '''left join "Workflow_aachen_finished" finished on finished.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareFEH" bareFEH on bareFEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareSEH" bareSEH on bareSEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareModule" bareModule on bareModule.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_basePlate" basePlate on basePlate.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bonded" bonded on bonded.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_topEncapsulated" topEncapsulated on topEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bottomEncapsulated" bottomEncapsulated on bottomEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_final" final on final.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_finalCold" finalCold on finalCold.module_name = module.cern_module_id ''' +
        '''where module.cern_module_id = ''' + "'" + str(module_name) + "'", con=engine)
    columns = df.columns
    list_columns = []
    list_values_comment = []
    for column in df:
        df[column] = df[column].fillna("")
        list_columns.append(column)
        list_values_comment.append(df[column].max())
    assembly_data_comment = dict(zip(list_columns, list_values_comment))

    df = pd.read_sql_query(
        '''SELECT \
        defined.operator as "Aachen - Modulename is created", \
        bareModule.operator as "1. Bare module assembly", \
        basePlate.operator as "2. Bare module to baseplate", \
        bareFEH.operator as "3. FEH assembly", \
        bareSEH.operator as "4. SEH fully assembled", \
        bonded.operator as "5. Module wire-bonded", \
        topEncapsulated.operator as "6. After topEncapsulated", \
        bottomEncapsulated.operator as "7. After bottomEncapsulated", \
        final.operator as "8. Final module",  \
        finalCold.operator as "9. Final module cold", \
        finished.operator as "Aachen - Module construction is finished" \
        ''' +
        '''FROM "Module_Overview" module ''' +
        '''left join "Workflow_aachen_defined" defined on defined.module_name = module.cern_module_id ''' +
        '''left join "Workflow_aachen_finished" finished on finished.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareFEH" bareFEH on bareFEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareSEH" bareSEH on bareSEH.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bareModule" bareModule on bareModule.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_basePlate" basePlate on basePlate.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bonded" bonded on bonded.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_topEncapsulated" topEncapsulated on topEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_bottomEncapsulated" bottomEncapsulated on bottomEncapsulated.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_final" final on final.module_name = module.cern_module_id ''' +
        '''left join "Workflow_mod_finalCold" finalCold on finalCold.module_name = module.cern_module_id ''' +
        '''where module.cern_module_id = ''' + "'" + str(module_name) + "'", con=engine)
    columns = df.columns
    list_columns = []
    list_values = []
    for column in df:
        df[column] = df[column].fillna("")
        print(df[column])
        list_columns.append(column)
        list_values.append(df[column].max())
    list_operators = list_values
    assembly_data_operator = dict(zip(list_columns, list_values))


    data = []
    data.append(list_assemblynames)
    data.append(list_timestamp_values)
    data.append(list_operators)
    df = pd.DataFrame(data).transpose()
    df.columns = ['assemblystep', 'timestamp', 'operator']
    print(df)


    # project start date

    proj_start = assembly_data['Aachen - Modulename is created']
    # number of days from project start to task start
    df['start_num'] = (df.timestamp - proj_start).dt.days
    # number of days from project start to end of tasks
    df['end_num'] = (df.timestamp - proj_start).dt.days
    # days between start and end of each task
    df['days_start_to_end'] = df.end_num - df.start_num
    print(df)

    fig, ax = plt.subplots(1, figsize=(16, 6))
    ax.barh(df.assemblystep, df.days_start_to_end, left=df.start_num)
    plt.savefig('gantt.pdf')

    # create a column with the color for each department
    # def color(row):
    #     c_dict = {'MKT': '#E64646', 'FIN': '#E69646', 'ENG': '#34D05C', 'PROD': '#34D0C3', 'IT': '#3475D0'}
    #     return c_dict[row['Department']]
    #
    # df['color'] = df.apply(color, axis=1)
    from matplotlib.patches import Patch
    fig, ax = plt.subplots(1, figsize=(16, 6))
    # bars
    # ax.barh(df.assemblystep, df.current_num, left=df.start_num, color=df.color)
    ax.barh(df.assemblystep, df.days_start_to_end, left=df.start_num, alpha=0.5)
    # texts
    # for idx, row in df.iterrows():
    #     ax.text(row.end_num + 0.1, idx,
    #             f"{int(row.Completion * 100)}%",
    #             va='center', alpha=0.8)
    ##### LEGENDS #####
    c_dict = {'MKT': '#E64646', 'FIN': '#E69646', 'ENG': '#34D05C', 'PROD': '#34D0C3', 'IT': '#3475D0'}
    legend_elements = [Patch(facecolor=c_dict[i], label=i) for i in c_dict]
    plt.legend(handles=legend_elements)
    ##### TICKS #####
    xticks = np.arange(0, df.end_num.max() + 1, 3)
    xticks_labels = pd.date_range(proj_start, end=assembly_data['Aachen - Module construction is finished']).strftime("%m/%d")
    xticks_minor = np.arange(0, df.end_num.max() + 1, 1)
    ax.set_xticks(xticks)
    ax.set_xticks(xticks_minor, minor=True)
    ax.set_xticklabels(xticks_labels[::3])
    plt.savefig('gantt2.pdf')


    class ModuleDetailForm(FlaskForm):
        module_type  = SelectField(u'Module', default=ModuleOverview_data.cern_module_id, choices=module_list, validators=[])
        arrival_date = StringField(u'Arrival Date', default=ModuleOverview_data.arrival_date, validators=[])
        shipping_date = StringField(u'Shipping Date', default=ModuleOverview_data.shipping_date, validators=[])
        module_status = SelectField(u'Station', default=ModuleOverview_data.module_status, choices=status_list, validators=[])
        current_location = StringField(u'Location', default=ModuleOverview_data.current_location, validators=[])
        comment         = TextAreaField(u'Comment', default=ModuleOverview_data.comment)
        manual_grade           = SelectField(u'Grade', choices=grade_list, validators=[])
        feh_1_id        = StringField(u'Feh 1', validators=[])
        feh_2_id        = StringField(u'Feh 2', validators=[])
        seh_id        = StringField(u'SEH', validators=[])
        top_sensor      = SelectField(u'Top Sensor', choices=sensor_list, validators=[])
        bottom_sensor   = SelectField(u'Bottom Sensor', choices=sensor_list, validators=[])
        jig_baremodule  = SelectField(u'jig_baremodule', validators=[])
        jig_bonding     = SelectField(u'jig_bonding', validators=[])
        jig_HV_pigtaile = SelectField(u'jig_HV_pigtaile', validators=[])
        submit = SubmitField("Update")

    form = ModuleDetailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        # print(form.module.data, form.location.data, form.comment.data)
        ModuleOverview_data.module_type = form.module_type.data
        ModuleOverview_data.arrival_date = form.arrival_date.data
        ModuleOverview_data.shipping_date = form.shipping_date.data
        ModuleOverview_data.module_status = form.module_status.data
        ModuleOverview_data.current_location = form.current_location.data
        ModuleOverview_data.current_location_timestamp = datetime.now(timezone.utc)
        ModuleOverview_data.feh_right = form.feh_1_id.data
        ModuleOverview_data.feh_left = form.feh_2_id.data
        ModuleOverview_data.seh_id = form.seh_id.data
        ModuleOverview_data.sensor_top = form.top_sensor.data
        ModuleOverview_data.sensor_bottom = form.bottom_sensor.data
        ModuleOverview_data.comment = form.comment.data
        ModuleOverview_data.current_location = form.current_location.data
        ModuleTool_data.jig_baremodule = form.jig_baremodule.data
        ModuleTool_data.jig_bonding = form.jig_bonding.data
        ModuleTool_data.jig_HV_pigtaile = form.jig_HV_pigtaile.data
        db.session.add(ModuleOverview_data)
        db.session.add(ModuleTool_data)
        db.session.commit()
        return redirect(url_for("flask_modules.module_detail", sensor_number=ModuleOverview_data.id), form=form)

    print(ModuleOverview_data.to_dict)

    return render_template("modules_detail.html", data=ModuleOverview_data, form=form, assembly_data=assembly_data, assembly_data_comment=assembly_data_comment, assembly_data_operator=assembly_data_operator)

# @flask_modules.route('/data/module_<module_number>')
# # Get CMS DB data for this module
# def module_detail_cern_db(module_number):
#
#     json_data = {'data': df_cern.to_dict(orient="records"),
#         'recordsFiltered': 0,
#         'recordsTotal': len(df_cern),
#         'draw': request.args.get('draw', type=int),
#          }
#     json_string = json.dumps(json_data, default=str)
#     json_string = json_string.replace("NaN", "null")
#     json_data = json.loads(json_string)
#     return json_data

@flask_modules.route('/sensor_<sensor_name>', methods=['GET', 'POST'])
def sensor_detail(sensor_name):
    SensorOverview_data = SensorOverview.query.filter_by(sensor_name=sensor_name).first()

    tech_data = {}
    # Fast Switch Button in Header
    all_ids_raw = pd.read_sql_query('''SELECT sensor_name  FROM "Sensor_Overview" ORDER BY sensor_name; ''', con=engine)
    all_ids = np.flip(all_ids_raw['sensor_name'].to_list())
    all_sensors = np.flip(all_ids_raw['sensor_name'].to_list())
    tech_data['button_id'] = all_ids
    tech_data['button_name'] = all_sensors

    try:
        # get sensor details from CERN DB:
        # CERN DATABASE:
        print("try getting data from cern db")
        import sys
        from io import StringIO
        sys.path.append('application/scripts')
        from rhapi import RhApi
        url = "https://cmsdca.cern.ch/trk_rhapi"

        rh = RhApi(url, debug=False, sso="login")
        df_cern_sensor = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trker_sensors_v p where p.SNSR_NAME ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
        df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.p1120 p where p.NAME_LABEL = " + "'" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
        print(df_cern)
        # print(df_cern_sensor)

        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_serial_number=df_cern_sensor['SNSR_SER_NUM'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_type=df_cern_sensor['SENSOR_TYPE'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_manufacturer=df_cern_sensor['MANUFACTURER_NAME'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_location=df_cern_sensor['LOCATION_NAME'][0]))
        # db.session.commit()


        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_production_date=df_cern['PRODUCTION_DATE'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_batch_number=str(df_cern['BATCH_NUMBER'][0])))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_barcode=df_cern['BARCODE'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_serial_number=df_cern['SERIAL_NUMBER'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_position=df_cern['A2S_SENSOR_POSN'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_known_problem=df_cern['BARCODE'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_orientation=df_cern['ASENSOR_ORIENTATION'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_type=df_cern['ASENSOR_TYPE'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_status=df_cern['ASTATUS'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(sensor_description=df_cern['DESCRIPTION'][0]))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(cern_db_update_ts=datetime.now()))
        SensorOverview_data.query.filter_by(sensor_name=SensorOverview_data.sensor_name).update(dict(cern_db_update_ts=datetime.now()))
        db.session.commit()

    except Exception as e:
        print('Error getting CERN data - using local data copy: ', e)

    SensorOverview_data = SensorOverview.query.filter_by(sensor_name=sensor_name).first()

    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.cern_module_id)
    module_list = list(zip(module_value, module_name))


    # Get module construction data
    sensor_name = SensorOverview_data.sensor_name
    print(sensor_name)
    df = pd.read_sql_query(
        '''SELECT \
        bareSensor.timestamp as "1. Bare sensor", \
        polyimide.timestamp as "2. Mod Polyimide", \
        tail.timestamp as "3. tail", \
        tailBonded.timestamp as "4. tailBonded", \
        tailEncapsulated.timestamp as "5. tailEncapsulated", \
        spacersToPSs.timestamp as "6. spacersToPSs" \
        ''' +
        '''FROM "Sensor_Overview" sensor ''' +
        '''left join "Workflow_mod_bareSensor" bareSensor on bareSensor.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_polyimide" polyimide on polyimide.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tail" tail on tail.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailBonded" tailBonded on tailBonded.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailEncapsulated" tailEncapsulated on tailEncapsulated.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_spacersToPSs" spacersToPSs on spacersToPSs.sensor_name = sensor.sensor_name ''' +
        '''where sensor.sensor_name = ''' + "'" + str(sensor_name) + "'", con=engine)

    columns = df.columns
    list_columns = []
    list_values = []
    for column in df:
        list_columns.append(column)
        list_values.append(df[column].max())
    assembly_data = dict(zip(list_columns, list_values))

    df = pd.read_sql_query(
        '''SELECT \
        bareSensor.comment as "1. Bare sensor", \
        polyimide.comment as "2. Mod Polyimide", \
        tail.comment as "3. tail", \
        tailBonded.comment as "4. tailBonded", \
        tailEncapsulated.comment as "5. tailEncapsulated", \
        spacersToPSs.comment as "6. spacersToPSs" \
        ''' +
        '''FROM "Sensor_Overview" sensor ''' +
        '''left join "Workflow_mod_bareSensor" bareSensor on bareSensor.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_polyimide" polyimide on polyimide.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tail" tail on tail.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailBonded" tailBonded on tailBonded.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailEncapsulated" tailEncapsulated on tailEncapsulated.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_spacersToPSs" spacersToPSs on spacersToPSs.sensor_name = sensor.sensor_name ''' +
        '''where sensor.sensor_name = ''' + "'" + str(sensor_name) + "'", con=engine)

    columns = df.columns
    list_columns = []
    list_values = []
    for column in df:
        print(df[column])
        list_columns.append(column)
        list_values.append(df[column].max())
    assembly_data_comment = dict(zip(list_columns, list_values))

    df = pd.read_sql_query(
        '''SELECT \
        bareSensor.operator as "1. Bare sensor", \
        polyimide.operator as "2. Mod Polyimide", \
        tail.operator as "3. tail", \
        tailBonded.operator as "4. tailBonded", \
        tailEncapsulated.operator as "5. tailEncapsulated", \
        spacersToPSs.operator as "6. spacersToPSs" \
        ''' +
        '''FROM "Sensor_Overview" sensor ''' +
        '''left join "Workflow_mod_bareSensor" bareSensor on bareSensor.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_polyimide" polyimide on polyimide.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tail" tail on tail.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailBonded" tailBonded on tailBonded.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_tailEncapsulated" tailEncapsulated on tailEncapsulated.sensor_name = sensor.sensor_name ''' +
        '''left join "Workflow_mod_spacersToPSs" spacersToPSs on spacersToPSs.sensor_name = sensor.sensor_name ''' +
        '''where sensor.sensor_name = ''' + "'" + str(sensor_name) + "'", con=engine)

    columns = df.columns
    list_columns = []
    list_values = []
    for column in df:
        list_columns.append(column)
        list_values.append(df[column].max())

    assembly_data_operator = dict(zip(list_columns, list_values))



    class SensorDetailForm(FlaskForm):
        module          = SelectField(u'Module', default=SensorOverview_data.module, choices=module_list, validators=[DataRequired()])
        location        = SelectField(u'current Location', default=SensorOverview_data.current_location, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'manual Grade', default=SensorOverview_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        # status          = SelectField(u'Station', default=HVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=SensorOverview_data.comment)
        submit          = SubmitField("Update")

    form = SensorDetailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        print(form.module.data, form.location.data, form.comment.data)
        SensorOverview_data.module             = form.module.data
        SensorOverview_data.current_location   = form.location.data
        SensorOverview_data.current_location_timestamp = datetime.now(timezone.utc)
        SensorOverview_data.comment            = form.comment.data
        SensorOverview_data.manual_grade       = form.grade.data

        db.session.add(SensorOverview_data)
        db.session.commit()
        return redirect(url_for("flask_modules.sensor_detail", sensor_number=SensorOverview_data.id))


    return render_template("sensor_detail.html", data=SensorOverview_data, tech_data=tech_data, form=form, assembly_data=assembly_data, assembly_data_comment=assembly_data_comment, assembly_data_operator=assembly_data_operator)

@flask_modules.route('/sensor/old_<serial_number>', methods=['GET', 'POST'])
def sensor_detail_serial(serial_number):
    SensorOverview_data = SensorOverview.query.filter_by(sensor_name=serial_number).first()

    tech_data = {}
    # Fast Switch Button in Header
    all_ids_raw = pd.read_sql_query('''SELECT id, sensor_name  FROM "Sensor_Overview" ORDER BY sensor_name; ''', con=engine)
    all_ids = np.flip(all_ids_raw['id'].to_list())
    all_sensors = np.flip(all_ids_raw['sensor_name'].to_list())
    tech_data['button_id'] = all_ids
    tech_data['button_name'] = all_sensors

    # get sensor details from CERN DB:
    # CERN DATABASE:
    cern_data = {}
    try:
        print("try getting data from cern db")
        import sys
        from io import StringIO
        sys.path.append('application/scripts')
        from rhapi import RhApi
        url = "https://cmsdca.cern.ch/trk_rhapi"

        rh = RhApi(url, debug=False, sso="login")
        df_cern_sensor = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trker_sensors_v p where p.SNSR_NAME ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
        print(df_cern_sensor)
        try:
            cern_data['sensor'] = df_cern_sensor['SENSOR'][0]
            cern_data['serial_number'] = df_cern_sensor['SNSR_SER_NUM'][0]
            cern_data['name'] = df_cern_sensor['SNSR_NAME'][0]
            cern_data['type'] = df_cern_sensor['SENSOR_TYPE'][0]
            cern_data['manufacturer'] = df_cern_sensor['MANUFACTURER_NAME'][0]
            cern_data['location'] = df_cern_sensor['LOCATION_NAME'][0]
        except Exception as e:
            print('error CERN DB for sensor detail')

        df_cern_wafer = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trker_sensor_wafers_v p where p.SNSR_NAME ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
        print(df_cern_wafer)
    except Exception as e:
        print("could not get cern db data ", e)
    #
    # plotly_layout = json.dumps({'margin': dict(l=0, r=0, t=50, b=0)})
    # plotly_config = json.dumps({'displaylogo': False,
    #                             'toImageButtonOptions': {
    #                                 'format': 'png',  # one of png, svg, jpeg, webp
    #                                 'filename': 'custom_image',
    #                                 'height': 1000,
    #                                 'width': 1400,
    #                                 'scale': 2  # Multiply title/legend/axis/canvas sizes by this factor
    #                             },
    #                             'scrollZoom': True,
    #                             'displayModeBar': True,
    #                             'modeBarButtonsToAdd': ['drawline',
    #                                                     'drawopenpath',
    #                                                     'drawclosedpath',
    #                                                     'drawcircle',
    #                                                     'drawrect',
    #                                                     'eraseshape'
    #                                                     ]
    #                             })
    #
    # # get all measurements id where this sensor is used:
    # df_measuremend_ids = pd.read_sql_query('''SELECT id, timestamp, "assembly_step" FROM "IV_Measurement" WHERE iv_file_sensor = ''' + "'" + str(SensorOverview_data.sensor_name) + "' ORDER BY timestamp" + ';', con=engine)

    # # plot all data as reference
    # df_all = pd.read_sql_query('''SELECT "IV_measurement_id", voltage, current, "ramp_direction", temperature, humidity  FROM "IV_Measurement_Results" ''' + ";", con=engine)
    # # df_measurement_ids = df_all["IV_measurement_id"].unique()
    # #
    # # fig = go.Figure()
    # # for i in range(len(df_measuremend_ids['id'])):
    # #     fig.add_trace(go.Line(
    # #         x=df_all[df_all['IV_measurement_id'] == df_measuremend_ids['id'][i]]['voltage'],
    # #         y=df_all[df_all['IV_measurement_id'] == df_measuremend_ids['id'][i]]['current'],
    # #         name=str(df_measuremend_ids['timestamp'][i].date()) + " - Aachen - " + str(df_measuremend_ids['assembly_step'][i])  + " - " + str(round(df_all['temperature'].mean(), 2)) + "°C - " + str(round(df_all['humidity'].mean(), 2)) + "%H", mode='lines+markers'))
    # #
    # # try:
    # #     # CERN DATABASE:
    # #     import sys
    # #     from io import StringIO
    # #     sys.path.append('application/scripts')
    # #     from rhapi import RhApi
    # #     url = "https://cmsdca.cern.ch/trk_rhapi"
    # #
    # #     rh = RhApi(url, debug=False, sso="login")
    # #     df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
    # #     run_numbers = df_cern['RUN_NUMBER'].unique()
    # #
    # #     sql_string = "select * from trker_cmsr.runs p where "
    # #     for i in run_numbers:
    # #         sql_string += "p.RUN_NUMBER =" + str(i) + " or "
    # #     sql_string += "p.RUN_NUMBER = -1 order by 'BEGIN_DATE' desc"
    # #
    # #     df_cern_runs = pd.read_csv(StringIO(rh.csv(sql_string)), sep=",")
    # #
    # #     df_cern_runs['BEGIN_DATE'] = pd.to_datetime(df_cern_runs['BEGIN_DATE']).dt.date
    # #
    # #     for i in run_numbers:
    # #         mean_temp = round(df_cern[df_cern['RUN_NUMBER'] == i]['TEMP_DEGC'].mean(), 2)
    # #         mean_hum = round(df_cern[df_cern['RUN_NUMBER'] == i]['RH_PRCNT'].mean(), 2)
    # #
    # #         fig.add_trace(go.Line(
    # #             x=df_cern[df_cern['RUN_NUMBER'] == i]['VOLTS'],
    # #             y=df_cern[df_cern['RUN_NUMBER'] == i]['CURRNT_AMP'],
    # #             name=str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['BEGIN_DATE'].unique()[0]) + " - "+ str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['LOCATION'].unique()[0]) + " - "+ str(df_cern[df_cern['RUN_NUMBER'] == i]['RUN_TYPE'].unique()[0])  + " - " + str(mean_temp) + "°C - " + str(mean_hum) + "%H" + " - " + str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['DESCRIPTION'].unique()[0]), mode='lines+markers',))
    # #
    # #
    # #
    # # except Exception as e:
    # #     print("could not get plot data from cern db ", e)
    # #
    # # fig.update_layout(
    # #     title_text="IV-Curve",
    # #     autosize=True,
    # #     height=1000,
    # #     paper_bgcolor="LightSteelBlue",
    # # )
    # #
    # # fig.update_layout(dict(updatemenus=[
    # #     dict(
    # #         type="buttons",
    # #         direction="left",
    # #         buttons=list([
    # #             dict(
    # #                 args=["visible", "legendonly"],
    # #                 label="Deselect All",
    # #                 method="restyle"
    # #             ),
    # #             dict(
    # #                 args=["visible", True],
    # #                 label="Select All",
    # #                 method="restyle"
    # #             )
    # #         ]),
    # #         pad={"r": 10, "t": 10},
    # #         showactive=False,
    # #         x=1,
    # #         xanchor="left",
    # #         y=0,
    # #         yanchor="top"
    # #     ),
    # #     dict(active=1,
    # #          buttons=list([
    # #              dict(label='Log Scale',
    # #                   method='update',
    # #                   args=[{'visible': [True, True]},
    # #                         {'title': 'Log scale',
    # #                          'yaxis': {'type': 'log'}}]),
    # #              dict(label='Linear Scale',
    # #                   method='update',
    # #                   args=[{'visible': [True, False]},
    # #                         {'title': 'Linear scale',
    # #                          'yaxis': {'type': 'linear'}}])
    # #          ]),
    # #      ),
    # # ]
    # # ))
    # # fig.update_xaxes(title_text="Voltage in Volt")
    # # fig.update_yaxes(title_text="Current in pA")
    # # fig.update_xaxes(autorange="reversed")
    # # fig.update_yaxes(autorange="reversed")
    # # fig_all = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    # # plotly_data = {}
    # # plotly_data['fig_all'] = fig_all
    # # plotly_data['plotly_layout'] = plotly_layout
    # # plotly_data['plotly_config'] = plotly_config
    # query data from general dables
    print(cern_data)

    station_data = GeneralStation.query.all()
    station_list_id = []
    station_list_name = []
    for station in station_data:
        station_list_id.append(station.id)
        station_list_name.append(station.name)
    station_list = list(zip(station_list_id, station_list_name))

    operator_data = GeneralOperators.query.all()
    operator_list_id = []
    operator_list_name = []
    for operator in operator_data:
        operator_list_id.append(operator.id)
        operator_list_name.append(str(operator.family_name + " " + operator.first_name))
    operator_list = list(zip(operator_list_id, operator_list_name))

    status_data = GeneralStatus.query.all()
    status_value = []
    status_name = []
    for status in status_data:
        status_value.append(status.id)
        status_name.append(status.name)
    status_list = list(zip(status_value, status_name))

    grade_data = GeneralGrade.query.all()
    grade_list_id = []
    grade_list_name = []
    for grade in grade_data:
        grade_list_id.append(grade.id)
        grade_list_name.append(str(grade.value))
    grade_list = list(zip(grade_list_id, grade_list_name))

    module_data = ModuleOverview.query.all()
    module_value = []
    module_name = []
    for module in module_data:
        module_value.append(module.id)
        module_name.append(module.cern_module_id)
    module_list = list(zip(module_value, module_name))

    class SensorDetailForm(FlaskForm):
        module          = SelectField(u'Module', default=SensorOverview_data.module, choices=module_list, validators=[DataRequired()])
        location        = SelectField(u'current Location', default=SensorOverview_data.current_location, choices=station_list, validators=[DataRequired()])
        grade           = SelectField(u'manual Grade', default=SensorOverview_data.manual_grade, choices=grade_list, validators=[DataRequired()])
        # status          = SelectField(u'Station', default=HVMeasurement_data.measurement_status, choices=status_list, validators=[DataRequired()])
        comment         = TextAreaField(u'Comment', default=SensorOverview_data.comment)
        submit          = SubmitField("Update")

    form = SensorDetailForm()
    if form.validate_on_submit():
        flash('Submission successful !', 'success')
        print(form.module.data, form.location.data, form.comment.data)
        SensorOverview_data.module             = form.module.data
        SensorOverview_data.current_location   = form.location.data
        SensorOverview_data.current_location_timestamp = datetime.now(timezone.utc)
        SensorOverview_data.comment            = form.comment.data
        SensorOverview_data.manual_grade       = form.grade.data

        db.session.add(SensorOverview_data)
        db.session.commit()
        return redirect(url_for("flask_modules.sensor_detail", sensor_number=SensorOverview_data.id))


    return render_template("sensor_detail.html", data=SensorOverview_data, tech_data=tech_data, form=form, cern_data=cern_data)

# @flask_modules.route('cern_data/sensor_<int:sensor_number>', methods=['GET', 'POST'])
# def sensor_detail_cern(sensor_number):
#
#     # get sensor details from CERN DB:
#     # CERN DATABASE:
#     cern_data = {}
#     try:
#         print("try getting data from cern db")
#         import sys
#         from io import StringIO
#         sys.path.append('application/scripts')
#         from rhapi import RhApi
#         url = "https://cmsdca.cern.ch/trk_rhapi"
#
#         rh = RhApi(url, debug=False, sso="login")
#         df_cern_sensor = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trker_sensors_v p where p.SNSR_NAME ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
#         print(df_cern_sensor)
#         try:
#             cern_data['sensor'] = df_cern_sensor['SENSOR'][0]
#             cern_data['serial_number'] = df_cern_sensor['SNSR_SER_NUM'][0]
#             cern_data['name'] = df_cern_sensor['SNSR_NAME'][0]
#             cern_data['type'] = df_cern_sensor['SENSOR_TYPE'][0]
#             cern_data['manufacturer'] = df_cern_sensor['MANUFACTURER_NAME'][0]
#             cern_data['location'] = df_cern_sensor['LOCATION_NAME'][0]
#         except Exception as e:
#             print('error CERN DB for sensor detail')
#
#         df_cern_wafer = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trker_sensor_wafers_v p where p.SNSR_NAME ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
#         print(df_cern_wafer)
#     except Exception as e:
#         print("could not get cern db data ", e)
#     #
#     # plotly_layout = json.dumps({'margin': dict(l=0, r=0, t=50, b=0)})
#     # plotly_config = json.dumps({'displaylogo': False,
#     #                             'toImageButtonOptions': {
#     #                                 'format': 'png',  # one of png, svg, jpeg, webp
#     #                                 'filename': 'custom_image',
#     #                                 'height': 1000,
#     #                                 'width': 1400,
#     #                                 'scale': 2  # Multiply title/legend/axis/canvas sizes by this factor
#     #                             },
#     #                             'scrollZoom': True,
#     #                             'displayModeBar': True,
#     #                             'modeBarButtonsToAdd': ['drawline',
#     #                                                     'drawopenpath',
#     #                                                     'drawclosedpath',
#     #                                                     'drawcircle',
#     #                                                     'drawrect',
#     #                                                     'eraseshape'
#     #                                                     ]
#     #                             })
#     #
#     # # get all measurements id where this sensor is used:
#     # df_measuremend_ids = pd.read_sql_query('''SELECT id, timestamp, "assembly_step" FROM "IV_Measurement" WHERE iv_file_sensor = ''' + "'" + str(SensorOverview_data.sensor_name) + "' ORDER BY timestamp" + ';', con=engine)
##
#     # # plot all data as reference
#     # df_all = pd.read_sql_query('''SELECT "IV_measurement_id", voltage, current, "ramp_direction", temperature, humidity  FROM "IV_Measurement_Results" ''' + ";", con=engine)
#     # # df_measurement_ids = df_all["IV_measurement_id"].unique()
#     # #
#     # # fig = go.Figure()
#     # # for i in range(len(df_measuremend_ids['id'])):
#     # #     fig.add_trace(go.Line(
#     # #         x=df_all[df_all['IV_measurement_id'] == df_measuremend_ids['id'][i]]['voltage'],
#     # #         y=df_all[df_all['IV_measurement_id'] == df_measuremend_ids['id'][i]]['current'],
#     # #         name=str(df_measuremend_ids['timestamp'][i].date()) + " - Aachen - " + str(df_measuremend_ids['assembly_step'][i])  + " - " + str(round(df_all['temperature'].mean(), 2)) + "°C - " + str(round(df_all['humidity'].mean(), 2)) + "%H", mode='lines+markers'))
#     # #
#     # # try:
#     # #     # CERN DATABASE:
#     # #     import sys
#     # #     from io import StringIO
#     # #     sys.path.append('application/scripts')
#     # #     from rhapi import RhApi
#     # #     url = "https://cmsdca.cern.ch/trk_rhapi"
#     # #
#     # #     rh = RhApi(url, debug=False, sso="login")
#     # #     df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(SensorOverview_data.sensor_name) + "'")), sep=",")
#     # #     run_numbers = df_cern['RUN_NUMBER'].unique()
#     # #
#     # #     sql_string = "select * from trker_cmsr.runs p where "
#     # #     for i in run_numbers:
#     # #         sql_string += "p.RUN_NUMBER =" + str(i) + " or "
#     # #     sql_string += "p.RUN_NUMBER = -1 order by 'BEGIN_DATE' desc"
#     # #
#     # #     df_cern_runs = pd.read_csv(StringIO(rh.csv(sql_string)), sep=",")
#     # #
#     # #     df_cern_runs['BEGIN_DATE'] = pd.to_datetime(df_cern_runs['BEGIN_DATE']).dt.date
#     # #
#     # #     for i in run_numbers:
#     # #         mean_temp = round(df_cern[df_cern['RUN_NUMBER'] == i]['TEMP_DEGC'].mean(), 2)
#     # #         mean_hum = round(df_cern[df_cern['RUN_NUMBER'] == i]['RH_PRCNT'].mean(), 2)
#     # #
#     # #         fig.add_trace(go.Line(
#     # #             x=df_cern[df_cern['RUN_NUMBER'] == i]['VOLTS'],
#     # #             y=df_cern[df_cern['RUN_NUMBER'] == i]['CURRNT_AMP'],
#     # #             name=str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['BEGIN_DATE'].unique()[0]) + " - "+ str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['LOCATION'].unique()[0]) + " - "+ str(df_cern[df_cern['RUN_NUMBER'] == i]['RUN_TYPE'].unique()[0])  + " - " + str(mean_temp) + "°C - " + str(mean_hum) + "%H" + " - " + str(df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['DESCRIPTION'].unique()[0]), mode='lines+markers',))
#     # #
#     # #
#     # #
#     # # except Exception as e:
#     # #     print("could not get plot data from cern db ", e)
#     # #
#     # # fig.update_layout(
#     # #     title_text="IV-Curve",
#     # #     autosize=True,
#     # #     height=1000,
#     # #     paper_bgcolor="LightSteelBlue",
#     # # )
#     # #
#     # # fig.update_layout(dict(updatemenus=[
#     # #     dict(
#     # #         type="buttons",
#     # #         direction="left",
#     # #         buttons=list([
#     # #             dict(
#     # #                 args=["visible", "legendonly"],
#     # #                 label="Deselect All",
#     # #                 method="restyle"
#     # #             ),
#     # #             dict(
#     # #                 args=["visible", True],
#     # #                 label="Select All",
#     # #                 method="restyle"
#     # #             )
#     # #         ]),
#     # #         pad={"r": 10, "t": 10},
#     # #         showactive=False,
#     # #         x=1,
#     # #         xanchor="left",
#     # #         y=0,
#     # #         yanchor="top"
#     # #     ),
#     # #     dict(active=1,
#     # #          buttons=list([
#     # #              dict(label='Log Scale',
#     # #                   method='update',
#     # #                   args=[{'visible': [True, True]},
#     # #                         {'title': 'Log scale',
#     # #                          'yaxis': {'type': 'log'}}]),
#     # #              dict(label='Linear Scale',
#     # #                   method='update',
#     # #                   args=[{'visible': [True, False]},
#     # #                         {'title': 'Linear scale',
#     # #                          'yaxis': {'type': 'linear'}}])
#     # #          ]),
#     # #      ),
#     # # ]
#     # # ))
#     # # fig.update_xaxes(title_text="Voltage in Volt")
#     # # fig.update_yaxes(title_text="Current in pA")
#     # # fig.update_xaxes(autorange="reversed")
#     # # fig.update_yaxes(autorange="reversed")
#     # # fig_all = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
#     # # plotly_data = {}
#     # # plotly_data['fig_all'] = fig_all
#     # # plotly_data['plotly_layout'] = plotly_layout
#     # # plotly_data['plotly_config'] = plotly_config
#     # query data from general dables
#     print(cern_data)
#     return "Hello World"


@flask_modules.route('/data_sensor_<sensor_name>')
def sensor_detail_cern_db(sensor_name):
    # SensorOverview_data = SensorOverview.query.filter_by(sensor_name=sensor_name).first()
    # get sensor details from CERN DB:
    # CERN DATABASE:
    print("try getting data from cern db")
    import sys
    from io import StringIO
    sys.path.append('application/scripts')
    from rhapi import RhApi
    url = "https://cmsdca.cern.ch/trk_rhapi"

    rh = RhApi(url, debug=False, sso="login")


    json_data = {'data': df_cern.to_dict(orient="records"),
        'recordsFiltered': 0,
        'recordsTotal': len(df_cern),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/pulltest_data_modules_<int:module_number>')
def pulltest_data_modules(module_number):
    db_table_class = PulltestMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(module=module_number)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
@flask_modules.route('/pulltest_data_HV_<int:module_number>')
def pulltest_data_HV(module_number):
    db_table_class = PulltestMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query.filter_by(module=module_number)
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data
@flask_modules.route('/metrology_sensor_data_<int:module_number>')
def metrology_data_sensor(module_number):
    db_table_class = MetrologySensorSensorAlignment
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(module=module_number)

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/metrology_bridge_data_<int:module_number>')
def metrology_data_bridge(module_number):
    db_table_class = MetrologySensorBridgeAlignment
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(module=module_number)

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/metrology_height_data_<int:module_number>')
def metrology_data_height(module_number):
    db_table_class = MetrologyHeightProfile
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(module=module_number)

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/iv_curve_data_<sensor_name>')
def iv_curve_data(sensor_name):
    db_table_class = IVMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(iv_file_sensor=sensor_name).filter(IVMeasurement.bad_measurement.isnot(True))

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_grading() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/iv_curve_cern_data_<sensor_name>')
def iv_curve_data_cern(sensor_name):
    import sys
    from io import StringIO

    sys.path.append('application/scripts')
    from rhapi import RhApi

    url = "https://cmsdca.cern.ch/trk_rhapi"

    rh = RhApi(url, debug=False, sso="login")
    df_cern = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.tracker_sensor_iv_v p where p.SENSOR ='" + str(sensor_name) + "'")), sep=",")
    run_numbers = df_cern['RUN_NUMBER'].unique()

    sql_string = "select * from trker_cmsr.runs p where "
    for i in run_numbers:
        sql_string += "p.RUN_NUMBER =" + str(i) + " or "
    sql_string += "p.RUN_NUMBER = -1"

    df_cern_runs = pd.read_csv(StringIO(rh.csv(sql_string)), sep=",")

    df_cern_runs['BEGIN_DATE'] = pd.to_datetime(df_cern_runs['BEGIN_DATE']).dt.date

    for i in run_numbers:
        df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['TEMP_DEGC'] = round(df_cern[df_cern['RUN_NUMBER'] == i]['TEMP_DEGC'].mean(), 2)
        df_cern_runs[df_cern_runs['RUN_NUMBER'] == i]['RH_PRCNT'] = round(df_cern[df_cern['RUN_NUMBER'] == i]['RH_PRCNT'].mean(), 2)

    json_data = {'data': df_cern_runs.to_dict(orient="records"),
        'recordsFiltered': 0,
        'recordsTotal': len(df_cern_runs),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    # jsonify(df_cern_runs.to_dict(orient="records"))
    print(df_cern_runs)
    return json_data

@flask_modules.route('/ivt_curve_data_<sensor_name>')
def ivt_curve_data(sensor_name):
    db_table_class = IVtMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(iv_file_sensor=sensor_name).filter(IVtMeasurement.bad_measurement.isnot(True))

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_grading() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/hv_curve_data_<sensor_name>')
def hv_curve_data(sensor_name):
    db_table_class = HVMeasurement
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(iv_file_sensor=sensor_name).filter(HVMeasurement.bad_measurement.isnot(True))

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_grading() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data

@flask_modules.route('/si_measurement_data_<sensor_name>')
def si_curve_data(sensor_name):
    db_table_class = SurfaceInspection
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)

    # global filter
    query = db_table_class.query.filter_by(file_sensor=sensor_name)

    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)
    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)

    json_data = {'data': [data.to_dict_all() for data in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': db_table_class.query.count(),
        'draw': request.args.get('draw', type=int),
         }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data



## CMS DB Data
@flask_modules.route('/cmsdb_feh_<serial_number>')
def module_detail_cmsdb_feh(serial_number):
    rh = RhApi(url, debug=False, sso="login")
    df = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.p6640 pp where pp.SERIAL_NUMBER='" + str(serial_number) + "'")))
    df = df.T
    data = df.to_html()
    return data
@flask_modules.route('/cmsdb_seh_<serial_number>')
def module_detail_cmsdb_seh(serial_number):
    print('cms db feh data via iframe', serial_number)
    rh = RhApi(url, debug=False, sso="login")
    df = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.p6660 pp where pp.SERIAL_NUMBER='" + str(serial_number) + "'")))
    df = df.T
    data = df.to_html()
    return data
@flask_modules.route('/cmsdb_sensor_<serial_number>')
def module_detail_cmsdb_sensor(serial_number):
    rh = RhApi(url, debug=False, sso="login")
    df = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.p1120 pp where pp.BARCODE='" + str(serial_number) + "'")))
    df = df.T
    data = df.to_html()
    return data
@flask_modules.route('/cmsdb_module_<serial_number>')
def module_detail_cmsdb_module(serial_number):
    rh = RhApi(url, debug=False, sso="login")
    df = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.p9000 pp where pp.NAME_LABEL='" + str(serial_number) + "'")))
    df = df.T
    data = df.to_html()
    return data

@flask_modules.route('/cmsdb_manufacturers_<manufacturer>')

def module_detail_cmsdb_manufacturer(manufacturer):
    rh = RhApi(url, debug=False, sso="login")
    df = pd.read_csv(StringIO(rh.csv("select * from trker_cmsr.trkr_manufacturers_v manufacturers pp where pp.MANUFACTURER='" + str(manufacturer) + "'")))
    df = df.T
    data = df.to_html()
    return data