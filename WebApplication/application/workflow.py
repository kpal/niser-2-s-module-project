import os
import sys
import json
import pandas as pd
import jinja2
import paramiko
import influxdb_client
from datetime import datetime, timezone
from flask import Flask, render_template, request, Blueprint, url_for, flash, redirect, jsonify, url_for, flash, send_file, session
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, TextAreaField, SubmitField, PasswordField, BooleanField, RadioField, IntegerField, DateField, DateTimeField, TimeField, SelectField
from wtforms.validators import InputRequired, Email, DataRequired, Length, Regexp
from stat import S_ISDIR, S_ISREG

from application                import app, db, bcrypt, mail
from application.models         import *

sys.path.append('application/scripts')
from rhapi import RhApi
import sys
from io import StringIO
from cmsdbldr_client import LoaderClient

url = "https://cmsdca.cern.ch/trk_rhapi"
url_upload = "https://cmsdca.cern.ch/trk_loader/trker/int2r"
grade_codes = ['Good', 'Bad', 'Damaged', 'Needs retest', 'Need repair']

flask_workflow = Blueprint('flask_workflow', __name__, template_folder="templates/workflow", url_prefix="/workflow")



def datatables_json(db_table_class):
    column_list = []
    for column_name in db_table_class.__dict__:
        column_list.append(column_name)
    # global filter
    query = db_table_class.query
    total_filtered = query.count()
    # sorting
    order = []
    i = 0
    col = getattr(db_table_class, "timestamp")
    col = col.desc()
    order.append(col)
    query = query.order_by(*order)
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        if col_name not in column_list:
            col_name = 'id'
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = getattr(db_table_class, col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)

    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # json data
    json_data = {'data': [data.to_dict() for data in query],
                 'recordsFiltered': total_filtered,
                 'recordsTotal': db_table_class.query.count(),
                 'draw': request.args.get('draw', type=int),
                 }
    json_string = json.dumps(json_data, default=str)
    json_string = json_string.replace("NaN", "null")
    json_data = json.loads(json_string)
    return json_data


@flask_workflow.route('/', methods=["GET", "POST"])
def workflow():
    class QRForm(FlaskForm):
        input          = StringField(u'Input', validators=[DataRequired()])
        submit          = SubmitField("Save")

    form = QRForm()
    if form.validate_on_submit():
        if form.input.data == "mod_bareSensor":
            return redirect(url_for("flask_workflow.mod_bareSensor"))
        if form.input.data == "mod_polyimide":
            return redirect(url_for("flask_workflow.mod_polyimide"))
        if form.input.data == "mod_tail":
            return redirect(url_for("flask_workflow.mod_tail"))
        if form.input.data == "mod_tailBonded":
            return redirect(url_for("flask_workflow.mod_tailBonded"))
        if form.input.data == "mod_tailEncapsulated":
            return redirect(url_for("flask_workflow.mod_tailEncapsulated"))
        if form.input.data == "mod_spacersToPSs":
            return redirect(url_for("flask_workflow.mod_spacersToPSs"))
        if form.input.data == "mod_bareModule":
            return redirect(url_for("flask_workflow.mod_bareModule"))
        if form.input.data == "mod_final":
            return redirect(url_for("flask_workflow.mod_final"))
        if form.input.data == "mod_finalCold":
            return redirect(url_for("flask_workflow.mod_finalCold"))
        if form.input.data == "mod_bareSEH":
            return redirect(url_for("flask_workflow.mod_bareSEH"))
        if form.input.data == "mod_bareFEH":
            return redirect(url_for("flask_workflow.mod_bareFEH"))
        if form.input.data == "mod_basePlate":
            return redirect(url_for("flask_workflow.mod_basePlate"))
        if form.input.data == "mod_barePOH":
            return redirect(url_for("flask_workflow.mod_barePOH"))
        if form.input.data == "mod_bonded":
            return redirect(url_for("flask_workflow.mod_bonded"))
        if form.input.data == "mod_topEncapsulated":
            return redirect(url_for("flask_workflow.mod_topEncapsulated"))
        if form.input.data == "mod_bottomEncapsulated":
            return redirect(url_for("flask_workflow.mod_bottomEncapsulated"))

        flash('Code not known', 'danger')
        return redirect(url_for("flask_workflow.workflow"))
    return render_template("workflow.html", form=form)


@flask_workflow.route('/mod_bareSensor', methods=["GET", "POST"])
# Sensor Step 1
def mod_bareSensor():
    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    class modbareSensorForm(FlaskForm):
        sensor          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        measurement_iv_done                 = BooleanField(u'IV', validators=[])
        measurement_metrology_sensor_done   = BooleanField(u'Metrology', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = modbareSensorForm()
    if form.validate_on_submit():
        data = WorkflowModBareSensor(
            sensor_name=form.sensor.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            grade=form.grade.data,
            measurement_metrology_sensor_done = form.measurement_metrology_sensor_done.data,
            measurement_iv_done = form.measurement_iv_done.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bareSensor"))

    return render_template("mod_bareSensor.html", form=form,
                           sensors_list=sensors_list,
                           operators_list=operators_list)

@flask_workflow.route('/mod_polyimide', methods=["GET", "POST"])
# Sensor Step 2 (Kapton gluing)
def mod_polyimide():
    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    jigs = ToolsJigskaptongluing.query.filter_by().all()
    jigs_list = []
    for jig in jigs:
        jigs_list.append(jig.barcode)

    class modpolyimiderForm(FlaskForm):
        sensor          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        jig             = StringField(u'Jig', validators=[DataRequired()])
        timestamp       = StringField()
        measurement_iv_done                 = BooleanField(u'IV')
        measurement_metrology_sensor_done   = BooleanField(u'Metrology')
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = modpolyimiderForm()
    if form.validate_on_submit():
        print('form is opendd')
        if form.timestamp.data == None:
            timestamp =  datetime.now()
        else:
            timestamp = form.timestamp.data

        data = WorkflowModPolyimide(
            sensor_name=form.sensor.data,
            operator=form.operator.data,
            timestamp = timestamp,
            kapton_gluing_jig = form.jig.data,
            measurement_metrology_sensor_done=form.measurement_metrology_sensor_done.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_polyimide"))
    return render_template("mod_polyimide.html", form=form,
                           sensors_list=sensors_list,
                           operators_list=operators_list,
                           jigs_list=jigs_list)

@flask_workflow.route('/mod_tail', methods=["GET", "POST"])
# Sensor Step 3
def mod_tail():
    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    jigs = ToolsJigsHVpigtaile.query.filter_by().all()
    jigs_list = []
    for jig in jigs:
        jigs_list.append(jig.barcode)


    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    class modtailForm(FlaskForm):
        sensor          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done                 = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = modtailForm()
    if form.validate_on_submit():
        data = WorkflowModTail(
            sensor_name=form.sensor.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_tail"))
    return render_template("mod_tail.html", form=form,
                           sensors_list=sensors_list,
                           operators_list=operators_list,
                           jigs_list=jigs_list)

@flask_workflow.route('/mod_tailBonded', methods=["GET", "POST"])
# Sensor Step 4
def mod_tailBonded():
    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    jigs = ToolsJigsBonding.query.filter_by().all()
    jigs_list = []
    for jig in jigs:
        jigs_list.append(jig.barcode)

    class modtailbondedForm(FlaskForm):
        sensor          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done                 = BooleanField(u'IV', validators=[])
        measurement_pulltest_done   = BooleanField(u'Pulltest', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = modtailbondedForm()
    if form.validate_on_submit():
        data = WorkflowModTailBonded(
            sensor_name=form.sensor.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            hv_gluing_jig = form.jig.data,
            measurement_pulltest_done=form.measurement_pulltest_done.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_tailBonded"))
    return render_template("mod_tailBonded.html", form=form,
                           sensors_list=sensors_list,
                           operators_list=operators_list,
                           jigs_list=jigs_list)

@flask_workflow.route('/mod_tailEncapsulated', methods=["GET", "POST"])
def mod_tailEncapsulated():
    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    jigs = ToolsJigshybridegluing.query.filter_by().all()
    jigs_list = []
    for jig in jigs:
        jigs_list.append(jig.barcode)

    class modtailEncapsulatedForm(FlaskForm):
        sensor          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = modtailEncapsulatedForm()
    if form.validate_on_submit():
        data = WorkflowModTailEncapsulated(
            sensor_name=form.sensor.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_tailEncapsulated"))
    return render_template("mod_tailEncapsulated.html", form=form,
                           sensors_list=sensors_list,
                           operators_list=operators_list,
                           jigs_list=jigs_list)


@flask_workflow.route('/mod_bareModule', methods=["GET", "POST"])
# Module Step 1 (link sensors)
def mod_bareModule():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    sensors = SensorOverview.query.filter_by().all()
    sensors_list = []
    for sensor in sensors:
        sensors_list.append(sensor.sensor_name)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_bareModuleForm(FlaskForm):
        module              = StringField(u'Sensor', validators=[DataRequired()])
        operator            = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig               = StringField(u'Jig', validators=[DataRequired()])
        top_sensor          = StringField(u'Top', validators=[])
        bottom_sensor       = StringField(u'Bottom', validators=[])
        # measurement_iv_done = BooleanField(u'IV', validators=[])
        comment             = TextAreaField(u'Comment')
        submit              = SubmitField("Save")

    form = mod_bareModuleForm()
    if form.validate_on_submit():
        data = WorkflowModBareModule(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            top_sensor = form.top_sensor.data,
            bottom_sensor = form.bottom_sensor.data,
            # hv_gluing_jig = form.jig.data,
            # measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bareModule"))

    return render_template("mod_bareModule.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           sensors_list=sensors_list
                           )

@flask_workflow.route('/mod_basePlate', methods=["GET", "POST"])
# Module Step 2
def mod_basePlate():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_bareModuleForm(FlaskForm):
        module          = StringField(u'Sensor', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_bareModuleForm()
    if form.validate_on_submit():
        data = WorkflowModBasePlate(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            # hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_basePlate"))
    return render_template("mod_basePlate.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )

@flask_workflow.route('/mod_bareFEH', methods=["GET", "POST"])
# Module Step 3 (link FEH)
def mod_bareFEH():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    # rh = RhApi(url, debug=False, sso="login")
    # df_cern = pd.read_csv(StringIO(rh.csv("select p.ID, p.MANUFACTURER, p.KIND_OF_PART, p.BARCODE, p.SERIAL_NUMBER, p.PRODUCTION_DATE,  "
    #                                       "loc.LOCATION_NAME, loc.INSTITUTITON_NAME from trker_cmsr.p6640 p inner join trker_cmsr.parts parts on parts.ID = p.ID inner join trker_cmsr.trkr_locations_v loc on loc.LOCATION_ID = parts.LOCATION_ID where loc.LOCATION_NAME = 'Aachen 1B'")), sep=",")
    #
    # print(df_cern['BARCODE'])

    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'Module', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        left_feh        = StringField(u'left', validators=[DataRequired()])
        right_feh       = StringField(u'right', validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModBareFEH(
            module_name = form.module.data,
            operator    = form.operator.data,
            timestamp   = datetime.now(),
            left_feh    = form.left_feh.data,
            right_feh   = form.right_feh.data,
            # hv_gluing_jig = form.jig.data,
            grade       = form.grade.data,
            comment     = form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bareFEH"))
    return render_template("mod_bareFEH.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )

@flask_workflow.route('/mod_bareSEH', methods=["GET", "POST"])
# Module Step 4 (link SEH)
def mod_bareSEH():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        seh             = StringField(u'seh', validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        # measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModBareSEH(
            module_name = form.module.data,
            operator    = form.operator.data,
            timestamp   = datetime.now(),
            seh         = form.seh.data,
            # hv_gluing_jig = form.jig.data,
            # measurement_iv_done=form.measurement_iv_done.data,
            grade       = form.grade.data,
            comment     = form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bareSEH"))

    return render_template("mod_bareSEH.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )
@flask_workflow.route('/mod_bonded', methods=["GET", "POST"])
# Module Step 5
def mod_bonded():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModBonded(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            hv_gluing_jig = form.jig.data,
            # measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bonded"))
    return render_template("mod_bonded.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )

@flask_workflow.route('/mod_topEncapsulated', methods=["GET", "POST"])
def mod_topEncapsulated():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModTopEncapsulated(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            # hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_topEncapsulated"))
    return render_template("mod_topEncapsulated.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )

@flask_workflow.route('/mod_bottomEncapsulated', methods=["GET", "POST"])
def mod_bottomEncapsulated():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade           = StringField(u'Grade', validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModBottomEncapsulated(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            # hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_bottomEncapsulated"))
    return render_template("mod_bottomEncapsulated.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )

@flask_workflow.route('/mod_final', methods=["GET", "POST"])
def mod_final():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)


    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModFinal(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            # hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_final"))
    return render_template("mod_final.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )
@flask_workflow.route('/mod_finalCold', methods=["GET", "POST"])
def mod_finalCold():
    modules = ModuleOverview.query.filter_by().all()
    modules_list = []
    for value in modules:
        modules_list.append(value.cern_module_id)

    operators = GeneralOperators.query.filter_by(is_used=True).all()
    operators_list = []
    for operator in operators:
        operators_list.append(operator.identifier)

    class mod_basePlateForm(FlaskForm):
        module          = StringField(u'', validators=[DataRequired()])
        operator        = StringField(u'Operator', validators=[DataRequired()])
        grade               = SelectField(u'Grade', choices=grade_codes, validators=[DataRequired()])
        # jig             = StringField(u'Jig', validators=[DataRequired()])
        measurement_iv_done = BooleanField(u'IV', validators=[])
        comment         = TextAreaField(u'Comment')
        submit          = SubmitField("Save")

    form = mod_basePlateForm()
    if form.validate_on_submit():
        data = WorkflowModFinalCold(
            module_name=form.module.data,
            operator=form.operator.data,
            timestamp = datetime.now(),
            # hv_gluing_jig = form.jig.data,
            measurement_iv_done=form.measurement_iv_done.data,
            grade=form.grade.data,
            comment=form.comment.data,
        )
        db.session.add(data)
        db.session.commit()
        flash('Submission successful !', 'success')
        return redirect(url_for("flask_workflow.mod_finalCold"))
    return render_template("mod_finalCold.html", form=form,
                           modules_list=modules_list,
                           operators_list=operators_list,
                           )



@flask_workflow.route('/mod_bareSensor/data', methods=["GET", "POST"])
def mod_bareSensor_data():
    json_data = datatables_json(WorkflowModBareSensor)
    return json_data
@flask_workflow.route('/mod_polyimide/data', methods=["GET", "POST"])
def mod_polyimide_data():
    json_data = datatables_json(WorkflowModPolyimide)
    return json_data

@flask_workflow.route('/mod_tail/data', methods=["GET", "POST"])
def mod_tail_data():
    json_data = datatables_json(WorkflowModTail)
    return json_data
@flask_workflow.route('/mod_tailBonded/data', methods=["GET", "POST"])
def mod_tailBonded_data():
    json_data = datatables_json(WorkflowModTailBonded)
    return json_data

@flask_workflow.route('/mod_tailEncapsulated/data', methods=["GET", "POST"])
def mod_tailEncapsulated_data():
    json_data = datatables_json(WorkflowModTailEncapsulated)
    return json_data
@flask_workflow.route('/mod_spacersToPSs/data', methods=["GET", "POST"])
def mod_spacersToPSs_data():
    json_data = datatables_json(WorkflowModSpacersToPS)
    return json_data



@flask_workflow.route('/mod_bareModule/data', methods=["GET", "POST"])
def mod_bareModule_data():
    json_data = datatables_json(WorkflowModBareModule)
    return json_data

@flask_workflow.route('/mod_basePlate/data', methods=["GET", "POST"])
def mod_basePlate_data():
    json_data = datatables_json(WorkflowModBasePlate)
    return json_data

@flask_workflow.route('/mod_barePOH/data', methods=["GET", "POST"])
def mod_barePOH_data():
    json_data = datatables_json(WorkflowModBarePOH)
    return json_data

@flask_workflow.route('/mod_bareFEH/data', methods=["GET", "POST"])
def mod_bareFEH_data():
    json_data = datatables_json(WorkflowModBareFEH)
    return json_data

@flask_workflow.route('/mod_bareSEH/data', methods=["GET", "POST"])
def mod_bareSEH_data():
    json_data = datatables_json(WorkflowModBareSEH)
    return json_data

@flask_workflow.route('/mod_bonded/data', methods=["GET", "POST"])
def mod_bonded_data():
    json_data = datatables_json(WorkflowModBonded)
    return json_data

@flask_workflow.route('/mod_topEncapsulated/data', methods=["GET", "POST"])
def mod_topEncapsulated_data():
    json_data = datatables_json(WorkflowModTopEncapsulated)
    return json_data

@flask_workflow.route('/mod_bottomEncapsulated/data', methods=["GET", "POST"])
def mod_bottomEncapsulated_data():
    json_data = datatables_json(WorkflowModBottomEncapsulated)
    return json_data

@flask_workflow.route('/mod_final/data', methods=["GET", "POST"])
def mod_final_data():
    json_data = datatables_json(WorkflowModFinal)
    return json_data
@flask_workflow.route('/mod_finalCold/data', methods=["GET", "POST"])
def mod_finalCold_data():
    json_data = datatables_json(WorkflowModFinalCold)
    return json_data


@flask_workflow.route('/qr', methods=["GET", "POST"])
def qr_scanner():
    import barcode

    EAN = barcode.get_barcode_class('Gs1_128')
    data = []
    data.append(("Hello World", EAN("Hello World").render().decode('utf-8')))
    data.append(("Turn Off", EAN("%%POWEROFF").render().decode('utf-8')))
    data.append(("Change Language to German", EAN("%%GE").render().decode('utf-8')))
    data.append(("Change Language to English", EAN("%%EN").render().decode('utf-8')))
    data.append(("Sleeptime to 5min", EAN("%%ALLTIM04").render().decode('utf-8')))
    data.append(("Turn off Sleeptimer", EAN("%%ALL*TIDIS").render().decode('utf-8')))
    data.append(("Bluetooth Pairing", EAN("%%BT_HID").render().decode('utf-8')))

    data.append(("New Line", EAN("\\n").render().decode('utf-8')))
    data.append(("New Line/Enter", EAN("%0A").render().decode('utf-8')))
    print(data)


    return render_template("qr.html", data=data)
