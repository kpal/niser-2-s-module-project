FROM python:3.11

WORKDIR /WebApplication

COPY WebApplication/ .
COPY requirements.txt requirements.txt

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install gunicorn
RUN apt update
RUN apt install nano

RUN chmod +x boot.sh

ENV FLASK_APP run.py

EXPOSE 8080
ENTRYPOINT ["/bin/sh", "/WebApplication/boot.sh"]